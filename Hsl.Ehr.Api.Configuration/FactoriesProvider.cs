﻿using System;
using Hsl.Ehr.Domain.Abstract.Factories;
using Hsl.Ehr.Domain.Abstract.Repositories.Drafts;

namespace Hsl.Ehr.Api.Configuration
{
    public static class FactoriesProvider
    {
        private static IReacoesAdversasRepositoryFactory _ReacoesAdversasRepositoryFactory;

        public static void SetFactory(IReacoesAdversasRepositoryFactory ReacoesAdversasRepositoryFactory)
        {
            _ReacoesAdversasRepositoryFactory = ReacoesAdversasRepositoryFactory;
        }

        public static IReacoesAdversasRepositoryFactory GetReacoesAdversasRepositoryFactory()
        {
            if (_ReacoesAdversasRepositoryFactory == null)
                throw new InvalidOperationException("Não foi definida nenhuma factory para este repositório");

            return _ReacoesAdversasRepositoryFactory;
        }
    }
}
