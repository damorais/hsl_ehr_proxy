﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Api.Configuration
{
    internal abstract class ConfigurationFactory
    {
        private static Configuracoes configuracoes;

        internal static Configuracoes ObterConfiguracoes()
        {
            if (configuracoes == null)
                configuracoes = new Configuracoes();

            return configuracoes;
        }

    }
}
