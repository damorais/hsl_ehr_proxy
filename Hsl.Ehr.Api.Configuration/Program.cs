﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Services;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Api.Configuration
{
    class Program
    {
        static void Main(string[] args)
        {
            AppConfig.RegisterFactories();
            TesteService();
        }

        public static void TesteService()
        {
            var conn = System.Configuration.ConfigurationManager.ConnectionStrings["Padrao"].ConnectionString;

            var tiporeacaoservico = ServicesProvider.GetTiposDeReacaoService();//new TiposDeReacaoTerminologyService(conn, true);

            IEnumerable<TipoDeReacao> resultados = tiporeacaoservico.GetAll();
            TipoDeReacao reacao = tiporeacaoservico.GetByCode(4386001);

            foreach (var s in resultados)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("GetBYCODE");
            Console.WriteLine(reacao);
        }
    }
}
