﻿using Hsl.Ehr.Data.Factories;
using Hsl.Ehr.Data.Services;
using Hsl.Ehr.Data.Services.SIH;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;

namespace Hsl.Ehr.Api.Configuration
{
    //TODO: Preciso utilizar adequadamente do WebConfig
    public static class AppConfig
    {
        public static readonly string ConnectionString = ConfigurationFactory.ObterConfiguracoes().ConexaoPadrao;
        public static readonly string UrlProvedorDemografia = ConfigurationFactory.ObterConfiguracoes().UrlProvedorDemografia;


        public static void RegisterFactories()
        {
            FactoriesProvider.SetFactory(new ReacoesAdversasRepositoryFactory());
        }

        public static void RegisterServices()
        {
            //TODO: Mudar cache para algum parametro de configuração
            ServicesProvider.SetService(new TiposDeReacaoTerminologyService(ConnectionString, false));
            ServicesProvider.SetService(new CategoriasDeReacaoTerminologyService(ConnectionString, false));

            ServicesProvider.SetService(new AlimentosTerminologyService(ConnectionString, false));
            ServicesProvider.SetService(new InalantesTerminologyService(ConnectionString, false));
            ServicesProvider.SetService(new ContatoTerminologyService(ConnectionString, false));

            ServicesProvider.SetService(new MedicamentosTerminologyService(ConnectionString));
            
            ServicesProvider.SetService(new ReacoesAdversasService(ConnectionString, UrlProvedorDemografia));

            ServicesProvider.SetService(new AlergiasService(ConnectionString));
        }
    }
}
