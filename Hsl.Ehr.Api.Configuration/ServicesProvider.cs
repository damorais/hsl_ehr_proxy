﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Abstract.Services.Drafts;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.SIH;

namespace Hsl.Ehr.Api.Configuration
{
    public static class ServicesProvider
    {
        private static ITiposDeReacaoTerminologyService _TiposDeReacaoService;
        private static ICategoriasDeReacaoTerminologyService _CategoriasDeReacaoService;
        private static IMedicamentosTerminologyService _MedicamentosService;
        private static IAlimentosTerminologyService _AlimentosService;
        private static IInalantesTerminologyService _InalantesService;
        private static IContatoTerminologyService _ContatoService;

        private static IReacoesAdversasService _ReacoesAdversasService;
        private static IAlergiasService _AlergiasSihService;

        public static void SetService(ITiposDeReacaoTerminologyService TiposDeReacaoService)
        {
            _TiposDeReacaoService = TiposDeReacaoService;
        }

        public static void SetService(ICategoriasDeReacaoTerminologyService CategoriasDeReacaoService)
        {
            _CategoriasDeReacaoService = CategoriasDeReacaoService;
        }

        public static void SetService(IMedicamentosTerminologyService MedicamentosService)
        {
            _MedicamentosService = MedicamentosService;
        }

        public static void SetService(IReacoesAdversasService ReacoesAdversasService)
        {
            _ReacoesAdversasService = ReacoesAdversasService;
        }

        public static void SetService(IAlimentosTerminologyService AlimentosService)
        {
            _AlimentosService = AlimentosService;
        }

        public static void SetService(IInalantesTerminologyService InalantesService)
        {
            _InalantesService = InalantesService;
        }

        public static void SetService(IContatoTerminologyService ContatoService)
        {
            _ContatoService = ContatoService;
        }

        public static void SetService(IAlergiasService AlergiasSihService)
        {
            _AlergiasSihService = AlergiasSihService;
        }

        public static ITiposDeReacaoTerminologyService GetTiposDeReacaoService()
        {
            if (_TiposDeReacaoService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para tipos de reação");

            return _TiposDeReacaoService;
        }

        public static ICategoriasDeReacaoTerminologyService GetCategoriasDeReacaoService()
        {
            if (_CategoriasDeReacaoService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para categorias de reação");

            return _CategoriasDeReacaoService;
        }

        public static IMedicamentosTerminologyService GetMedicamentosService()
        {
            if (_MedicamentosService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para medicamentos");

            return _MedicamentosService;
        }

        public static IReacoesAdversasService GetReacoesAdversasService()
        {
            if(_ReacoesAdversasService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para reações adversas");

            return _ReacoesAdversasService;
        }

        public static IAlimentosTerminologyService GetAlimentosService()
        {
            if (_AlimentosService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para alimentos");

            return _AlimentosService;
        }

        public static IInalantesTerminologyService GetInalantesService()
        {
            if (_InalantesService== null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para inalantes");

            return _InalantesService;
        }

        public static IContatoTerminologyService GetContatoService()
        {
            if (_ContatoService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para contato");

            return _ContatoService;
        }

        public static IAlergiasService GetAlergiasSihService()
        {
            if (_AlergiasSihService == null)
                throw new InvalidOperationException("Não foi definido nenhum serviço para alergias do SIH");

            return _AlergiasSihService;
        }
    }
}
