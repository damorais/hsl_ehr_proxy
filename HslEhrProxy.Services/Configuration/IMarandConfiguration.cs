﻿namespace HslEhrProxy.Services.Configuration
{
    public interface IMarandConfiguration
    {
        string ServerUrl { get; }

        byte[] GetAsciiEncodedUsernamePasswordPair();
    }
}