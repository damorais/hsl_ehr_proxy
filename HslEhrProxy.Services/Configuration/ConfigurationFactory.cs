﻿using System.Text;

namespace HslEhrProxy.Services.Configuration
{
    public sealed class MarandConfiguration : IMarandConfiguration
    {
        public string ServerUrl { get; }
        private string Username { get; }
        private string Password { get; }

        public MarandConfiguration()
        {
            //TODO: Isto deverá ser movido para o arquivo de configurações
            ServerUrl = @"http://hslmarandh01:8081/rest/v1";
            Username = "admin";
            Password = "admin";
        }

        private string GetUsernamePasswordPair() => $"{Username}:{Password}";
        public byte[] GetAsciiEncodedUsernamePasswordPair() => Encoding.ASCII.GetBytes(GetUsernamePasswordPair());
    }


    public static class ConfigurationFactory
    {
        private static IMarandConfiguration _marandConfiguration;
        public static IMarandConfiguration GetMarandConfiguration()
        {
            return _marandConfiguration ?? (_marandConfiguration = new MarandConfiguration());
        }
    }
}
