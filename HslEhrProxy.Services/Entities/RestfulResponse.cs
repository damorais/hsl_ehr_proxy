﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslEhrProxy.Services.Entities
{
    public interface IRestfulIdentity
    {
    }

    public class RestfulIdentity<T> : IRestfulIdentity
    {
        public T Value { get; private set; }
        public RestfulIdentity(T Value)
        {
            this.Value = Value;
        }
    }

    public static class RestfulIdentityExtensions
    {
        public static RestfulIdentity<B> Bind<A, B>(this RestfulIdentity<A> a, Func<A, RestfulIdentity<B>> func) =>
            func(a.Value);

        private static RestfulIdentity<T> ToRestIdentity<T>(this T Item) => new RestfulIdentity<T>(Item);

        public static RestfulIdentity<C> SelectMany<A, B, C>(
            this RestfulIdentity<A> a, Func<A, RestfulIdentity<B>> func, Func<A, B, C> select) =>
                select(a.Value, a.Bind(func).Value).ToRestIdentity();
    }
}
