﻿using System;
using System.Net.Http;
using HslEhrProxy.Services.Configuration;

namespace HslEhrProxy.Services
{

    public static class MarandHttpClientFactory
    {
        public static HttpClient CreateHttpClient(IMarandConfiguration Configuration)
        {
            var httpClient = new HttpClient();

            var authorizationEncoded = 
                Convert.ToBase64String(Configuration.GetAsciiEncodedUsernamePasswordPair());

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic {authorizationEncoded}");

            return httpClient;
        }
        
    }
}