﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Helpers;
using System.Web.Mvc;
using HslEhrProxy.Services.Configuration;

namespace HslEhrProxy.Services.Services
{
    //TODO: Isto deixa de existir no novo cenário. Não preciso manipular o formulário
    public class MarandFormService
    {
        private readonly IMarandConfiguration _marandConfiguration;

        public MarandFormService(IMarandConfiguration MarandConfiguration)
        {
            _marandConfiguration = MarandConfiguration;
        }

        private string BuildBaseUrl() =>
            $"{_marandConfiguration.ServerUrl}/form";

        private string BuildUrl(string FormName, string FormVersion) => 
            $"{BuildBaseUrl()}/{FormName}/{FormVersion}";

        private string BuildUrl(string FormName, string FormVersion, string FormResource) =>
            $"{BuildUrl(FormName, FormVersion)}/resource/{FormResource}";            


        //TODO: Código está repetitivo. Precisa refatorar.
        public string GetFormResource(string FormName, string FormVersion, string FormResource)
        { 
            var urlTemplate = BuildUrl(FormName, FormVersion, FormResource);     

            string result;

            using (var client = MarandHttpClientFactory.CreateHttpClient(_marandConfiguration))
            {
                var response = client.GetAsync(urlTemplate).Result;
                var responseContent = response.Content.ReadAsStringAsync().Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        result = responseContent;
                        break;
                     case HttpStatusCode.BadRequest:
                        result = "{ \"Erro\": \"Unable to parse form version\"}";
                        break;
                    case HttpStatusCode.Forbidden:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                    case HttpStatusCode.NotFound:
                        result =
                            "{ \"Erro\": \"Form not found / Form version does not exists / Resource does not Exists\"}";
                        break;
                    case HttpStatusCode.Unauthorized:
                        result = 
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                   default:
                        var errorMessage =
                            JsonConvert
                                .DeserializeObject<Dictionary<string, object>>(
                                    responseContent
                                )["exceptionMessage"].ToString();
                        result =
                            "{ \"Erro:\": \"Failed to call the ThinkEhrServer: " + errorMessage + "\"";
                        throw new Exception(result); 
                }
            }

            return result;
        }

        public string GetForm(string FormName, string FormVersion)
        {
            var path = BuildUrl(FormName, FormVersion);

            string result;

            using (var client = MarandHttpClientFactory.CreateHttpClient(_marandConfiguration))
            {
                var response = client.GetAsync(path).Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        result = responseContent;
                        break;
                     case HttpStatusCode.BadRequest:
                        result = "{ \"Erro\": \"Unable to parse form version\"}";
                        break;
                    case HttpStatusCode.Forbidden:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                    case HttpStatusCode.NotFound:
                        result =
                            "{ \"Erro\": \"Form not found / Form version does not exists\"}";
                        break;
                    case HttpStatusCode.Unauthorized:
                        result = 
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                   default:
                        var errorMessage =
                            JsonConvert
                                .DeserializeObject<Dictionary<string, object>>(
                                    responseContent
                                )["exceptionMessage"].ToString();
                        result =
                            "{ \"Erro:\": \"Failed to call the ThinkEhrServer: " + errorMessage + "\"";
                        throw new Exception(result); 
                }
            }

            return result;
        }
    }
}
