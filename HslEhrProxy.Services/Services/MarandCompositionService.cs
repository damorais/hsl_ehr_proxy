﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using HslEhrProxy.Services.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HslEhrProxy.Services.Services
{
    public class MarandCompositionService
    {
        private readonly IMarandConfiguration _marandConfiguration;

        public MarandCompositionService(IMarandConfiguration MarandConfiguration)
        {
            _marandConfiguration = MarandConfiguration;
        }

        private string BuildBaseUrl() =>
            $"{_marandConfiguration.ServerUrl}/composition";

        public string SaveComposition(string TemplateId, Guid PatientEhrId, Dictionary<string, object> Composition)
        {
            //TODO: Preciso arranjar uma forma melhor de passar o composition. Passar como string não me parece legal
            //Preciso lidar com: CommiterName, CommiterId - Virão do serviço de segurança, provavelmente

            //composition?templateId=Alergias&ehrId=703239b3-fabc-4383-9be8-5c4340b7ea25&format=FLAT
            //TODO: Temporário
            //var PatientId = new Guid("703239b3-fabc-4383-9be8-5c4340b7ea25")

            var path = $"{BuildBaseUrl()}?templateId={TemplateId}&ehrId={PatientEhrId}&format=FLAT";

            string result;

            using (var client = MarandHttpClientFactory.CreateHttpClient(_marandConfiguration))
            {
                var response = client.PostAsJsonAsync(path, Composition).Result;
                var responseContent = response.Content.ReadAsStringAsync().Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.Created:
                        //TODO: Incluir leitura da resposta da criação, para obter o ID da compositon criada.
                        result = responseContent; 
                        //result = "{ \"Erro\": \"Composition has been created.\"}";
                        break;
                    case HttpStatusCode.NoContent:
                        result = "{ \"Erro\": \"Composition has been deleted.\"}";
                        //TODO: Ver a questão do link para a última versão ativa da composition(retorno da plataforma nesse caso)
                        break;
                    case HttpStatusCode.BadRequest:
                        result = "{ \"Erro\": \"Unable to parse format parameter.\"}";
                        break;
                    case HttpStatusCode.Forbidden:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request.\"}";
                        break;
                    case HttpStatusCode.NotFound:
                        result =
                            "{ \"Erro\": \"Composition with the given ID does not exists.\"}";
                        break;
                    case HttpStatusCode.Unauthorized:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request.\"}";
                        break;
                    default:
                        var errorMessage =
                            JsonConvert
                                .DeserializeObject<Dictionary<string, object>>(
                                    responseContent
                                )["exceptionMessage"].ToString();
                        result = "{ \"Erro:\": \"Failed to call the ThinkEhrServer: " + errorMessage + "\"";

                        throw new Exception(result);
                }
            }

            return result;
        }

        //TODO: Código está repetitivo. Precisa refatorar.
        public string GetComposition(Guid CompositionId)
        {
            //TODO:Ajustar o ID da composition para lidar com o dominio / versão da composition ::default::1
            var path = $"{BuildBaseUrl()}/{CompositionId.ToString()}?format=FLAT";

            string result;

            using (var client = MarandHttpClientFactory.CreateHttpClient(_marandConfiguration))
            {
                var response = client.GetAsync(path).Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        result = responseContent;
                        break;
                    case HttpStatusCode.NoContent:
                        result = "{ \"Erro\": \"Composition has been deleted.\"}";
                        //TODO: Ver a questão do link para a última versão ativa da composition(retorno da plataforma nesse caso)
                        break;
                    case HttpStatusCode.BadRequest:
                        result = "{ \"Erro\": \"Unable to parse format parameter \"}";
                        break;
                    case HttpStatusCode.Forbidden:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                    case HttpStatusCode.NotFound:
                        result =
                            "{ \"Erro\": \"Composition with the given ID does not exists \"}";
                        break;
                    case HttpStatusCode.Unauthorized:
                        result =
                            "{ \"Erro\": \"Forbidden = the user does not have the required permissions to execute this request\"}";
                        break;
                    default:
                        var errorMessage =
                            JsonConvert
                                .DeserializeObject<Dictionary<string, object>>(
                                    responseContent
                                )["exceptionMessage"].ToString();
                        result =
                            "{ \"Erro:\": \"Failed to call the ThinkEhrServer: " + errorMessage + "\"";
                        throw new Exception(result);
                }
            }

            return result;
        }
    }
}