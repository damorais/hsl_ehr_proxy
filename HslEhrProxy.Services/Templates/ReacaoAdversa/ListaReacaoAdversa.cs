﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslEhrProxy.Services.Archetypes.AdverseReaction;
using HslEhrProxy.Services.Templates.Basic;

namespace HslEhrProxy.Services.Templates.ReacaoAdversa
{
    public class ListaReacoesAdversas : ITemplate
    {
        //TODO: Verificar o que fazer - Afirmação de Exclusão
        public IEnumerable<AdverseReactionRisk> RiscosDeReacoesAdversas { get; private set; }
    }
}
