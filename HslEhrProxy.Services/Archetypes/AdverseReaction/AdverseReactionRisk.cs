﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslEhrProxy.Services.Basic;

namespace HslEhrProxy.Services.Archetypes.AdverseReaction
{
    public class EventoAdverso : ICluster
    {
        public AdverseReactionDamage DanoDaReacaoAoPaciente { get; private set; }
        //TODO Adicionar validação: Obrigatorieadade
        //TODO: Preciso especializar este campo
        public IEnumerable<SnomedCodedText> TiposDeReacao { get; private set; }
        //TODO Adicionar validação: Obrigatorieadade
        public string DescricaoReacao { get; private set; }
        //TODO Adicionar validação: Obrigatorieadade
        public DateTime DataInicioReacao { get; private set; }
        public SnomedCodedText ViaDeAdministracao  { get; private set; }
        public TimeSpan? TempoDeInfusao { get; private set; }
        public string IntervencaoClinicaImediata { get; private set; }
        public string ComentariosSobreEvento { get; private set; }

        public EventoAdverso(SnomedCodedText TipoDeReacao, 
            string DescricaoReacao, DateTime DataInicioReacao) :
            this(null, new List<SnomedCodedText>() { TipoDeReacao }, DescricaoReacao, DataInicioReacao, null, null, null, null)
        {

        }

        public EventoAdverso(IEnumerable<SnomedCodedText> TiposDeReacao,
            string DescricaoReacao, DateTime DataInicioReacao) :
            this(null, TiposDeReacao, DescricaoReacao, DataInicioReacao, null, null, null, null)
        {

        }

        public EventoAdverso(AdverseReactionDamage DanoAoPaciente, 
            IEnumerable<SnomedCodedText> TiposDeReacao, string DescricaoReacao,
            DateTime DataInicioReacao, SnomedCodedText ViaDeAdministracao,
            TimeSpan? TempoDeInfusao, string IntervencaoClinicaImediata, 
            string ComentarioSobreEvento)
        {
            if (TiposDeReacao == null || TiposDeReacao.Count() == 0)
                throw new ArgumentException("É preciso informar um ou mais tipos de reação");

            if(string.IsNullOrEmpty(DescricaoReacao))
                throw new ArgumentException("É preciso informar a descrição da reação.");

            if(DataInicioReacao == DateTime.MinValue)
                throw new ArgumentException("A reação precisa possuir uma data válida.");

            this.DanoDaReacaoAoPaciente = DanoAoPaciente;
            this.TiposDeReacao = TiposDeReacao;
            this.DescricaoReacao = DescricaoReacao;
            this.DataInicioReacao = DataInicioReacao;
            this.ViaDeAdministracao = ViaDeAdministracao;
            this.TempoDeInfusao = TempoDeInfusao;
            this.IntervencaoClinicaImediata = IntervencaoClinicaImediata;
            this.ComentariosSobreEvento = ComentariosSobreEvento;
        }

    }

    public class Resumo : ICluster
    {
        //TODO: Adicionar validação: Obrigatoriedade
        public DateTime DataNotificacao { get; private set; }
        public string Comentarios { get; set; }
    }

    public class DetalhesEvento : ICluster
    {
        public EventoAdverso EventoAdverso { get; private set; }
        //TODO: Adicionar validação: Obrigatoriedade
        //TODO: Criar tipo especializado. Por enquanto está aberto. Terminologia própria (lookup de medicamentos
        public IEnumerable<ICodedText> Substancia { get; private set; }
    }

    //TODO: Estudar sobre como tratar a cardinalidade
    //Cardinalidade: 0:N - Preciso entender como reproduzir isso na arquitetura de classes
    //A cardinalidade é reproduzida pelo número de registros ativos na base
    public class AdverseReactionRisk
    {
        public DetalhesEvento DetalhesEvento { get; private set; }

        //TODO: Adicionar validação: Obrigatoriedade
        //TODO: Categoria vem do SNOMED
        public ICodedText Categoria { get; private set; }
        //TODO: Adicionar validação: Obrigatoriedade
        public string Item { get; private set; }
        public Resumo Resumo { get; private set; }
        
        //TODO: Adicionar validação: Obrigatoriedade
        //A obrigatoriedade de bool é resolvida pelo tipo. Default: False
        public bool NotificarEventoAdverso { get; private set; }

        public AdverseReactionRisk(DetalhesEvento DetalhesEvento, ICodedText Categoria, string Item, Resumo Resumo, bool NotificarEventoAdverso = false)
        {
            #region Validação de argumentos
            if (DetalhesEvento == null)
                throw new ArgumentException("DetalhesEvento não pode ser nulo.");

            if(Categoria == null)
                throw new ArgumentException("Categoria não pode ser nula.");
            #endregion

            this.DetalhesEvento = DetalhesEvento;
            this.Categoria = Categoria;
            this.Item = Item;
            this.Resumo = Resumo;
            this.NotificarEventoAdverso = NotificarEventoAdverso;
        }
    }
}