﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using HslEhrProxy.Services.Basic;

namespace HslEhrProxy.Services.Archetypes.AdverseReaction
{
    /// <summary>
    /// Enum representando os internal codes possíveis para os internal codes 
    /// de classificação de dano
    /// </summary>
    public enum TipoClassificacaoDano
    {
        Nenhum = 1,
        Leve = 2,
        Moderado = 3,
        Grave = 4
    }

    /// <summary>
    /// Representação do internal code de classsificação de dano, 
    /// do arquétipo openEHR-EHR-CLUSTER.hsl_damage_adverse_reaction.v0
    /// Tipo: DV_CODED_TEXT - Com mapeamento para Internal Code
    /// </summary>
    public class ClassificacaoDanoInternalCode : InternalCode
    {
        //TODO: Colocar as strings como informações configuráveis.
        public static 
            new Dictionary<TipoClassificacaoDano, ClassificacaoDanoInternalCode> AllowedValues = 
                new Dictionary<TipoClassificacaoDano, ClassificacaoDanoInternalCode>()
                {
                    { TipoClassificacaoDano.Nenhum,
                        null },
                    { TipoClassificacaoDano.Leve,
                        new ClassificacaoDanoInternalCode("at0010", "Leve - Com ou sem necessidade de intervenção clínica") },
                    { TipoClassificacaoDano.Moderado,
                        new ClassificacaoDanoInternalCode("at0005", "Moderado - Com necessidade de intervenção clínica") },
                    { TipoClassificacaoDano.Grave,
                        new ClassificacaoDanoInternalCode("at0007", "Grave - Grave - Hemorragia, Broncoaspiração, Edema de Glote ...") }
                };

        private ClassificacaoDanoInternalCode(string Code, string Value) : base(Code, Value) { }

        public static ClassificacaoDanoInternalCode GetClassificacaoDano(TipoClassificacaoDano TipoClassificacaoDano)
        {
            if (!AllowedValues.ContainsKey(TipoClassificacaoDano))
                throw new ArgumentException($"O tipo de classificação não é válido. Verifique os valores aceitos pelo tipo. TipoClassificacaoDano {TipoClassificacaoDano}");
            else
                return AllowedValues[TipoClassificacaoDano];
        }
    }

    /// <summary>
    /// Representação em classe do Arquétipo DanoReacaoPaciente
    /// Arquétipo: openEHR-EHR-CLUSTER.hsl_damage_adverse_reaction.v0
    /// Criado em: 2016-04-05 por dmgmorais
    /// </summary>    
    public class AdverseReactionDamage : ICluster
    {
        public bool DanoParaOPaciente { get; private set; }

        public ClassificacaoDanoInternalCode ClassificacaoDano { get; private set; }

        public AdverseReactionDamage(bool DanoParaOPaciente) : this(DanoParaOPaciente, TipoClassificacaoDano.Nenhum) 
        {
        }

        public AdverseReactionDamage(bool DanoParaOPaciente, TipoClassificacaoDano TipoClassificacaoDano)
        {
            this.DanoParaOPaciente = DanoParaOPaciente;

            if (DanoParaOPaciente && TipoClassificacaoDano == TipoClassificacaoDano.Nenhum)
                throw new ArgumentException("Se há dano para o paciente, ClassificacaoDano não pode ser null");
            else
                this.ClassificacaoDano = ClassificacaoDanoInternalCode.GetClassificacaoDano(TipoClassificacaoDano);
        }

        public override string ToString() =>
            $"- Dano para o Paciente: {DanoParaOPaciente} \n - ClassificacaoDano: {ClassificacaoDano.ToString()} ";
    }
}