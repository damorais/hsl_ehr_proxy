﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslEhrProxy.Services.Basic
{
    public abstract class InternalCode : ICodedText
    {
        //TODO: Ver o que fazer com isso.
        public static Dictionary<int, InternalCode> AllowedValues;

        public string Code { get; private set; }

        public string Value { get; private set; }

        public InternalCode(string Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }

        public override string ToString() =>
            $"Code: { Code }, Value: {Value}";
    }
}
