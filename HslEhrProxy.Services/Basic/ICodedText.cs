﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslEhrProxy.Services.Basic
{
    public static class ICodedTextExtensions
    {
        public static string ToString(this ICodedText codedText) =>
            $"Code: {codedText.Code} - Value: {codedText.Value}";
    }

    public interface ICodedText
    {
        string Code { get; }       
        string Value { get; }
    }
}
