﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslEhrProxy.Services.Basic
{
    public class SnomedCodedText : ICodedText
    {
        public string Code { get; private set; }
        public string Value { get; private set; }

        public SnomedCodedText(string Code) : this(Code, string.Empty){ }
        public SnomedCodedText(string Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }
    }
}
