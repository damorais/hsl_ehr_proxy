﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hsl.Ehr.Domain.Abstract.Repositories;
using Hsl.Ehr.Domain.Entidades;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;

namespace Hsl.Ehr.Data.Repositories.ReacoesAdversas
{
    public class ReacoesAdversasRepositoryStatic : IReacoesAdversasRepository
    {
        private static List<ReacaoAdversa> _ReacoesAdversas;
        private static List<ReacaoAdversa> ReacoesAdversas
        {
            get
            {
                if (_ReacoesAdversas == null)
                {
                    _ReacoesAdversas = new List<ReacaoAdversa>();
                }

                return _ReacoesAdversas;
            }
        }

        public IEnumerable<ReacaoAdversa> GetAll(int IdPaciente)
        {
            return ReacoesAdversas;
        }

        public ReacaoAdversa Get(int IdPaciente, int IdReacaoAdversa)
        {
            return ReacoesAdversas.FirstOrDefault(e => e.IdReacaoAdversa == IdReacaoAdversa);
        }

        //TODO: Provavelmente não vai retornar a lista dos itens modificados.
        public IEnumerable<ReacaoAdversa> Add(int IdPaciente, IEnumerable<ReacaoAdversa> ReacoesAdversas)
        {
            var ultimoId = ReacoesAdversasRepositoryStatic.ReacoesAdversas.Count();

            var toAdd = new List<ReacaoAdversa>();

            foreach (var r in ReacoesAdversas)
            {
                if (r.IdReacaoAdversa != 0)
                    throw new Exception("Método usado para inclusão!");

                //ARGH!!!!
                ultimoId++;
                r.IdReacaoAdversa = ultimoId;

                toAdd.Add(r);
            }

            ReacoesAdversasRepositoryStatic.ReacoesAdversas.AddRange(toAdd);

            return toAdd;
        }
    }
}
