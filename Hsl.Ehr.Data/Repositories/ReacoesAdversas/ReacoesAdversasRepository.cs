﻿using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Util.AdvancedTypes;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System;
using Newtonsoft.Json;

namespace Hsl.Ehr.Data.Repositories.ReacoesAdversas
{
    internal class ReacoesAdversasRepository
    {
        private readonly IDbConnection _connection;
        private readonly IDbTransaction _transaction;
        private readonly string _retornarProcedureName;
        private readonly string _retornarAtivasPorPacienteProcedureName;
        private readonly string _retornarInativasPorPacienteProcedureName;
        private readonly string _retornarHistoricoProcedureName;
        private readonly string _adicionarProcedureName;
        private readonly string _adicionarSubstanciaProcedureName;
        private readonly string _adicionarTipoDeReacaoProcedureName;
        private readonly string _retornarSubstanciasProcedureName;
        private readonly string _inativarReacaoAdversaProcedureName;
        private readonly string _saveOnTEHRQueueProcedureName;
        private readonly string _bloquearParaEdicaoProcedureName;
        private readonly string _removerBloqueioEdicaoProcedureName;

        internal ReacoesAdversasRepository(IDbConnection Connection)
        {
            _connection = Connection;
            var packageName                             = "SYS_HSL.RES_REACAO_ADVERSA_PKG";
            _retornarProcedureName                      = $"{packageName}.Retornar";
            _retornarAtivasPorPacienteProcedureName     = $"{packageName}.RetornarAtivosPorPaciente";
            _retornarInativasPorPacienteProcedureName   = $"{packageName}.RetornarInativosPorPaciente";
            _retornarHistoricoProcedureName             = $"{packageName}.RetornarHistorico";
            _adicionarProcedureName                     = $"{packageName}.Adicionar";
            _adicionarSubstanciaProcedureName           = $"{packageName}.AdicionarSubstancia";
            _adicionarTipoDeReacaoProcedureName         = $"{packageName}.AdicionarTipoReacao";
            _retornarSubstanciasProcedureName           = $"{packageName}.RetornarSubstancias";
            _inativarReacaoAdversaProcedureName         = $"{packageName}.InativarReacaoEDocumento";
            _saveOnTEHRQueueProcedureName               = $"{packageName}.SaveOnTEhrQueue";
            _bloquearParaEdicaoProcedureName            = $"{packageName}.BloquearParaEdicao";
            _removerBloqueioEdicaoProcedureName         = $"{packageName}.RemoverBloqueioEdicao";
        }

        internal ReacoesAdversasRepository(IDbConnection Connection, IDbTransaction Transaction) : this(Connection)
        {
            _transaction = Transaction;
        }

        internal Option<ReacaoAdversa> Adicionar(Documento Documento, ReacaoAdversa ReacaoAdversa, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pCategoriaReacaoCode",      value: ReacaoAdversa.Categoria.Code,            
                dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pItem",                     value: ReacaoAdversa.Item,                      
                dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("pDescricao",                value: ReacaoAdversa.Descricao,                 
                dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("pNotificarEventoAdverso",   value: ReacaoAdversa.NotificarEventoAdverso ? 1 : 0,     
                dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pIdDocumento",              value: Documento.IdDocumento,                   
                dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pVersaoDocumento",          value: Documento.Versao,                        
                dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pUsuario",                  value: Usuario,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);

            if(ReacaoAdversa.IdReacaoOriginal == 0)
                parameters.Add("pIdReacaoOriginal", value: null,
                    dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            else
                parameters.Add("pIdReacaoOriginal", value: ReacaoAdversa.IdReacaoOriginal,
                    dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

            parameters.Add("ResultSet",                                                                 
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var dsReacaoAdversa = _connection.Query<dynamic>(
                sql: _adicionarProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            return dsReacaoAdversa != null ?
                Option.From(ConstruirReacaoAdversa(dsReacaoAdversa)) :
                Option.None<ReacaoAdversa>();
        }

        internal Option<ReacaoAdversa> InativarPorModificacao(int IdReacaoAdversa, int IdEvento, string Usuario) =>
            this.Inativar(IdReacaoAdversa, StatusDocumento.Modificado, IdEvento, Usuario);

        internal Option<ReacaoAdversa> Inativar(int IdReacaoAdversa, string Justificativa,
                int IdEvento, string Usuario) =>
            this.Inativar(IdReacaoAdversa, StatusDocumento.Inativo, IdEvento, Usuario, Justificativa);

        private Option<ReacaoAdversa> Inativar(int IdReacaoAdversa, StatusDocumento Status, int IdEvento,
            string Usuario, string Justificativa = null)
        {
            var parameters = new OracleDynamicParameters();

            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pJustificativa", value: Justificativa,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("pIdEventoInativacao", value: IdEvento,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("pStatusDestino", value: (int)Status,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsReacaoAdversa = _connection.Query(
                sql: _inativarReacaoAdversaProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            return rsReacaoAdversa != null ?
                Option.From(ConstruirReacaoAdversa(rsReacaoAdversa)) :
                Option.None<ReacaoAdversa>();
        }

        internal Option<Substancia> AdicionarSubstancia(int IdReacaoAdversa, Substancia Substancia)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pCodeSubstancia", value: Substancia.Code,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("pValueSubstancia", value: Substancia.Value,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("pTerminology", value: Substancia.Terminology,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
           
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var substancia = _connection.Query<Substancia>(
                sql: _adicionarSubstanciaProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return Option.From(substancia);
        }

        internal Option<TipoDeReacao> AdicionarTipoReacao(int IdReacaoAdversa, int CodeTipoReacao)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pCodeTipoReacao", value: CodeTipoReacao,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var tipoDeReacao = _connection.Query<TipoDeReacao>(
                sql: _adicionarTipoDeReacaoProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return Option.From(tipoDeReacao);
        }

        internal Option<ReacaoAdversa> Retornar(int IdReacaoAdversa)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var resultSet =
                 _connection.Query
                 (
                    sql: _retornarProcedureName,
                    param: parameters,
                    transaction: _transaction,
                    commandType: CommandType.StoredProcedure
                );

            return resultSet != null ?
                Option.From(ConstruirReacaoAdversa(resultSet)) :
                Option.None<ReacaoAdversa>();
        }

        internal IEnumerable<Substancia> RetornarSubstancias(int IdReacaoAdversa)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var substancias = _connection.Query<Substancia>(
                sql: _retornarSubstanciasProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            return (substancias == null) ? new List<Substancia>() : substancias;
        }

        internal IEnumerable<TipoDeReacao> RetornarTiposDeReacao(int IdReacaoAdversa)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var tiposDeReacao = _connection.Query<TipoDeReacao>(
                sql: _retornarSubstanciasProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            return (tiposDeReacao == null) ? new List<TipoDeReacao>() : tiposDeReacao;
        }

        internal IEnumerable<ReacaoAdversa> RetornarReacoesAtivasPorPaciente(int IdPaciente)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdPaciente", value: IdPaciente,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsReacoesAdversas = _connection.Query(
                sql: _retornarAtivasPorPacienteProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            var reacoesAdversas = new List<ReacaoAdversa>();

            if(rsReacoesAdversas != null)
            {
                var idsReacaoAdversa = rsReacoesAdversas.Select(item => item.REACAO_ID_REACAO_ADVERSA).Distinct().ToList();

                foreach(var idReacao in idsReacaoAdversa)
                    reacoesAdversas.Add(
                        ConstruirReacaoAdversa(rsReacoesAdversas.Where(item => item.REACAO_ID_REACAO_ADVERSA == idReacao))
                    );
            }

            return reacoesAdversas;
        }

        internal IEnumerable<ReacaoAdversa> RetornarReacoesInativasPorPaciente(int IdPaciente)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdPaciente", value: IdPaciente,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsReacoesAdversas = _connection.Query(
                sql: _retornarInativasPorPacienteProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            var reacoesAdversas = new List<ReacaoAdversa>();

            if (rsReacoesAdversas != null)
            {
                var idsReacaoAdversa = rsReacoesAdversas.Select(item => item.REACAO_ID_REACAO_ADVERSA).Distinct().ToList();

                foreach (var idReacao in idsReacaoAdversa)
                    reacoesAdversas.Add(
                        ConstruirReacaoAdversa(rsReacoesAdversas.Where(item => item.REACAO_ID_REACAO_ADVERSA == idReacao))
                    );
            }

            return reacoesAdversas;
        }

        internal IEnumerable<ReacaoAdversa> RetornarHistorico(int IdReacaoAdversa)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsReacoesAdversas = _connection.Query(
                sql: _retornarHistoricoProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure
            );

            var reacoesAdversas = new List<ReacaoAdversa>();

            if (rsReacoesAdversas != null)
            {
                var idsReacaoAdversa = rsReacoesAdversas.Select(item => item.REACAO_ID_REACAO_ADVERSA).Distinct().ToList();

                foreach (var idReacao in idsReacaoAdversa)
                    reacoesAdversas.Add(
                        ConstruirReacaoAdversa(rsReacoesAdversas.Where(item => item.REACAO_ID_REACAO_ADVERSA == idReacao))
                    );
            }

            return reacoesAdversas;
        }

        internal Option<ReacaoAdversa> BloquearParaEdicao(int IdReacaoAdversa, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var resultSet =
                 _connection.Query
                 (
                    sql: _bloquearParaEdicaoProcedureName,
                    param: parameters,
                    transaction: _transaction,
                    commandType: CommandType.StoredProcedure
                );

            return resultSet != null ?
                Option.From(ConstruirReacaoAdversa(resultSet)) :
                Option.None<ReacaoAdversa>();
        }

        internal Option<ReacaoAdversa> RemoverBloqueioEdicao(int IdReacaoAdversa, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var resultSet =
                 _connection.Query
                 (
                    sql: _removerBloqueioEdicaoProcedureName,
                    param: parameters,
                    transaction: _transaction,
                    commandType: CommandType.StoredProcedure
                );

            return resultSet != null ?
                Option.From(ConstruirReacaoAdversa(resultSet)) :
                Option.None<ReacaoAdversa>();
        }

        internal bool SaveOnTEHRQueue(
            int IdReacaoAdversa, int IdReacaoAdversaOriginal, int IdDocumento, int VersaoDocumento, 
            int IdStatus, Dictionary<string, object> SerializedReacaoAdversaComposition, 
            string IdPacienteTEHR, string Usuario)
        {
            var composition = SerializedReacaoAdversaComposition != null ?
                JsonConvert.SerializeObject(SerializedReacaoAdversaComposition):
                null;

            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdReacaoAdversa",  value: IdReacaoAdversa,                     
                dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pIdReacaoAdversaOriginal", value: IdReacaoAdversaOriginal,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdDocumento", value: IdDocumento,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pVersaoDocumento", value: VersaoDocumento,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdStatus",         value: IdStatus,                            
                dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("pTEhrData",         value: composition,  
                dbType: OracleDbType.Clob,      direction: ParameterDirection.Input);
            parameters.Add("pIdPacienteTEhr",   value: IdPacienteTEHR.ToString() ,          
                dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("pUsuario",          value: Usuario,                             
                dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("ResultSet", 
                dbType: OracleDbType.RefCursor,     direction: ParameterDirection.Output);

            var resultSet = _connection.Execute(
                sql: _saveOnTEHRQueueProcedureName,
                param: parameters,
                transaction: _transaction,
                commandType: CommandType.StoredProcedure);

            return true;
        }

        private Documento ConstruirDocumento(IEnumerable<dynamic> RsDocumento) =>
            RsDocumento.Select(item => new Documento(
                        (int)item.DOCUMENTO_ID_DOCUMENTO,
                        (int)item.DOCUMENTO_VERSAO,
                        (TipoDocumento)item.DOCUMENTO_ID_TIPO_DOCUMENTO,
                        item.DOCUMENTO_DATA_CRIACAO,
                        item.DOCUMENTO_USUARIO_CRIACAO,
                        item.DOCUMENTO_DATA_ALTERACAO,
                        item.DOCUMENTO_USUARIO_ALTERACAO,
                        item.DOCUMENTO_DATA_BLOQUEIO,
                        item.DOCUMENTO_USUARIO_BLOQUEIO,
                        (StatusDocumento)item.DOCUMENTO_ID_STATUS,
                        (int)item.DOCUMENTO_ID_EVENTO,
                        (int)item.DOCUMENTO_ID_PACIENTE)).FirstOrDefault();

        private IEnumerable<TipoDeReacao> ConstruirTiposDeReacao(IEnumerable<dynamic> RsTiposDeReacao) =>
            RsTiposDeReacao.Select(
                item => new TipoDeReacao((int)item.TIPO_REACAO_CODE, item.TIPO_REACAO_VALUE));

        private IEnumerable<Substancia> ConstruirSubstancias(IEnumerable<dynamic> RsSubstancias) =>
            RsSubstancias.Select(
                item => new Substancia(item.SUBSTANCIA_CODE, item.SUBSTANCIA_VALUE, item.SUBSTANCIA_TERMINOLOGY));

        private CategoriaDaReacao ConstruirCategoria(IEnumerable<dynamic> RsCategoria) =>
            RsCategoria.Select(
                item => new CategoriaDaReacao((int)item.CATEGORIA_REACAO_CODE, item.CATEGORIA_REACAO_VALUE))
            .FirstOrDefault();

        private ReacaoAdversa ConstruirReacaoAdversa(IEnumerable<dynamic> RsReacaoAdversa)
        {
            var Documento = ConstruirDocumento(
                RsReacaoAdversa.Select(item => new {
                    item.REACAO_ID_REACAO_ADVERSA,
                    item.DOCUMENTO_ID_DOCUMENTO,
                    item.DOCUMENTO_VERSAO,
                    item.DOCUMENTO_ID_TIPO_DOCUMENTO,
                    item.DOCUMENTO_USUARIO_CRIACAO,
                    item.DOCUMENTO_DATA_CRIACAO,
                    item.DOCUMENTO_USUARIO_ALTERACAO,
                    item.DOCUMENTO_DATA_ALTERACAO,
                    item.DOCUMENTO_USUARIO_BLOQUEIO,
                    item.DOCUMENTO_DATA_BLOQUEIO,
                    item.DOCUMENTO_ID_STATUS,
                    item.DOCUMENTO_ID_EVENTO,
                    item.DOCUMENTO_ID_PACIENTE
                }).Distinct());

            var Categoria = ConstruirCategoria(
                RsReacaoAdversa.Select(
                    item => new { item.REACAO_ID_REACAO_ADVERSA, item.CATEGORIA_REACAO_CODE, item.CATEGORIA_REACAO_VALUE })
                .Distinct());

            var Substancias = ConstruirSubstancias(
                RsReacaoAdversa.Where(item => item.SUBSTANCIA_CODE != null).Select(
                    item => new { item.REACAO_ID_REACAO_ADVERSA, item.SUBSTANCIA_CODE, item.SUBSTANCIA_VALUE, item.SUBSTANCIA_TERMINOLOGY })
                .Distinct());

            var TiposDeReacao = ConstruirTiposDeReacao(
               RsReacaoAdversa.Where(r => r.TIPO_REACAO_CODE != null).Select(
                   r => new { r.REACAO_ID_REACAO_ADVERSA, r.TIPO_REACAO_CODE, r.TIPO_REACAO_VALUE })
               .Distinct());

            return RsReacaoAdversa.Select(item => new {
                item.REACAO_ID_REACAO_ADVERSA,
                item.REACAO_ITEM,
                item.REACAO_DESCRICAO,
                item.REACAO_NOT_EVENTO_ADVERSO,
                item.REACAO_ID_STATUS,
                item.REACAO_JUST_INATIVACAO,
                item.REACAO_DATA_CRIACAO,
                item.REACAO_USUARIO_CRIACAO,
                item.REACAO_DATA_ALTERACAO,
                item.REACAO_USUARIO_ALTERACAO,
                item.REACAO_ID_REACAO_ORIGINAL
            }).Distinct().Select(item => new ReacaoAdversa(
                (int)item.REACAO_ID_REACAO_ADVERSA, Documento, Categoria,
                item.REACAO_ITEM, Substancias, TiposDeReacao,
                item.REACAO_DESCRICAO, ((int)item.REACAO_NOT_EVENTO_ADVERSO != 0), 
                (StatusDocumento)item.REACAO_ID_STATUS, item.REACAO_JUST_INATIVACAO,
                item.REACAO_DATA_CRIACAO, item.REACAO_USUARIO_CRIACAO, 
                item.REACAO_DATA_ALTERACAO, item.REACAO_USUARIO_ALTERACAO,
                (int)item.REACAO_ID_REACAO_ORIGINAL)).FirstOrDefault();
        }
    }
}