﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Abstract.Repositories.Drafts;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;
using System.Collections.Generic;

namespace Hsl.Ehr.Data.Repositories.ReacoesAdversas
{
    public class ReacoesAdversasDraftsRepository : IReacoesAdversasDraftsRepository
    {
        private IDbConnection Connection;
        private IDbTransaction Transaction;
        private readonly string PackageName;
        private readonly string RetornarPorPacienteProcedureName;
        private readonly string RetornarTodosPorPacienteProcedureName;
        private readonly string AdicionarProcedureName;
        private readonly string AtualizarProcedureName;
        private readonly string ExcluirProcedureName;
        private readonly string RetornarProcedureName;
        

        public ReacoesAdversasDraftsRepository(IDbConnection Connection)
        {
            this.Connection = Connection;
            PackageName                             = "SYS_HSL.RES_REACAO_ADVERSA_DRAFT_PKG";
            RetornarProcedureName                   = $"{PackageName}.Retornar";
            RetornarPorPacienteProcedureName        = $"{PackageName}.RetornarPorPaciente";
            RetornarTodosPorPacienteProcedureName   = $"{PackageName}.RetornarTodosPorPaciente";
            AdicionarProcedureName                  = $"{PackageName}.Adicionar";
            AtualizarProcedureName                  = $"{PackageName}.Atualizar";
            ExcluirProcedureName                    = $"{PackageName}.Excluir";
        }

        public ReacoesAdversasDraftsRepository(IDbConnection Connection, IDbTransaction Transaction) : this(Connection)
        {
            this.Transaction = Transaction;
        }

        private ReacoesAdversasDraft NewFromResult(dynamic DraftQueryResult)
        {
            ReacoesAdversasDraft result = new ReacoesAdversasDraft();

            if (DraftQueryResult != null)
            {
                var conteudoRascunho = JsonConvert.DeserializeObject<ConteudoReacoesAdversasDraft>(DraftQueryResult.CONTEUDO);

                result = new ReacoesAdversasDraft(
                    IdDraft: (int)DraftQueryResult.IDDRAFT,
                    Conteudo: conteudoRascunho,
                    DataCriacao: (DateTime)DraftQueryResult.DATACRIACAO,
                    DataAlteracao: (DateTime)DraftQueryResult.DATAALTERACAO,
                    Usuario: (string)DraftQueryResult.USUARIO,
                    IdPaciente: (int)DraftQueryResult.IDPACIENTE
                );
            }

            return result;
        }

        public ReacoesAdversasDraft Retornar(int IdReacaoAdversaDraft)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDraft", value: IdReacaoAdversaDraft, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rascunho = Connection.Query(
                sql: RetornarProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return NewFromResult(rascunho);
        }

        public ReacoesAdversasDraft RetornarPorPaciente(int IdPaciente, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdPaciente",   value: IdPaciente,  dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pUsuario",      value: Usuario,     dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("ResultSet",                         dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rascunho = Connection.Query(
                sql: RetornarPorPacienteProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();
            
            return NewFromResult(rascunho); 
        }

        public IEnumerable<ReacoesAdversasDraft> RetornarTodosPorPaciente(int IdPaciente)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdPaciente", value: IdPaciente, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsRascunhos = Connection.Query(
                sql: RetornarTodosPorPacienteProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            var rascunhos = new List<ReacoesAdversasDraft>();

            if (rsRascunhos != null)
            {
                foreach(var linha in rsRascunhos)
                    rascunhos.Add(NewFromResult(linha));
            }

            return rascunhos;
        }

        //TODO: Deverá ser modificado o tipo do parâmetro
        public int Adicionar(ReacoesAdversasDraft Draft, string Usuario, int IdPaciente)
        {
            var conteudoJsonString = JsonConvert.SerializeObject(Draft.Conteudo);

            var parameters = new OracleDynamicParameters();
            parameters.Add("pConteudo",      value: conteudoJsonString, dbType: OracleDbType.Clob,      direction: ParameterDirection.Input);
            parameters.Add("pUsuario",       value: Usuario,            dbType: OracleDbType.Varchar2,  direction: ParameterDirection.Input);
            parameters.Add("pIdPaciente",    value: IdPaciente,         dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("rId",                                       dbType: OracleDbType.Int32,     direction: ParameterDirection.Output);

            Connection.Execute(
                sql: AdicionarProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure);

            return parameters.Get<OracleDecimal>("rId").ToInt32();
        }

        //TODO: Deverá ser modificado o tipo
        public ReacoesAdversasDraft Atualizar(int IdReacoesAdversasDraft, ReacoesAdversasDraft Draft)
        {
            var conteudoJsonString = JsonConvert.SerializeObject(Draft.Conteudo);
            
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDraft",   value: IdReacoesAdversasDraft,     dbType: OracleDbType.Int32,     direction: ParameterDirection.Input);
            parameters.Add("pConteudo",  value: conteudoJsonString,         dbType: OracleDbType.Clob,      direction: ParameterDirection.Input);
            parameters.Add("ResultSet",                                     dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            
            var rascunho = Connection.Query(
                sql: AtualizarProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return NewFromResult(rascunho);
        }   
        
        public int Excluir(int IdReacoesAdversasDraft)
        {
            //TODO: Adicionar tratamento para falha de exclusão
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDraft", value: IdReacoesAdversasDraft, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

            int result = Connection.Execute(
                sql: ExcluirProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure);

            return result;
        }  
    }
}
