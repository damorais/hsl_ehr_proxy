﻿using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Util.AdvancedTypes;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;

namespace Hsl.Ehr.Data.Repositories
{
    internal class DocumentosRepository
    {
        private IDbConnection Connection;
        private IDbTransaction Transaction;
        private readonly string PackageName;
        private readonly string CriarNovoProcedureName;
        private readonly string CriarNovaVersaoProcedureName;
        private readonly string InativarProcedureName;
        private readonly string RetornarPorVersaoProcedureName;
       
        internal DocumentosRepository(IDbConnection Connection)
        {
            this.Connection = Connection;

            PackageName                     = "SYS_HSL.RES_DOCUMENTO_PKG";
            CriarNovoProcedureName          = $"{PackageName}.CriarNovo";
            CriarNovaVersaoProcedureName    = $"{PackageName}.CriarNovaVersao";
            InativarProcedureName           = $"{PackageName}.Inativar";
            RetornarPorVersaoProcedureName  = $"{PackageName}.RetornarPorVersao";
        }

        internal DocumentosRepository(IDbConnection Connection, IDbTransaction Transaction) : this(Connection)
        {
            this.Transaction = Transaction;
        }

        public Option<Documento> CriarNovo(TipoDocumento TipoDocumento, int IdPaciente, int IdEvento, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdTipoDocumento",  value: (int)TipoDocumento,  dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdPaciente",       value: IdPaciente,          dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdEvento",         value: IdEvento,            dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario",          value: Usuario,             dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",                                     dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var documento = Connection.Query<Documento>(
                sql: CriarNovoProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return documento.AsOption();
        }

        public Option<Documento> CriarNovaVersao(Documento Documento, int IdEvento, string Usuario)
        {
            var novaVersao = Documento.Versao + 1;

            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDocumento", value: Documento.IdDocumento, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pNovaVersao", value: novaVersao, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdTipoDocumento", value: (int)Documento.TipoDocumento, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdPaciente", value: Documento.IdPaciente, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdEvento", value: IdEvento, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var documento = Connection.Query<Documento>(
                sql: CriarNovaVersaoProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return documento.AsOption();
        }
        
        public Option<Documento> Inativar(int IdDocumento, int Versao, string Usuario)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDocumento", value: IdDocumento, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pVersao", value: Versao, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var documento = Connection.Query<Documento>(
                sql: InativarProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return documento.AsOption();
        }

        public Option<Documento> RetornarPorVersao(int IdDocumento, int Versao)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdDocumento", value: IdDocumento, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pVersao", value: Versao, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var documento = Connection.Query<Documento>(
                sql: RetornarPorVersaoProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return documento.AsOption();
        }
    }
}
