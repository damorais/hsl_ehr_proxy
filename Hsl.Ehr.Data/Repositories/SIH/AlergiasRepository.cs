﻿using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.SIH;
using Hsl.Ehr.Util.AdvancedTypes;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Data.Repositories.SIH
{
    public class AlergiasRepository
    {
        private IDbConnection Connection;
        private IDbTransaction Transaction;
        private readonly string PackageName;
        private readonly string MarcarParaRemocaoSihProcedureName;
        private readonly string RetornarAlergiasATranscreverProcedureName;
        private readonly string RetornarNumeroSequenciaSIHParaReacaoProcedureName;

        public AlergiasRepository(IDbConnection Connection)
        {
            this.Connection = Connection;
            PackageName = "SYS_HSL.RES_ALERGIAS_SIH_PKG";
            MarcarParaRemocaoSihProcedureName = $"{PackageName}.MarcarParaRemocaoSih";
            RetornarAlergiasATranscreverProcedureName = $"{PackageName}.RetornarAlergiasATranscrever";
            RetornarNumeroSequenciaSIHParaReacaoProcedureName = $"{PackageName}.RetornarNrSequenciaSIH";
        }

        public AlergiasRepository(IDbConnection Connection, IDbTransaction Transaction) : this(Connection)
        {
            this.Transaction = Transaction;
        }

        public Option<dynamic> MarcarParaRemocaoSIH(int IdEvento, int SequenciaSIH, int IdPaciente, string Usuario)
        {
            var parameters = new OracleDynamicParameters();

            parameters.Add("pIdEvento", value: IdEvento,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pNrSequenciaSIH", value: SequenciaSIH,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pIdPaciente", value: IdPaciente,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pUsuario", value: Usuario,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var result = Connection.Query(
                sql: MarcarParaRemocaoSihProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return Option.From(result);
        }

        public IEnumerable<AlergiaSIH> RetornarAlergiasATranscrever(int IdPaciente)
        {
            var parameters = new OracleDynamicParameters();

            parameters.Add("pIdPaciente", value: IdPaciente,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var result = Connection.Query<AlergiaSIH>(
                sql: RetornarAlergiasATranscreverProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).ToList();

            return result;
        }

        public IEnumerable<int> RetornarNumeroSequenciaSIHParaReacao(int IdReacaoAdversa)
        {
            var parameters = new OracleDynamicParameters();

            parameters.Add("pIdReacaoAdversa", value: IdReacaoAdversa,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var result = Connection.Query<int>(
                sql: RetornarNumeroSequenciaSIHParaReacaoProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            return result; 
        }
    }
}
