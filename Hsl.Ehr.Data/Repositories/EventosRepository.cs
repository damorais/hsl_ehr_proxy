﻿using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Util.AdvancedTypes;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Data.Repositories
{
    public class EventosRepository
    {
        private IDbConnection Connection;
        private IDbTransaction Transaction;
        private readonly string PackageName;
        private readonly string CriarProcedureName;

        public EventosRepository(IDbConnection Connection)
        {
            this.Connection = Connection;
            PackageName = "SYS_HSL.RES_EVENTO_PKG";
            CriarProcedureName = $"{PackageName}.Criar";
        }

        public EventosRepository(IDbConnection Connection, IDbTransaction Transaction) : this(Connection)
        {
            this.Transaction = Transaction;
        }

        public Option<Evento> Criar(TipoEvento TipoEvento, string Descricao, int? NumeroAtendimentoSIH)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pIdTipoEvento", value: (int)TipoEvento,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("pDescricao", value: Descricao,
                dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);

            if (NumeroAtendimentoSIH.HasValue)
                parameters.Add("pNumeroAtendimento", value:  NumeroAtendimentoSIH.Value, 
                    dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            else
                parameters.Add("pNumeroAtendimento", value: null,
                    dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

            parameters.Add("ResultSet",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var rsEvento = Connection.Query<dynamic>(
                sql: CriarProcedureName,
                param: parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            return rsEvento != null ?
                Option.From(ConstruirEvento(rsEvento)) :
                Option.None<Evento>();
        }

        private Evento ConstruirEvento(IEnumerable<dynamic> RsEvento) =>
            RsEvento.Select(item => new Evento(
                (int)item.EVENTO_ID_EVENTO,
                item.EVENTO_DESCRICAO,
                (TipoEvento)item.EVENTO_ID_TIPO_EVENTO,
                item.EVENTO_DATA_CRIACAO,
                (int?)item.EVENTO_NR_ATENDIMENTO_SIH)).FirstOrDefault();
    }
}
