﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Util.AdvancedTypes;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories
{
    public class Paciente
    {
        public int PacienteId { get; set; }
        public string Nome { get; set; }
        public int CodigoSIH { get; set; }
        public string Prontuario { get; set; }
        public string EhrId { get; set; }

        public override string ToString() =>
            $"ID: {PacienteId}, Nome: {Nome}, CodigoSIH: {CodigoSIH}, Prontuario: {Prontuario}, EhrID: {EhrId}";
    }

    public class PacientesRepository
    {
        private readonly string UrlProvedorDemografia; // = $"http://sihassistencialservicesimulacao.hsl.org.br:1000/Pacientes/ID_{IdPaciente}";
        private readonly IDbConnection Connection;

        public PacientesRepository(string UrlProvedorDemografia)
        {
            this.UrlProvedorDemografia = UrlProvedorDemografia;
        }

        public PacientesRepository(IDbConnection Connection, string UrlProvedorDemografia) : this(UrlProvedorDemografia)
        {
            this.Connection = Connection;
        }

        private Option<Paciente> BuscarPacienteServicoDemografico(int IdPaciente)
        {
            //TODO: Mover para configurações
            var urlDestino = $"{UrlProvedorDemografia}/Pacientes/ID_{IdPaciente}";//$"http://sihassistencialservicesimulacao.hsl.org.br:1000/Pacientes/ID_{IdPaciente}";

            Option<Paciente> result;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                var response = client.GetAsync(urlDestino).Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        result = JsonConvert.DeserializeObject<Paciente>(responseContent).AsOption();
                        break;
                    case HttpStatusCode.NotFound:
                        result = Option.None<Paciente>();
                        break;
                    default:
                        throw new Exception(responseContent);
                }
            }

            return result;
        }

        private Option<Paciente> BuscarPacienteBancoDeDados(int IdPaciente)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pPacienteId", value: IdPaciente,
                dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

            parameters.Add("rPaciente",
                dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var paciente = Connection.Query<Paciente>(
                sql: "SYS_HSL.HSL_DEM_PACIENTE_PKG.RetornarPacientePorId",
                param: parameters,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            return paciente != null ? 
                Option.From(paciente) : 
                Option.None<Paciente>();
        }

        public Option<Paciente> BuscarPaciente(int IdPaciente)
        {
            var paciente = (this.Connection != null)?
                this.BuscarPacienteBancoDeDados(IdPaciente) :
                Option.None<Paciente>();

            if (Option.IsNone(paciente))
                paciente = this.BuscarPacienteServicoDemografico(IdPaciente);

            return paciente;
        }
    }
}
