﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    internal class InalantesRepository : OracleTerminologyRepository<Inalante>,
        ISnomedTerminologyRepository<Inalante>
    {
        public InalantesRepository(IDbConnection Connection) :
            base(Connection, "SYS_HSL.TRM_INALANTE")
        {
        }

        public new IEnumerable<Inalante> GetAll()
        {
            return base.GetAll();
        }

        public Inalante GetByCode(int Code)
        {
            return GetByCode(Code, OracleDbType.Int32);
        }
    }
}
