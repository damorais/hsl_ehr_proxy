﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    class ContatoRepository : OracleTerminologyRepository<Contato>,
        ISnomedTerminologyRepository<Contato>
    {
        public ContatoRepository(IDbConnection Connection) :
            base(Connection, "SYS_HSL.TRM_CONTATO")
        {
        }

        public new IEnumerable<Contato> GetAll()
        {
            return base.GetAll();
        }

        public Contato GetByCode(int Code)
        {
            return GetByCode(Code, OracleDbType.Int32);
        }
    }
}
