﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    public abstract class OracleTerminologyRepository<T> where T : ICodedText
    {
        private IDbConnection Connection;
        private readonly string TableName;
        private readonly string PackageName;
        private readonly string GetAllProcedureName;
        private readonly string GetByCodeProcedureName;
        private readonly string GetByValueProcedureName;

        internal OracleTerminologyRepository(IDbConnection Connection, string TableName) :
            this(Connection, TableName, $"{TableName}_PKG")
        {
        }

        internal OracleTerminologyRepository(IDbConnection Connection, string TableName, string PackageName)
        {
            this.Connection = Connection;
            this.TableName = TableName;
            this.PackageName = PackageName;
            GetAllProcedureName = $"{PackageName}.GetAll";
            GetByCodeProcedureName = $"{PackageName}.GetByCode";
            GetByValueProcedureName = $"{PackageName}.GetByValue";
        }

        internal IEnumerable<T> GetAll()
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<T>(
                sql: GetAllProcedureName, 
                param: parameters, 
                commandType: CommandType.StoredProcedure
            );
        }

        internal IEnumerable<T> GetByValue(string Value)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pValue", value: Value, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<T>(
                sql: GetByValueProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            );
        }

        internal T GetByCode<U>(U Code, OracleDbType DbType)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pCode", value: Code, dbType: DbType, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<T>(
                sql: GetByCodeProcedureName,
                param: parameters, 
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();
        }
    }
}
