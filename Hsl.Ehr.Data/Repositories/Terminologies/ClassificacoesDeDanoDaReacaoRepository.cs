﻿using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    internal class ClassificacoesDeDanoDaReacaoRepository : 
        OracleTerminologyRepository<ClassificacaoDeDanoDaReacao>, 
        IInternalTerminologyRepository<ClassificacaoDeDanoDaReacao>
    {
        public ClassificacoesDeDanoDaReacaoRepository(IDbConnection Connection) :base(Connection, "SYS_HSL.CLASSIFICACAO_DANO_REACAO")
        {
        }

        public new IEnumerable<ClassificacaoDeDanoDaReacao> GetAll()
        {
            return base.GetAll();
        }

        public ClassificacaoDeDanoDaReacao GetByCode(string Code)
        {
            return GetByCode(Code, OracleDbType.Varchar2);
        }
    }
}
