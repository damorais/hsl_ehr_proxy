﻿using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    //TODO: Tornar a query/procedure configurável externamente. Assim é possível redirecionar a procedure sem que seja necessário um deploy
    internal class TiposDeReacaoRepository :
        OracleTerminologyRepository<TipoDeReacao>,
        ISnomedTerminologyRepository<TipoDeReacao>
    {
        public TiposDeReacaoRepository(IDbConnection Connection) :
            base(Connection, "SYS_HSL.TRM_TIPO_REACAO")
        {
        }

        public new IEnumerable<TipoDeReacao> GetAll()
        {
            return base.GetAll();
        }

        public TipoDeReacao GetByCode(int Code)
        {
            return GetByCode(Code, OracleDbType.Int32);
        }
    }
}
