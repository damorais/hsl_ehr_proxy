﻿using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    internal class CategoriasDeReacaoRepository : 
        OracleTerminologyRepository<CategoriaDaReacao>, 
        ISnomedTerminologyRepository<CategoriaDaReacao>
    {
        public CategoriasDeReacaoRepository(IDbConnection Connection) : 
            base(Connection, "SYS_HSL.TRM_CATEGORIA_REACAO")
        {
        }

        public new IEnumerable<CategoriaDaReacao> GetAll()
        {
            return base.GetAll();
        }

        public CategoriaDaReacao GetByCode(int Code)
        {
            return GetByCode(Code, OracleDbType.Int32);
        }
    }
}
