﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Hsl.Ehr.Data.Util;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Hsl.Ehr.Domain.Entities.ValueObjects.Terminology;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    internal class MedicamentosRepository
    {
        private IDbConnection Connection;

        private readonly string PackageName;
        private readonly string GetAllProcedureName;
        private readonly string GetByCodeProcedureName;
        private readonly string GetSubstanciasByCodeProcedureName;
        private readonly string GetByNomeProcedureName;
        private readonly string GetSubstanciaByDCBProcedureName;

        public MedicamentosRepository(IDbConnection Connection)
        {
            this.Connection = Connection;
            PackageName                         = "SYS_HSL.TRM_MEDICAMENTOS_REACAO_PKG";
            GetAllProcedureName                 = $"{PackageName}.GetAll";
            GetByCodeProcedureName              = $"{PackageName}.GetByCode";
            GetSubstanciasByCodeProcedureName   = $"{PackageName}.GetSubstanciasByCode";
            GetByNomeProcedureName              = $"{PackageName}.GetByNome";
            GetSubstanciaByDCBProcedureName     = $"{PackageName}.GetSubstanciaByDCB";
        }

        public IEnumerable<MedicamentoResumido> GetAll()
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<Medicamento>(
                sql: GetAllProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            ).Select(m => new MedicamentoResumido(m.Code, m.Value)); 
        }

        public IEnumerable<MedicamentoResumido> GetByValue(string Nome)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pNome", value: Nome, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<Medicamento>(
                sql: GetByNomeProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            ).Select(m => new MedicamentoResumido(m.Code, m.Value));
        }

        public Medicamento GetByCode(int Code)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pCode", value: Code, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var medicamento = Connection.Query<Medicamento>(
                sql: GetByCodeProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

            if(medicamento != null)
                medicamento = medicamento.InjectSubstancias(
                    this.GetSubstanciasByCode(medicamento.Code)
                );

            return medicamento;
        }

        public Substancia GetSubstanciaByDCB(string DCB)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pDCB", value: DCB, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<Substancia>(
                sql: GetSubstanciaByDCBProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();
        }

        private IEnumerable<Substancia> GetSubstanciasByCode(int Code)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("pCode_Medicamento", value: Code, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("ResultSet", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return Connection.Query<Substancia>(
                GetSubstanciasByCodeProcedureName,
                param: parameters,
                commandType: CommandType.StoredProcedure
            );
        }
    }
}
