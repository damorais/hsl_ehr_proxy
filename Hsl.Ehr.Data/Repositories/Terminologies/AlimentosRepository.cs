﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Abstract.Repositories.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Repositories.Terminologies
{
    internal class AlimentosRepository : OracleTerminologyRepository<Alimento>,
        ISnomedTerminologyRepository<Alimento>
    {
        public AlimentosRepository(IDbConnection Connection) :
            base(Connection, "SYS_HSL.TRM_ALIMENTO")
        {
        }

        public new IEnumerable<Alimento> GetAll()
        {
            return base.GetAll();
        }

        public Alimento GetByCode(int Code)
        {
            return GetByCode(Code, OracleDbType.Int32);
        }
    }
}
