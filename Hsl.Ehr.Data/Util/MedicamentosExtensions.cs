﻿using System.Collections.Generic;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Data.Util
{
    internal static class MedicamentosExtensions
    {
        internal static Medicamento InjectSubstancias(this Medicamento Medicamento, IEnumerable<Substancia> Substancias)
        {
            return new Medicamento(
                Code: Medicamento.Code,
                Value: Medicamento.Value,
                Substancias: (Substancias != null ? Substancias : new List<Substancia>())
            );
        }
    }
}