﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Data.Marand
{
    public static class MarandTransformations
    {
        internal static Dictionary<string, object> AsMarandAdverseReactionCompositionAbsence(this ReacaoAdversa ReacaoAdversa) =>
           new Dictionary<string, object>()
            {
                { "ctx/language", "pt" },
                { "ctx/territory", "BR" },
                { "ctx/composer_name", $"{ReacaoAdversa.UsuarioAlteracao}|enfermagem"},
                { "ctx/time", DateTime.Now },
                {
                    "lista_de_reações_adversas", new Dictionary<string, object>()
                    {
                        {
                            "afirmação_de_alergias", new Dictionary<string, object>()
                            {
                                {  "afirmação_da_exclusão", ReacaoAdversa.Item }
                            }
                        }
                    }
                }
            };

        internal static Dictionary<string, object> AsMarandAdverseReactionCompositionPresence(this ReacaoAdversa ReacaoAdversa) =>
            new Dictionary<string, object>()
            {
                { "ctx/language", "pt" },
                { "ctx/territory", "BR" },
                { "ctx/composer_name", $"{ReacaoAdversa.UsuarioAlteracao}|enfermagem"},
                { "ctx/time", DateTime.Now },
                {
                    "lista_de_reações_adversas", new List<Dictionary<string, object>>()
                    {
                        new Dictionary<string, object>()
                        {
                            {
                                "risco_de_reação_adversa_alergias", new List<Dictionary<string, object>>()
                                {
                                    new Dictionary<string, object>()
                                    {
                                        { "categoria",  ReacaoAdversa.Categoria.AsMarandPropertyItem() },
                                        { "item",       ReacaoAdversa.Item },
                                        {
                                            "detalhes_do_evento", new Dictionary<string, object>()
                                            {
                                                {
                                                    "substancia", ReacaoAdversa.Substancias.AsMarandPropertyItem()
                                                },
                                                {
                                                    "evento_adverso", new Dictionary<string, object>()
                                                    {
                                                        { "tipo_de_reação", ReacaoAdversa.TiposDeReacao.AsMarandPropertyItem() },
                                                        { "descrição_da_reação", ReacaoAdversa.Descricao },
                                                        { "data_início_da_reação", ReacaoAdversa.DataCriacao.AsMarandPropertyItemOnlyDate() },
                                                    }
                                                }
                                            }

                                        },
                                        { "notificar_evento_adverso", ReacaoAdversa.NotificarEventoAdverso }
                                    }
                                }
                            }
                        }
                    }
                }
            };

        public static Dictionary<string, object> AsMarandAdverseReactionComposition(this ReacaoAdversa ReacaoAdversa) =>
            ReacaoAdversa.Categoria.Code != (int)CategoriasDaReacaoEnum.NegaAlergias ?
                ReacaoAdversa.AsMarandAdverseReactionCompositionPresence() :
                ReacaoAdversa.AsMarandAdverseReactionCompositionAbsence();

        public static Dictionary<string, string> AsMarandPropertyItem(this ISnomedCodedText CodedText) =>
            new Dictionary<string, string>()
            {
                { "|code", CodedText.Code.ToString() },
                { "|value", CodedText.Value }
            };

        public static IEnumerable<Dictionary<string, string>> AsMarandPropertyItem(this IEnumerable<ISnomedCodedText> CodedTextList) =>
            CodedTextList.Select(e => e.AsMarandPropertyItem());

        public static Dictionary<string, string> AsMarandPropertyItem(this IInternalCode InternalCode) =>
            new Dictionary<string, string>()
            {
                { "|code", InternalCode.Code.ToString() },
                { "|value", InternalCode.Value }
            };

        public static IEnumerable<Dictionary<string, string>> AsMarandPropertyItem(this IEnumerable<IInternalCode> InternalCodeList) =>
            InternalCodeList.Select(e => new Dictionary<string, string>()
            {
                { "|code", e.Code.ToString() },
                { "|value", e.Value }
            });

        public static Dictionary<string, string> AsMarandPropertyItem(this ISubstancia ExternalCode) =>
            new Dictionary<string, string>()
            {
                { "|code", ExternalCode.Code.ToString() },
                { "|value", ExternalCode.Value },
                { "|terminology", ExternalCode.Terminology }
            };

        public static IEnumerable<Dictionary<string, string>> AsMarandPropertyItem(this IEnumerable<ISubstancia> ExternalCodeList) =>
            ExternalCodeList.Select(e => e.AsMarandPropertyItem());

        public static string AsMarandPropertyItemOnlyDate(this DateTime Date) => Date.ToString("yyyy-MM-dd");
    }
}
