﻿using Hsl.Ehr.Data.Repositories.ReacoesAdversas;
using Hsl.Ehr.Domain.Abstract.Factories;
using Hsl.Ehr.Domain.Abstract.Repositories;

namespace Hsl.Ehr.Data.Factories
{
    public class ReacoesAdversasRepositoryFactory : IReacoesAdversasRepositoryFactory
    {
        public IReacoesAdversasRepository Create()
        {
            return new ReacoesAdversasRepositoryStatic();
        }
    }
}
