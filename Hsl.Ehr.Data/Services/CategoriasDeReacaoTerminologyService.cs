﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Services
{
    public class CategoriasDeReacaoTerminologyService : ICategoriasDeReacaoTerminologyService
    {
        private readonly string ConnectionString;
        private readonly bool CachedData;
        private Dictionary<int, CategoriaDaReacao> Cache;

        public CategoriasDeReacaoTerminologyService(string ConnectionString, bool CachedData)
        {
            this.ConnectionString = ConnectionString;
            this.CachedData = CachedData;

            if (CachedData)
                this.LoadData();
        }

        private void LoadData()
        {
            IEnumerable<CategoriaDaReacao> categoriasDeReacao = this.GetAllFromRepository();

            this.Cache = new Dictionary<int, CategoriaDaReacao>();

            foreach (var tipoDeReacao in categoriasDeReacao)
                this.Cache.Add(tipoDeReacao.Code, tipoDeReacao);
        }

        private IEnumerable<CategoriaDaReacao> GetAllFromRepository()
        {
            IEnumerable<CategoriaDaReacao> categoriasDeReacao;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                categoriasDeReacao = new CategoriasDeReacaoRepository(connection).GetAll();

            return categoriasDeReacao;
        }

        private CategoriaDaReacao GetByCodeFromRepository(int Code)
        {
            CategoriaDaReacao categoria;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                categoria = new CategoriasDeReacaoRepository(connection).GetByCode(Code);

            return categoria;
        }


        private IEnumerable<CategoriaDaReacao> GetAllFromCache() => Cache.Values;

        private CategoriaDaReacao GetByCodeFromCache(int Code) => Cache.ContainsKey(Code) ? Cache[Code] : null;


        public IEnumerable<CategoriaDaReacao> GetAll() => CachedData ? Cache.Values : GetAllFromRepository();

        public CategoriaDaReacao GetByCode(int Code) => this.CachedData ? this.GetByCodeFromCache(Code) : this.GetByCodeFromRepository(Code);

    }
}
