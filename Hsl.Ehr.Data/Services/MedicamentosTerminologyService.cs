﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Hsl.Ehr.Domain.Entities.ValueObjects.Terminology;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Services
{
    public class MedicamentosTerminologyService : IMedicamentosTerminologyService
    {
        private readonly string ConnectionString;
        public MedicamentosTerminologyService(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }       

        public IEnumerable<MedicamentoResumido> GetAll()
        {
            IEnumerable<MedicamentoResumido> medicamentos;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                medicamentos = new MedicamentosRepository(connection).GetAll();

            return medicamentos;
        }

        public IEnumerable<MedicamentoResumido> GetByValue(string Value)
        {
            IEnumerable<MedicamentoResumido> medicamentos;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                medicamentos = new MedicamentosRepository(connection).GetByValue(Value);

            return medicamentos;
        }

        public Medicamento GetByCode(int Code)
        {
            Medicamento medicamento;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                medicamento = new MedicamentosRepository(connection).GetByCode(Code);

            return medicamento;
        }

        public Substancia GetSubstanciaByDCB(string DCB)
        {
            Substancia substancia;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                substancia = new MedicamentosRepository(connection).GetSubstanciaByDCB(DCB);

            return substancia;
        }
    }
}
