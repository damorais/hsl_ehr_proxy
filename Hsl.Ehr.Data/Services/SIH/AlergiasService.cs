﻿using Hsl.Ehr.Data.Repositories.SIH;
using Hsl.Ehr.Domain.Abstract.Services.SIH;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.SIH;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Data.Services.SIH
{
    public class AlergiasService : IAlergiasService
    {
        private readonly string ConnectionString;
        public AlergiasService(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }

        public IEnumerable<AlergiaSIH> RecuperarAlergiasNaoTranscritas(int IdPaciente)
        {
            IEnumerable<AlergiaSIH> alergias;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                alergias = new AlergiasRepository(connection).RetornarAlergiasATranscrever(IdPaciente);

            return alergias;
        }
    }
}
