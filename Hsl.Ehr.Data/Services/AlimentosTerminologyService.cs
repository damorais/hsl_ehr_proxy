﻿using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Hsl.Ehr.Data.Services
{
    public class AlimentosTerminologyService : IAlimentosTerminologyService
    {
        private readonly string ConnectionString;
        private readonly bool CachedData;
        private Dictionary<int, Alimento> Cache;

        public AlimentosTerminologyService(string ConnectionString, bool CachedData)
        {
            this.ConnectionString = ConnectionString;
            this.CachedData = CachedData;

            if (CachedData)
                this.LoadData();
        }

        private void LoadData()
        {
            IEnumerable<Alimento> alimentos = this.GetAllFromRepository();

            Cache = new Dictionary<int, Alimento>();

            foreach (var alimento in alimentos)
                this.Cache.Add(alimento.Code, alimento);
        }

        private IEnumerable<Alimento> GetAllFromRepository()
        {
            IEnumerable<Alimento> alimentos;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                alimentos = new AlimentosRepository(connection).GetAll();

            return alimentos;
        }

        private Alimento GetByCodeFromRepository(int Code)
        {
            Alimento alimento;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                alimento = new AlimentosRepository(connection).GetByCode(Code);

            return alimento;
        }

        private IEnumerable<Alimento> GetByValueFromRepository(string Value)
        {
            IEnumerable<Alimento> alimentos;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                alimentos = new AlimentosRepository(connection).GetByValue(Value);

            return alimentos;
        }

        private IEnumerable<Alimento> GetAllFromCache() => Cache.Values;

        private Alimento GetByCodeFromCache(int Code) => Cache.ContainsKey(Code) ? Cache[Code] : null;

        private IEnumerable<Alimento> GetByValueFromCache(string Value) => Cache.Values.Where(a => a.Value.ToUpper().Contains(Value.ToUpper()));

        public IEnumerable<Alimento> GetAll() => CachedData ? Cache.Values : GetAllFromRepository();
        
        public Alimento GetByCode(int Code) => this.CachedData ? this.GetByCodeFromCache(Code) : this.GetByCodeFromRepository(Code);

        public IEnumerable<Alimento> GetByValue(string Value) => this.CachedData ? this.GetByValueFromCache(Value) : this.GetByValueFromRepository(Value); 
    }
}
