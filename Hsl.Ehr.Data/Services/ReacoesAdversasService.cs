﻿using System.Data;
using Hsl.Ehr.Domain.Abstract.Services.Drafts;
using Oracle.ManagedDataAccess.Client;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;
using Hsl.Ehr.Data.Repositories.ReacoesAdversas;
using Hsl.Ehr.Data.Repositories;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Util.AdvancedTypes;
using System.Linq;
using System;
using System.Collections.Generic;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Data.Repositories.SIH;
using Hsl.Ehr.Data.Marand;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Configuration;

namespace Hsl.Ehr.Data.Services
{
    //TODO: deverá ir para domain. O construtor deverá receber o repositório a ser utilizado.
    public class ReacoesAdversasService : IReacoesAdversasService
    {
        private readonly string ConnectionString;
        private readonly string UrlProvedorDemografia;

        public ReacoesAdversasService(string ConnectionString, string UrlProvedorDemografia)
        {
            this.ConnectionString = ConnectionString;
            this.UrlProvedorDemografia = UrlProvedorDemografia;
        }

        public int AdicionarDraft(ReacoesAdversasDraft Draft, string Usuario, int IdPaciente)
        {
            int idNovoDraft;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                //Atribui um código temporário para as novas reações que não possuem o código atribuído
                if (Draft.Conteudo != null)
                    Draft.Conteudo.NovasReacoes.ToList().ForEach(r =>
                        r.CodigoTemporario = string.IsNullOrEmpty(r.CodigoTemporario) ?
                            r.CodigoTemporario = $"TMP_{Guid.NewGuid()}" :
                            r.CodigoTemporario
                    );

                idNovoDraft = new ReacoesAdversasDraftsRepository(connection).Adicionar(Draft, Usuario, IdPaciente);
            }

            return idNovoDraft;
        }

        public void AtualizarDraft(int IdReacoesAdversasDraft, ReacoesAdversasDraft DraftAtualizado, string Usuario)
        {
            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                var reacaoAdversaRepository = new ReacoesAdversasRepository(connection, transaction);
                var draftsRepository = new ReacoesAdversasDraftsRepository(connection, transaction);

                var draftOriginal = draftsRepository.Retornar(IdReacoesAdversasDraft);

                var isSuccessful = true;
                //TODO: Adicionar controle: Só permitir bloqueio se o usuário for o mesmo
                //Desbloqueia todos os documentos presentes na versão original do rascunho
                if (draftOriginal != null && draftOriginal.Conteudo != null)
                {
                    var idsAtualizadas = draftOriginal.Conteudo.ReacoesAtualizadas.Select(r => r.IdReacaoAdversa);
                    var idsInativadas = draftOriginal.Conteudo.ReacoesInativadas.Select(r => r.IdReacao);
                    var IdsReacoesADesbloquear = idsAtualizadas.Union(idsInativadas).ToList();

                    var resultadoRemocaoDesbloqueio = IdsReacoesADesbloquear.Select(
                        idReacao => reacaoAdversaRepository.RemoverBloqueioEdicao(idReacao, Usuario)
                    ).ToList();

                    isSuccessful = (resultadoRemocaoDesbloqueio.Where(r => Option.IsNone(r)).FirstOrDefault() == null);
                }

                //Insere o bloqueio em todos os documentos no rascunho
                if(isSuccessful && DraftAtualizado.Conteudo != null)
                {
                    //Atribui um código temporário para as novas reações que não possuem o código atribuído
                    if (DraftAtualizado.Conteudo != null)
                        DraftAtualizado.Conteudo.NovasReacoes.ToList().ForEach(r =>
                            r.CodigoTemporario = string.IsNullOrEmpty(r.CodigoTemporario) ?
                                r.CodigoTemporario = $"TMP_{Guid.NewGuid()}" :
                                r.CodigoTemporario);

                    DraftAtualizado.Conteudo.ReacoesAtualizadas = AjustarAtualizadas(DraftAtualizado.Conteudo.ReacoesAtualizadas, reacaoAdversaRepository, Usuario);

                    var idsAtualizadas = DraftAtualizado.Conteudo.ReacoesAtualizadas.Select(r => r.IdReacaoAdversa);
                    var idsInativadas = DraftAtualizado.Conteudo.ReacoesInativadas.Select(r => r.IdReacao);
                    var IdsReacoesABloquear = idsAtualizadas.Union(idsInativadas).ToList();

                    var resultadoRemocaoDesbloqueio = IdsReacoesABloquear.Select(
                        idReacao => reacaoAdversaRepository.BloquearParaEdicao(idReacao, Usuario)
                    ).ToList();

                    isSuccessful = (resultadoRemocaoDesbloqueio.Where(r => Option.IsNone(r)).FirstOrDefault() == null);
                }

                if (isSuccessful)
                {
                    //TODO: O atualizar deve ter algum retorno para assegurar o sucesso da operação
                    draftsRepository.Atualizar(IdReacoesAdversasDraft, DraftAtualizado);
                    transaction.Commit();

                }
                else
                {
                    transaction.Rollback();
                }
            }
        }

        private IEnumerable<ReacaoAdversa> AjustarAtualizadas(IEnumerable<ReacaoAdversa> ReacoesModificadas, ReacoesAdversasRepository ReacoesAdversasRepository, string Usuario)
        {
            var reacoesAtualizadas = new List<ReacaoAdversa>();

            foreach(var reacaoModificada in ReacoesModificadas)
            {
                var reacaoOriginalOption = ReacoesAdversasRepository.Retornar(reacaoModificada.IdReacaoAdversa);

                if (!Option.IsNone(reacaoOriginalOption))
                {
                    var reacaoOriginal = Option.Some(reacaoOriginalOption);

                    reacaoOriginal.TiposDeReacao = reacaoModificada.TiposDeReacao;
                    reacaoOriginal.Descricao = reacaoModificada.Descricao;
                    reacaoOriginal.DataAlteracao = DateTime.Now;
                    reacaoOriginal.UsuarioAlteracao = Usuario;
                    reacoesAtualizadas.Add(reacaoOriginal);
                }
            }

            return reacoesAtualizadas;
        }

        public Try<string> ExcluirDraft(int IdReacoesAdversasDraft, string Usuario)
        {
            Try<string> result;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                var draftRepository = new ReacoesAdversasDraftsRepository(connection, transaction);
                var reacaoAdversaRepository = new ReacoesAdversasRepository(connection, transaction);

                var draft = draftRepository.Retornar(IdReacoesAdversasDraft);

                if (draft.Usuario != null && draft.Usuario == Usuario)
                {
                    draftRepository.Excluir(IdReacoesAdversasDraft);

                    var resultadoDesbloqueioAtualizacao = draft.Conteudo.ReacoesAtualizadas.Select(reacaoAtualizada =>
                        reacaoAdversaRepository.RemoverBloqueioEdicao(reacaoAtualizada.IdReacaoAdversa, Usuario)
                    ).ToList();

                    var resultadoDesbloqueioInativacao = draft.Conteudo.ReacoesInativadas.Select(reacaoInativada =>
                        reacaoAdversaRepository.RemoverBloqueioEdicao(reacaoInativada.IdReacao, Usuario)
                    ).ToList();

                    var isSuccessful =
                    (
                        resultadoDesbloqueioAtualizacao.Where(r => Option.IsNone(r)).FirstOrDefault() == null &&
                        resultadoDesbloqueioInativacao.Where(r => Option.IsNone(r)).FirstOrDefault() == null
                    );

                    if (isSuccessful)
                    {
                        transaction.Commit();
                        result = new Success<string>("Rascunho de reação adversa excluído");
                    }
                    else
                    {
                        transaction.Rollback();
                        result = new Failure<string>(new Exception("Não foi possível reverter o bloqueio dos documentos"));
                    }
                }
                else
                {
                    result = new Failure<string>(new UnauthorizedAccessException("Usuário não autorizado. Rascunho pertence a outro usuário ou não foi encontrado"));
                }
            }
            return result;
        }

        public ReacoesAdversasDraft RetornarDraftPorPaciente(int IdPaciente, string Usuario)
        {
            ReacoesAdversasDraft draft;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                draft = new ReacoesAdversasDraftsRepository(connection).RetornarPorPaciente(IdPaciente, Usuario);

            return draft;
        }

        public IEnumerable<ReacaoAdversa> RetornarNovasReacoesPorOutrosEmRascunho(int IdPaciente, string Usuario)
        {
            var resultado = new List<ReacaoAdversa>();
             
            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                var drafts = new ReacoesAdversasDraftsRepository(connection).RetornarTodosPorPaciente(IdPaciente);


                foreach(var rascunho in drafts)
                {
                    if(rascunho.Usuario != Usuario && rascunho.Conteudo != null)
                    {
                        var novasReacoes = rascunho.Conteudo.NovasReacoes.Select(nr => nr.ReacaoAdversa).ToList();
                        novasReacoes.ForEach(reacao =>
                        {
                            reacao.Documento = new Documento();
                            reacao.Documento.UsuarioBloqueio = rascunho.Usuario;
                            reacao.Documento.DataBloqueio = rascunho.DataAlteracao;
                        });

                        resultado.AddRange(novasReacoes);
                    }
                }
            }
                
            return resultado;
        }

        public IEnumerable<ReacaoAdversa> RetornarReacoesAtivasPorPaciente(int IdPaciente)
        {
            IEnumerable<ReacaoAdversa> reacoes;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                reacoes = new ReacoesAdversasRepository(connection).RetornarReacoesAtivasPorPaciente(IdPaciente);

            return reacoes;
        }

        public IEnumerable<ReacaoAdversa> RetornarReacoesInativasPorPaciente(int IdPaciente)
        {
            IEnumerable<ReacaoAdversa> reacoes;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                reacoes = new ReacoesAdversasRepository(connection).RetornarReacoesInativasPorPaciente(IdPaciente);

            return reacoes;
        }

        public IEnumerable<ReacaoAdversa> RetornarHistoricoReacao(int IdReacaoAdversa)
        {
            IEnumerable<ReacaoAdversa> reacoes;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                reacoes = new ReacoesAdversasRepository(connection).RetornarHistorico(IdReacaoAdversa);

            return reacoes;
        }

        public Option<ReacaoAdversa> RetornarReacaoAdversa(int IdPaciente, int IdReacaoAdversa)
        {
            Option<ReacaoAdversa> reacao;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                reacao = new ReacoesAdversasRepository(connection).Retornar(IdReacaoAdversa);

            return reacao;
        }

        public bool PromoverDraft(int IdReacaoAdversaDraft, string Usuario, int? NumeroAtendimentoSIH)
        {
            //TODO: Preciso refatorar melhorando o tratamento de erros e as operações a serem executadas
            //Recupero o rascunho
            ReacoesAdversasDraft draft;

            IEnumerable<ReacaoAdversa> reacoesModificadas = new List<ReacaoAdversa>();

            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                draft = new ReacoesAdversasDraftsRepository(connection).Retornar(IdReacaoAdversaDraft);
            }
                
            using (IDbConnection connection = new OracleConnection(ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                var eventoRepository        = new EventosRepository(connection, transaction);
                var docRepository           = new DocumentosRepository(connection, transaction);
                var reacoesRepository       = new ReacoesAdversasRepository(connection, transaction);
                var alergiasSIHRepository   = new AlergiasRepository(connection, transaction);

                //Cria o evento ao qual as alterações feitas no rascunho estarão vinculadas
                var eventoCriado = eventoRepository.Criar(TipoEvento.RegistroReacaoAdversa, "Registro de reação adversa", NumeroAtendimentoSIH);

                //Inclui as novas reações no repositório principal
                var reacoesAdicionadas =
                    draft.Conteudo.NovasReacoes.Select(novaReacao =>
                        AdicionarReacao(draft, docRepository, reacoesRepository, novaReacao.ReacaoAdversa, eventoCriado, Usuario)).ToList();

                //Desativa a versão anterior da reação e insere uma nova com as alterações efetuadas
                var reacoesAlteradas =
                    draft.Conteudo.ReacoesAtualizadas.Select(reacaoAlterada =>
                        AtualizarReacao(draft, docRepository, reacoesRepository, reacaoAlterada, eventoCriado, Usuario)).ToList();

                //Inativa as reações no repositório principal
                var reacoesInativadas =
                    draft.Conteudo.ReacoesInativadas.Select(reacaoAInativar =>
                        InativarReacao(reacoesRepository, reacaoAInativar.IdReacao, reacaoAInativar.Justificativa, eventoCriado, Usuario)).ToList();

                //Adiciona para a lista de inativações no SIH as reações inativadas,
                //a versão anterior das atualizadas e as marcadas como validadas
                var statusMarcadasParaInativacaoSIH = this.InativarReacoesSIH(draft, alergiasSIHRepository, eventoCriado, draft.IdPaciente, Usuario);

                var registrosAfetados = new ReacoesAdversasDraftsRepository(connection, transaction).Excluir(draft.IdDraft);

                var isSuccessful = 
                    (
                        reacoesAdicionadas.Where(r => Option.IsNone(r)).FirstOrDefault() == null &&
                        reacoesAlteradas.Where(r => Option.IsNone(r)).FirstOrDefault() == null &&
                        reacoesInativadas.Where(r => Option.IsNone(r)).FirstOrDefault() == null &&
                        !statusMarcadasParaInativacaoSIH.Contains(false)
                    );

                if (isSuccessful)
                    transaction.Commit();
                else
                    transaction.Rollback();

                if (isSuccessful)
                {
                    Task.Factory.StartNew(() =>
                       SendToMarand(draft.IdPaciente, Usuario, reacoesAdicionadas, reacoesAlteradas, reacoesInativadas) 
                    );
                }

                return isSuccessful;
            }
        }

        private async Task SendToMarand(int IdPaciente, string Usuario, IEnumerable<Option<ReacaoAdversa>> ReacoesAdicionadas, IEnumerable<Option<ReacaoAdversa>> ReacoesAlteradas, IEnumerable<Option<ReacaoAdversa>> ReacoesInativadas)
        {
            var caminhoLog = Configuracoes.RetornarConfiguracao(AppSettings.CaminhoLogThinkEHR);

            try
            {
                using (IDbConnection connection = new OracleConnection(ConnectionString))
                {
                    var paciente = new PacientesRepository(connection, this.UrlProvedorDemografia).BuscarPaciente(IdPaciente);

                    if (Option.IsNone(paciente))
                        throw new ArgumentException("Paciente não encontrado!");

                    var reacoesRepository = new ReacoesAdversasRepository(connection);

                    if (ReacoesAdicionadas.Where(r => Option.IsNone(r)).FirstOrDefault() == null)
                    {
                        var resultado = ReacoesAdicionadas
                            .Select(reacao => 
                                SaveOnMarand(reacoesRepository, Option.Some(reacao), 1, Option.Some(paciente).EhrId, Usuario))
                            .ToList();

                        if (resultado.Contains(false))
                            throw new Exception("Falha ao sincronizar com a Marand - Adicionadas");
                    }

                    if (ReacoesAlteradas.Where(r => Option.IsNone(r)).FirstOrDefault() == null)
                    {
                        var resultado = ReacoesAlteradas
                            .Select(reacao => 
                                SaveOnMarand(reacoesRepository, Option.Some(reacao), 2, Option.Some(paciente).EhrId, Usuario))
                            .ToList();

                        if (resultado.Contains(false))
                            throw new Exception("Falha ao sincronizar com a Marand - Alteradas");
                    }

                    if (ReacoesInativadas.Where(r => Option.IsNone(r)).FirstOrDefault() == null)
                    {
                        var outroRepo = reacoesRepository = new ReacoesAdversasRepository(connection);

                        var resultado = ReacoesInativadas
                            .Select(reacao => 
                                SaveOnMarand(reacoesRepository, Option.Some(reacao), 3, Option.Some(paciente).EhrId, Usuario))
                            .ToList();

                        if (resultado.Contains(false))
                            throw new Exception("Falha ao sincronizar com a Marand - Inativadas");
                    }
                }
            }
            catch (Exception ex)
            {
                var valores = $"Adicionadas: {ReacoesAdicionadas.Count()}, Alteradas: {ReacoesAlteradas.Count()}, Removidas: {ReacoesInativadas.Count()}";
                var nomeArquivo = $"erros_marand_{DateTime.Now}.txt"
                    .Replace("/", "_")
                    .Replace("\\", "_")
                    .Replace(" ", "_")
                    .Replace(":", "_");
                nomeArquivo = $@"{caminhoLog}\{nomeArquivo}";

                System.IO.File.AppendAllText(nomeArquivo, $"------------------------------ \n Falha ao sincronizar com a Marand: {DateTime.Now} \n");
                System.IO.File.AppendAllText(nomeArquivo, $"Paciente: {IdPaciente} / Usuário: {Usuario}\n");
                System.IO.File.AppendAllText(nomeArquivo, $"Valores: {valores}\n");
                System.IO.File.AppendAllText(nomeArquivo, $"{ex.Message}\n");
            }
        }

        private bool SaveOnMarand(ReacoesAdversasRepository reacoesRepository, ReacaoAdversa reacao, int IdStatus, string IdPacienteTEHR, string Usuario)
        {
            var r = reacoesRepository.Retornar(reacao.IdReacaoAdversa);
            if (!Option.IsNone(r))
            {
                return reacoesRepository.SaveOnTEHRQueue(
                            IdReacaoAdversa: Option.Some(r).IdReacaoAdversa,
                            IdReacaoAdversaOriginal: Option.Some(r).IdReacaoOriginal,
                            IdDocumento: Option.Some(r).Documento.IdDocumento,
                            VersaoDocumento: Option.Some(r).Documento.Versao,
                            IdStatus: IdStatus,
                            SerializedReacaoAdversaComposition: IdStatus == 3 ? null : Option.Some(r).AsMarandAdverseReactionComposition(),
                            IdPacienteTEHR: IdPacienteTEHR,
                            Usuario: Usuario
                        );
            }
            else
                return false;
        }

        private List<bool> InativarReacoesSIH(ReacoesAdversasDraft Draft, AlergiasRepository AlergiasSIHRepository, Option<Evento> EventoCriado, int IdPaciente, string Usuario)
        {
            //Recupera os NRSEQUENCIASIH equivalentes à reação atualizada
            var nrSequenciaAlteradasAInativarEmSIH =
                Draft.Conteudo.ReacoesAtualizadas.SelectMany(
                    reacaoAlterada => AlergiasSIHRepository.RetornarNumeroSequenciaSIHParaReacao(reacaoAlterada.IdReacaoAdversa)
                ).ToList();

            //Recupera os NRSEQUENCIASIH equivalentes à reação inativada
            var nrSequenciaInativadasAInativarEmSIH =
                Draft.Conteudo.ReacoesInativadas.SelectMany(
                    reacaoInativada => AlergiasSIHRepository.RetornarNumeroSequenciaSIHParaReacao(reacaoInativada.IdReacao)
                ).ToList();

            //Normaliza a lista de reações adversas validadas para uma lista com apenas os NRSEQUENCIASIH
            var nrSequenciaValidadasAInativarEmSIH = Draft.Conteudo.ReacoesValidadas.Select(reacaoValidada =>
                reacaoValidada.NrSequenciaSIH
            ).ToList();

            //Junta em uma única lista os NRSEQUENCIASIH a ser inativados
            var nrSequenciaAInativarSIH =
                nrSequenciaAlteradasAInativarEmSIH
                .Union(nrSequenciaInativadasAInativarEmSIH)
                .Union(nrSequenciaValidadasAInativarEmSIH).ToList();

            //Inativa as reações adversas no SIH
            var statusMarcadasParaInativacaoSIH = nrSequenciaAInativarSIH.Select(nrSIHReacaoAlterada =>
                InserirEmPassadasALimpo(EventoCriado, AlergiasSIHRepository, nrSIHReacaoAlterada, IdPaciente, Usuario)
            ).ToList();

            return statusMarcadasParaInativacaoSIH;
        }


        private bool InserirEmPassadasALimpo(Option<Evento> Evento, AlergiasRepository AlergiasSIHRepository, int SequenciaSIH, int IdPaciente, string Usuario)
        {
            var resultado = false;
            if (!Option.IsNone(Evento))
                resultado = IsSuccessfulOperation(
                    AlergiasSIHRepository.MarcarParaRemocaoSIH(Option.Some(Evento).IdEvento, SequenciaSIH, IdPaciente, Usuario)
                );

            return resultado;
        }

        private Option<ReacaoAdversa> InativarReacao(ReacoesAdversasRepository ReacoesRepository, int IdReacaoAdversa, string Justificativa, Option<Evento> Evento, string Usuario)
        {
            var resultado = Option.None<ReacaoAdversa>();

            if (!Option.IsNone(Evento))
                resultado = ReacoesRepository.Inativar(IdReacaoAdversa, Justificativa, Option.Some(Evento).IdEvento, Usuario);

            return resultado;
        }

        private Option<ReacaoAdversa> AdicionarReacao(ReacoesAdversasDraft Draft, DocumentosRepository docRepository, 
            ReacoesAdversasRepository reacoesRepository, ReacaoAdversa ReacaoAdversa, Option<Evento> Evento, string Usuario)
        {
            var result = from evento in Evento
                         from reacaoAdversa in ReacaoAdversa.AsOption()
                         from documentoAdicionado in CriarNovoDocumento(docRepository, Draft, evento.IdEvento)
                         from reacaoAdicionada in reacoesRepository.Adicionar(documentoAdicionado, reacaoAdversa, Usuario)
                         from substanciasAdicionadas in AdicionarSubstancias(reacoesRepository, reacaoAdicionada, ReacaoAdversa.Substancias)
                         from tiposDeReacaoAdicionados in AdicionarTiposDeReacao(reacoesRepository, reacaoAdicionada, ReacaoAdversa.TiposDeReacao)
                         select new { documentoAdicionado, reacaoAdicionada, substanciasAdicionadas, tiposDeReacaoAdicionados };

            if (Option.IsNone(result))
                return Option.None<ReacaoAdversa>();
            else
                return Option.From(Option.Some(result).reacaoAdicionada);
        }
        
        //TODO: Talvez mover a intativação para uma operação separada
        private Option<ReacaoAdversa> AtualizarReacao(ReacoesAdversasDraft Draft, DocumentosRepository docRepository, 
            ReacoesAdversasRepository reacoesRepository, ReacaoAdversa ReacaoAdversa, Option<Evento> Evento, string Usuario)
        {
            //TODO: Modificar a mensagem padrão para inativação por atualização
            var result = from evento in Evento
                         from reacaoAdversa in ReacaoAdversa.AsOption()
                         from reacaoInativada in reacoesRepository.InativarPorModificacao(
                             ReacaoAdversa.IdReacaoAdversa, evento.IdEvento, Usuario)
                         from novaVersaoDocumento in CriarNovaVersaoDocumento(docRepository, ReacaoAdversa, evento.IdEvento, Draft.Usuario)
                         from reacaoAlterada in reacoesRepository.Adicionar(novaVersaoDocumento, reacaoAdversa, Usuario)
                         from substanciasAdicionadas in AdicionarSubstancias(reacoesRepository, reacaoAlterada, ReacaoAdversa.Substancias)
                         from tiposDeReacaoAdicionados in AdicionarTiposDeReacao(reacoesRepository, reacaoAlterada, ReacaoAdversa.TiposDeReacao)
                         select new { novaVersaoDocumento, reacaoAlterada, substanciasAdicionadas, tiposDeReacaoAdicionados };

            if (Option.IsNone(result))
                return Option.None<ReacaoAdversa>();
            else
                return Option.From(Option.Some(result).reacaoAlterada);
        //    return IsSuccessfulOperation(result);
        }

        private Option<Documento> CriarNovaVersaoDocumento(DocumentosRepository docRepository, ReacaoAdversa ReacaoAdversa, int IdEvento, string Usuario)
        {
            var documento = from ultimoDocumento in docRepository.RetornarPorVersao(ReacaoAdversa.Documento.IdDocumento, ReacaoAdversa.Documento.Versao)
                            from novaVersaoDocumento in docRepository.CriarNovaVersao(ultimoDocumento, IdEvento, Usuario)
                            select novaVersaoDocumento;

            return documento;
        }

        private Option<Documento> CriarNovoDocumento(DocumentosRepository docRepository, ReacoesAdversasDraft Draft, int IdEvento) =>
           docRepository.CriarNovo(TipoDocumento.RegistroAlergias, Draft.IdPaciente, IdEvento, Draft.Usuario);

        private Option<List<Option<Substancia>>> AdicionarSubstancias(
            ReacoesAdversasRepository repository, ReacaoAdversa ReacaoAdversaDestino, IEnumerable<Substancia> Substancias)
        {
            var result = Substancias.Select(
                substancia => repository.AdicionarSubstancia(
                    ReacaoAdversaDestino.IdReacaoAdversa, substancia));
            
            return result.ToList().AsOption();
        }

        private Option<List<Option<TipoDeReacao>>> AdicionarTiposDeReacao(
            ReacoesAdversasRepository repository, ReacaoAdversa ReacaoAdversaDestino, IEnumerable<TipoDeReacao> TiposDeReacao)
        {
            var result = TiposDeReacao.Select(
                    tipoDeReacao => repository.AdicionarTipoReacao(
                        ReacaoAdversaDestino.IdReacaoAdversa, tipoDeReacao.Code)).ToList();

            if (result.Where(i => i.GetType() == typeof(None<TipoDeReacao>)).Count() > 0)
                return Option.None<List<Option<TipoDeReacao>>>();
            else
                return result.ToList().AsOption();
        }

        public bool IsSuccessfulOperation<T>(Option<T> result) => result.GetType() != typeof(None<T>);
    }
}