﻿using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Hsl.Ehr.Data.Services
{
    public class InalantesTerminologyService : IInalantesTerminologyService
    {
        private readonly string ConnectionString;
        private readonly bool CachedData;
        private Dictionary<int, Inalante> Cache;

        public InalantesTerminologyService(string ConnectionString, bool CachedData)
        {
            this.ConnectionString = ConnectionString;
            this.CachedData = CachedData;

            if (CachedData)
                this.LoadData();
        }

        private void LoadData()
        {
            IEnumerable<Inalante> inalantes = this.GetAllFromRepository();

            this.Cache = new Dictionary<int, Inalante>();

            foreach (var inalante in inalantes)
                this.Cache.Add(inalante.Code, inalante);
        }

        private IEnumerable<Inalante> GetAllFromRepository()
        {
            IEnumerable<Inalante> inalantes;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                inalantes = new InalantesRepository(connection).GetAll();

            return inalantes;
        }

        private Inalante GetByCodeFromRepository(int Code)
        {
            Inalante inalante;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                inalante = new InalantesRepository(connection).GetByCode(Code);

            return inalante;
        }

        private IEnumerable<Inalante> GetByValueFromRepository(string Value)
        {
            IEnumerable<Inalante> inalantes;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                inalantes = new InalantesRepository(connection).GetByValue(Value);

            return inalantes;
        }

        private IEnumerable<Inalante> GetAllFromCache() => Cache.Values;

        private Inalante GetByCodeFromCache(int Code) => Cache.ContainsKey(Code) ? Cache[Code] : null;

        private IEnumerable<Inalante> GetByValueFromCache(string Value) => Cache.Values.Where(i => i.Value.ToUpper().Contains(Value.ToUpper()));

        public IEnumerable<Inalante> GetAll() => CachedData ? Cache.Values : GetAllFromRepository();

        public Inalante GetByCode(int Code) => this.CachedData ? this.GetByCodeFromCache(Code) : this.GetByCodeFromRepository(Code);

        public IEnumerable<Inalante> GetByValue(string Value) => this.CachedData ? this.GetByValueFromCache(Value) : this.GetByValueFromRepository(Value);
    }
}
