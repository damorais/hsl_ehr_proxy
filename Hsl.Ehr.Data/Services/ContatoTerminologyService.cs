﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Services
{
    public class ContatoTerminologyService : IContatoTerminologyService
    {
        private readonly string ConnectionString;
        private readonly bool CachedData;
        private Dictionary<int, Contato> Cache;

        public ContatoTerminologyService(string ConnectionString, bool CachedData)
        {
            this.ConnectionString = ConnectionString;
            this.CachedData = CachedData;

            if (CachedData)
                this.LoadData();
        }

        private void LoadData()
        {
            IEnumerable<Contato> itemsContato = this.GetAllFromRepository();

            Cache = new Dictionary<int, Contato>();

            foreach (var itemContato in itemsContato)
                this.Cache.Add(itemContato.Code, itemContato);
        }

        private IEnumerable<Contato> GetAllFromRepository()
        {
            IEnumerable<Contato> itensContato;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                itensContato = new ContatoRepository(connection).GetAll();

            return itensContato;
        }

        private Contato GetByCodeFromRepository(int Code)
        {
            Contato itemContato;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                itemContato = new ContatoRepository(connection).GetByCode(Code);

            return itemContato;
        }

        private IEnumerable<Contato> GetByValueFromRepository(string Value)
        {
            IEnumerable<Contato> itensContato;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                itensContato = new ContatoRepository(connection).GetByValue(Value);

            return itensContato;
        }

        private IEnumerable<Contato> GetAllFromCache() => Cache.Values;

        private Contato GetByCodeFromCache(int Code) => Cache.ContainsKey(Code) ? Cache[Code] : null;

        private IEnumerable<Contato> GetByValueFromCache(string Value) => Cache.Values.Where(a => a.Value.ToUpper().Contains(Value.ToUpper()));

        public IEnumerable<Contato> GetAll() => CachedData ? Cache.Values : GetAllFromRepository();

        public Contato GetByCode(int Code) => this.CachedData ? this.GetByCodeFromCache(Code) : this.GetByCodeFromRepository(Code);

        public IEnumerable<Contato> GetByValue(string Value) => this.CachedData ? this.GetByValueFromCache(Value) : this.GetByValueFromRepository(Value);
    }
}
