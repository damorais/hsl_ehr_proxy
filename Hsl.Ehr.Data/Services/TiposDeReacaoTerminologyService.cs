﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Domain.Abstract.Services.Terminologies;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Oracle.ManagedDataAccess.Client;

namespace Hsl.Ehr.Data.Services
{
    public class TiposDeReacaoTerminologyService : ITiposDeReacaoTerminologyService
    {
        private readonly string ConnectionString;
        private readonly bool CachedData;
        private Dictionary<int, TipoDeReacao> Cache;

        public TiposDeReacaoTerminologyService(string ConnectionString, bool CachedData)
        {
            this.ConnectionString = ConnectionString;
            this.CachedData = CachedData;

            if (CachedData)
                this.LoadData();
        }

        private void LoadData()
        {
            IEnumerable<TipoDeReacao> tiposDeReacao = this.GetAllFromRepository();

            this.Cache = new Dictionary<int, TipoDeReacao>();

            foreach (var tipoDeReacao in tiposDeReacao)
                this.Cache.Add(tipoDeReacao.Code, tipoDeReacao);
        }

        private IEnumerable<TipoDeReacao> GetAllFromRepository()
        {
            IEnumerable<TipoDeReacao> tiposDeReacao;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                tiposDeReacao = new TiposDeReacaoRepository(connection).GetAll();

            return tiposDeReacao;
        }

        private TipoDeReacao GetByCodeFromRepository(int Code)
        {
            TipoDeReacao tipoDeReacao;

            using (IDbConnection connection = new OracleConnection(ConnectionString))
                tipoDeReacao = new TiposDeReacaoRepository(connection).GetByCode(Code);

            return tipoDeReacao;
        }

        private IEnumerable<TipoDeReacao> GetAllFromCache() => Cache.Values;

        private TipoDeReacao GetByCodeFromCache(int Code) => Cache.ContainsKey(Code) ? Cache[Code] : null;


        public IEnumerable<TipoDeReacao> GetAll() => CachedData ? Cache.Values : GetAllFromRepository();

        public TipoDeReacao GetByCode(int Code) => this.CachedData ? this.GetByCodeFromCache(Code) : this.GetByCodeFromRepository(Code);

    }
}
