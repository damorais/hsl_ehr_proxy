﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Util;

namespace Hsl.Ehr.Data.Configuration
{
    enum ConnectionStrings
    {
        Padrao
    }

    enum AppSettings
    {
        UrlProvedorDemografia,
        CaminhoLogThinkEHR
    }

    public class Configuracoes
    {
        public string ConexaoPadrao { private set; get; }
        public string UrlProvedorDemografia { private set; get; }

        public Configuracoes()
        {
            this.ConexaoPadrao = CriptografiaUtil.Decriptar(
                RetornarConexao(ConnectionStrings.Padrao)
            );
            this.UrlProvedorDemografia =
                RetornarConfiguracao(AppSettings.UrlProvedorDemografia);
        }

        internal static string RetornarConfiguracao(AppSettings ConfiguracaoItem) =>
            RetornarConfiguracao(ConfiguracaoItem, false);

        internal static string RetornarConfiguracao(AppSettings ConfiguracaoItem, bool ConfiguracaoCriptografada) =>
            ConfiguracaoCriptografada ?
                CriptografiaUtil.Decriptar(ConfigurationManager.AppSettings[ConfiguracaoItem.ToString()]) :
                    ConfigurationManager.AppSettings[ConfiguracaoItem.ToString()];

        internal static string RetornarConexao(ConnectionStrings Conexao) =>
            ConfigurationManager.ConnectionStrings[Conexao.ToString()].ConnectionString;
       
    }
}
