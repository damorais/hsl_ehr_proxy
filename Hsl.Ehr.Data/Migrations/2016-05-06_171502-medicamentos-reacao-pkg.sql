﻿--UP
create or replace PACKAGE TRM_MEDICAMENTOS_REACAO_PKG AS 

PROCEDURE GetAll
(
  ResultSet out SYS_REFCURSOR
);

PROCEDURE GetByNome
(
  pNome VARCHAR,
  ResultSet out SYS_REFCURSOR
);
  
PROCEDURE GetByCode
(
  pCode NUMBER,
  ResultSet out SYS_REFCURSOR
);

PROCEDURE GetSubstanciasByCode
(
  pCode_Medicamento NUMBER,
  ResultSet out SYS_REFCURSOR
);

PROCEDURE GetSubstanciaByDCB
(
  pDCB VARCHAR,
  ResultSet out SYS_REFCURSOR
);

END TRM_MEDICAMENTOS_REACAO_PKG;
/
create or replace PACKAGE BODY TRM_MEDICAMENTOS_REACAO_PKG AS

  PROCEDURE GetAll
  (
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
     OPEN ResultSet FOR
      SELECT ID AS Code, DS_REDUZIDA AS Value
        FROM SYS_HSL.HSL_PEP_MEDIC_REDUZ_ALERGIA_VW 
        GROUP BY (ID, DS_REDUZIDA) ORDER BY DS_REDUZIDA;   

  END GetAll;
  
  PROCEDURE GetByCode
  (
    pCode NUMBER,
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      --SELECT ID AS Code, DS_REDUZIDA AS Value 
        --FROM SYS_HSL.HSL_PEP_MEDIC_REDUZIDO_DCB_VW 
          --WHERE 
            --ID = pCode
        SELECT ID AS Code, DS_REDUZIDA AS Value
        FROM SYS_HSL.HSL_PEP_MEDIC_REDUZ_ALERGIA_VW 
          WHERE 
              ID = pCode
        GROUP BY (ID, DS_REDUZIDA) 
        ORDER BY DS_REDUZIDA;
  END GetByCode;
  
  PROCEDURE GetByNome
  (
    pNome VARCHAR,
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT ID AS Code, DS_REDUZIDA AS Value
        FROM SYS_HSL.HSL_PEP_MEDIC_REDUZ_ALERGIA_VW 
          WHERE 
            UPPER(DS_REDUZIDA) LIKE ('%' || UPPER(pNome) || '%')
        GROUP BY (ID, DS_REDUZIDA) ORDER BY DS_REDUZIDA;   
    
      --SELECT ID AS Code, DS_REDUZIDA AS Value 
        --FROM SYS_HSL.HSL_PEP_MEDIC_REDUZIDO_DCB_VW 
          --WHERE 
            --UPPER(DS_REDUZIDA) LIKE ('%' || UPPER(pNome) || '%')
        --GROUP BY (ID, DS_REDUZIDA) ORDER BY DS_REDUZIDA;    
  END GetByNome;
  
  PROCEDURE GetSubstanciasByCode
  (
    pCode_Medicamento NUMBER,
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT CD_DCB AS Dcb, CD_DCB AS Code, DS_DCB AS Value 
        FROM SYS_HSL.HSL_PEP_MEDIC_REDUZIDO_DCB_VW
          WHERE ID = pCode_Medicamento
        ORDER BY DS_DCB;
  END GetSubstanciasByCode;

  PROCEDURE GetSubstanciaByDCB
  (
    pDCB VARCHAR,
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT CD_DCB AS Dcb, CD_DCB AS Code, DS_DCB AS Value 
        FROM SYS_HSL.HSL_PEP_MEDIC_REDUZIDO_DCB_VW
          WHERE 
            UPPER(CD_DCB) = UPPER(pDCB)
        GROUP BY (CD_DCB, DS_DCB);
  END GetSubstanciaByDCB;

END TRM_MEDICAMENTOS_REACAO_PKG;
/
BEGIN
	INSERT INTO RES_MIGRACOES(ID, VERSAO) VALUES (6, '0.0.6');
	COMMIT;
END;




--DOWN
--drop package MEDICAMENTOS_REACAO_PKG;

--DELETE FROM RES_MIGRACOES WHERE VERSAO = '0.0.6';