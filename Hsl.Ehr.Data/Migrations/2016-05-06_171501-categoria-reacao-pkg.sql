﻿create or replace PACKAGE TRM_CATEGORIA_REACAO_PKG AS 

  PROCEDURE GetAll
  (
    ResultSet out SYS_REFCURSOR
  );
  
  PROCEDURE GetByCode
  (
    pCode TRM_CATEGORIA_REACAO.CODE%TYPE,
    ResultSet out SYS_REFCURSOR
  );  

END TRM_CATEGORIA_REACAO_PKG;
/
create or replace PACKAGE BODY TRM_CATEGORIA_REACAO_PKG AS

  PROCEDURE GetAll
  (
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR 
      SELECT CODE, VALUE FROM TRM_CATEGORIA_REACAO;
  END GetAll;

  PROCEDURE GetByCode
  (
    pCode TRM_CATEGORIA_REACAO.CODE%TYPE,
    ResultSet out SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR 
      SELECT CODE, VALUE FROM TRM_CATEGORIA_REACAO WHERE CODE = pCode;
  END GetByCode;

END TRM_CATEGORIA_REACAO_PKG;
/
BEGIN
	INSERT INTO RES_MIGRACOES(ID, VERSAO) VALUES (4, '0.0.4');
	COMMIT;
END;



--DOWN
--drop package CATEGORIA_REACAO_PKG;

--DELETE FROM SYS_THINKEHR.MIGRACOES WHERE VERSAO = '0.0.4';