﻿--UP

create or replace 
PACKAGE RES_DOCUMENTO_PKG AS 
  
  PROCEDURE RetornarPorVersao
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE Inserir
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE CriarNovo
  (
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE CriarNovaVersao
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pNovaVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE Inativar
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdEventoInativacao RES_DOCUMENTO.ID_EVENTO_INATIVACAO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_ALTERACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );

END RES_DOCUMENTO_PKG;

/	

create or replace 
PACKAGE BODY RES_DOCUMENTO_PKG AS
  PROCEDURE RetornarPorVersao
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS 
  BEGIN
    OPEN ResultSet FOR
       SELECT 
          ID_DOCUMENTO      AS IdDocumento, 
          ID_TIPO_DOCUMENTO AS TipoDocumento,
          VERSAO            AS Versao,
          USUARIO_CRIACAO   AS UsuarioCriacao,
          DATA_CRIACAO      AS DataCriacao,
          ID_STATUS         AS Status,
          ID_EVENTO         AS IdEvento,
          ID_PACIENTE       AS IdPaciente,
          USUARIO_ALTERACAO AS UsuarioAlteracao,
          DATA_ALTERACAO    AS DataAlteracao
        FROM RES_DOCUMENTO
        WHERE ID_DOCUMENTO = pIdDocumento AND Versao = pVersao;
  END RetornarPorVersao;

  PROCEDURE Inserir
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    INSERT INTO RES_DOCUMENTO(
      ID_DOCUMENTO, VERSAO, ID_TIPO_DOCUMENTO, USUARIO_CRIACAO, ID_PACIENTE, 
      ID_EVENTO, DATA_ALTERACAO, USUARIO_ALTERACAO, ID_STATUS)
    VALUES(pIdDocumento, pVersao, pIdTipoDocumento, pUsuario, pIdPaciente, 
      pIdEvento, SYSDATE, pUsuario, 1);
      
     RES_DOCUMENTO_PKG.RetornarPorVersao(pIdDocumento, pVersao, ResultSet);
  END Inserir;

  PROCEDURE CriarNovo
  (
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) IS
    IdDocumento NUMBER;
  BEGIN
    SELECT RES_DOCUMENTO_SEQ.NEXTVAL INTO IdDocumento FROM DUAL;
    
    RES_DOCUMENTO_PKG.INSERIR(
      IdDocumento,
      1,
      pIdTipoDocumento,
      pIdPaciente,
      pIdEvento,
      pUsuario,
      ResultSet);
      
  END CriarNovo;
  
  PROCEDURE CriarNovaVersao
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pNovaVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdTipoDocumento RES_DOCUMENTO.ID_TIPO_DOCUMENTO%TYPE,
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    pIdEvento RES_DOCUMENTO.ID_EVENTO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    RES_DOCUMENTO_PKG.INSERIR(
      pIdDocumento,
      pNovaVersao,
      pIdTipoDocumento,
      pIdPaciente,
      pIdEvento,
      pUsuario,
      ResultSet);
  END CriarNovaVersao;

  PROCEDURE Inativar
  (
    pIdDocumento RES_DOCUMENTO.ID_DOCUMENTO%TYPE,
    pVersao RES_DOCUMENTO.VERSAO%TYPE,
    pIdEventoInativacao RES_DOCUMENTO.ID_EVENTO_INATIVACAO%TYPE,
    pUsuario RES_DOCUMENTO.USUARIO_ALTERACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    UPDATE RES_DOCUMENTO SET
      ID_STATUS = 2,
      ID_EVENTO_INATIVACAO = pIdEventoInativacao
    WHERE ID_DOCUMENTO = pIdDocumento
      AND VERSAO = pVersao;
      
    RES_DOCUMENTO_PKG.RetornarPorVersao(pIdDocumento, pVersao, ResultSet);
  END Inativar;

END RES_DOCUMENTO_PKG;
/
BEGIN
  INSERT INTO RES_MIGRACOES(ID, VERSAO) VALUES (17, '0.0.17');
  COMMIT;
END;

--DOWN
-- DEVE SER FEITO O UPDATE MANUAL PARA A VERSAO ANTERIOR DO PACOTE
--DELETE FROM RES_MIGRACOES WHERE VERSAO = '0.0.17';