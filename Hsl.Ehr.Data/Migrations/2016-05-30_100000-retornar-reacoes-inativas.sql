﻿create or replace PACKAGE RES_REACAO_ADVERSA_PKG AS 

  PROCEDURE RetornarAtivosPorPaciente
  (
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE RetornarInativosPorPaciente
  (
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE Retornar
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE RetornarHistorico
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) ;
  
  PROCEDURE Adicionar
  (
    pCategoriaReacaoCode RES_REACAO_ADVERSA.CATEGORIA_REACAO_CODE%TYPE,
    pItem RES_REACAO_ADVERSA.ITEM%TYPE,
    pDescricao RES_REACAO_ADVERSA.DESCRICAO%TYPE,
    pNotificarEventoAdverso RES_REACAO_ADVERSA.NOTIFICAR_EVENTO_ADVERSO%TYPE,
    pIdDocumento RES_REACAO_ADVERSA.ID_DOCUMENTO%TYPE,
    pVersaoDocumento RES_REACAO_ADVERSA.VERSAO_DOCUMENTO%TYPE,
    pUsuario RES_REACAO_ADVERSA.USUARIO_CRIACAO%TYPE,
    pIdReacaoOriginal RES_REACAO_ADVERSA.ID_REACAO_ORIGINAL%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE AdicionarSubstancia
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pCodeSubstancia RES_REACAO_ADVERSA_SUBSTANCIAS.CODE_SUBSTANCIA%TYPE,
    pValueSubstancia RES_REACAO_ADVERSA_SUBSTANCIAS.VALUE_SUBSTANCIA%TYPE,
    pTerminology RES_REACAO_ADVERSA_SUBSTANCIAS.TERMINOLOGY%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE RetornarSubstancias
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE AdicionarTipoReacao
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pCodeTipoReacao RES_REACAO_ADVERSA_TIPOS.CODE_TIPO_REACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE InativarReacaoEDocumento
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pJustificativa RES_REACAO_ADVERSA.JUSTIFICATIVA_INATIVACAO%TYPE,
    pIdEventoInativacao RES_REACAO_ADVERSA.ID_EVENTO_INATIVACAO%TYPE,
    pUsuario RES_REACAO_ADVERSA.USUARIO_ALTERACAO%TYPE,
    pStatusDestino RES_REACAO_ADVERSA.ID_STATUS%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE RetornarTiposReacao
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );
  
  PROCEDURE SaveOnTEhrQueue
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA_THINKEHR.ID_REACAO_ADVERSA%TYPE,
    pIdReacaoAdversaOriginal RES_REACAO_ADVERSA_THINKEHR.ID_REACAO_ADVERSA%TYPE,
    pIdStatus RES_REACAO_ADVERSA_THINKEHR.ID_STATUS%TYPE,
    pTEHRData RES_REACAO_ADVERSA_THINKEHR.THINKEHR_DATA%TYPE,
    pIdPacienteTEHR RES_REACAO_ADVERSA_THINKEHR.ID_PACIENTE_TEHR%TYPE,
    pUsuario  RES_REACAO_ADVERSA_THINKEHR.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  );

END RES_REACAO_ADVERSA_PKG;

/


create or replace PACKAGE BODY RES_REACAO_ADVERSA_PKG AS

  PROCEDURE RetornarHistorico
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) IS
    IdReacaoOriginal NUMBER;
  BEGIN
    SELECT ID_REACAO_ORIGINAL 
    INTO IdReacaoOriginal
    FROM RES_REACAO_ADVERSA
    WHERE ID_REACAO_ADVERSA = pIdReacaoAdversa;
    
    OPEN ResultSet FOR
    SELECT * 
    FROM RES_REACAO_ADVERSA_VW 
    WHERE REACAO_ID_REACAO_ORIGINAL = IdReacaoOriginal
    ORDER BY REACAO_DATA_CRIACAO DESC;
    
  END RetornarHistorico;

  PROCEDURE RetornarAtivosPorPaciente
  (
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
    SELECT * 
      FROM RES_REACAO_ADVERSA_VW 
      WHERE
        DOCUMENTO_ID_PACIENTE = pIdPaciente AND REACAO_ID_STATUS = 1;
  END RetornarAtivosPorPaciente;
  
  PROCEDURE RetornarInativosPorPaciente
  (
    pIdPaciente RES_DOCUMENTO.ID_PACIENTE%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
    SELECT * 
      FROM RES_REACAO_ADVERSA_VW 
      WHERE
        DOCUMENTO_ID_PACIENTE = pIdPaciente AND REACAO_ID_STATUS = 2;
  END RetornarInativosPorPaciente;
  
  PROCEDURE Retornar
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT * 
      FROM RES_REACAO_ADVERSA_VW 
      WHERE
        REACAO_ID_REACAO_ADVERSA = pIdReacaoAdversa;
  END Retornar;
  
  PROCEDURE Adicionar
  (
    pCategoriaReacaoCode RES_REACAO_ADVERSA.CATEGORIA_REACAO_CODE%TYPE,
    pItem RES_REACAO_ADVERSA.ITEM%TYPE,
    pDescricao RES_REACAO_ADVERSA.DESCRICAO%TYPE,
    pNotificarEventoAdverso RES_REACAO_ADVERSA.NOTIFICAR_EVENTO_ADVERSO%TYPE,
    pIdDocumento RES_REACAO_ADVERSA.ID_DOCUMENTO%TYPE,
    pVersaoDocumento RES_REACAO_ADVERSA.VERSAO_DOCUMENTO%TYPE,
    pUsuario RES_REACAO_ADVERSA.USUARIO_CRIACAO%TYPE,
    pIdReacaoOriginal RES_REACAO_ADVERSA.ID_REACAO_ORIGINAL%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) IS
    IdReacaoAdversa NUMBER;
    IdReacaoOriginal NUMBER;
  BEGIN
    SELECT RES_REACAO_ADVERSA_SEQ.NEXTVAL INTO IdReacaoAdversa FROM DUAL;
    
    IF (pIdReacaoOriginal IS NULL) THEN
      IdReacaoOriginal := IdReacaoAdversa;
    ELSE
      IdReacaoOriginal := pIdReacaoOriginal;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('RESULTSET = ' || IdReacaoOriginal);
    
    INSERT INTO RES_REACAO_ADVERSA(
      ID_REACAO_ADVERSA, CATEGORIA_REACAO_CODE, ITEM, DESCRICAO, ID_STATUS,
      NOTIFICAR_EVENTO_ADVERSO, ID_DOCUMENTO, VERSAO_DOCUMENTO, 
      USUARIO_CRIACAO, USUARIO_ALTERACAO, ID_REACAO_ORIGINAL)
    VALUES (
      IdReacaoAdversa, pCategoriaReacaoCode, pItem, pDescricao, 1,
      pNotificarEventoAdverso, pIdDocumento, pVersaoDocumento, 
      pUsuario, pUsuario, IdReacaoOriginal);
    
    RES_REACAO_ADVERSA_PKG.Retornar(IdReacaoAdversa, ResultSet);
    
  END Adicionar;
  
  PROCEDURE AdicionarSubstancia
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pCodeSubstancia RES_REACAO_ADVERSA_SUBSTANCIAS.CODE_SUBSTANCIA%TYPE,
    pValueSubstancia RES_REACAO_ADVERSA_SUBSTANCIAS.VALUE_SUBSTANCIA%TYPE,
    pTerminology RES_REACAO_ADVERSA_SUBSTANCIAS.TERMINOLOGY%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    INSERT INTO RES_REACAO_ADVERSA_SUBSTANCIAS
      (ID_REACAO_ADVERSA, CODE_SUBSTANCIA, VALUE_SUBSTANCIA, TERMINOLOGY)
    VALUES
      (pIdReacaoAdversa, pCodeSubstancia, pValueSubstancia, pTerminology);
      
    OPEN ResultSet FOR
      SELECT
        ID_REACAO_ADVERSA AS IdReacaoAdversa,
        CODE_SUBSTANCIA AS Code,
        VALUE_SUBSTANCIA AS Value,
        TERMINOLOGY AS Terminology
      FROM RES_REACAO_ADVERSA_SUBSTANCIAS
      WHERE 
        ID_REACAO_ADVERSA = pIdReacaoAdversa
        AND CODE_SUBSTANCIA = pCodeSubstancia
        AND TERMINOLOGY = pTerminology;
  END AdicionarSubstancia;
  
  PROCEDURE RetornarSubstancias
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT
        ID_REACAO_ADVERSA AS IdReacaoAdversa,
        CODE_SUBSTANCIA AS Code,
        TERMINOLOGY AS Terminology
      FROM RES_REACAO_ADVERSA_SUBSTANCIAS
      WHERE 
        ID_REACAO_ADVERSA = pIdReacaoAdversa;
  END RetornarSubstancias;
  
  PROCEDURE AdicionarTipoReacao
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pCodeTipoReacao RES_REACAO_ADVERSA_TIPOS.CODE_TIPO_REACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    INSERT INTO RES_REACAO_ADVERSA_TIPOS
      (ID_REACAO_ADVERSA, CODE_TIPO_REACAO)
    VALUES
      (pIdReacaoAdversa, pCodeTipoReacao);
    
    OPEN ResultSet FOR
      SELECT
        ID_REACAO_ADVERSA AS IdReacaoAdversa,
        CODE_TIPO_REACAO AS Code
      FROM RES_REACAO_ADVERSA_TIPOS
      WHERE 
        ID_REACAO_ADVERSA = pIdReacaoAdversa
        AND CODE_TIPO_REACAO = pCodeTipoReacao;
  END AdicionarTipoReacao;
  
  PROCEDURE InativarReacaoEDocumento
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    pJustificativa RES_REACAO_ADVERSA.JUSTIFICATIVA_INATIVACAO%TYPE,
    pIdEventoInativacao RES_REACAO_ADVERSA.ID_EVENTO_INATIVACAO%TYPE,
    pUsuario RES_REACAO_ADVERSA.USUARIO_ALTERACAO%TYPE,
    pStatusDestino RES_REACAO_ADVERSA.ID_STATUS%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) IS
    RegistroExiste NUMBER;
    IdDocumento NUMBER;
    Versao NUMBER;
    DocumentoInativacao SYS_REFCURSOR;
  BEGIN
    SELECT COUNT(*) INTO RegistroExiste
    FROM RES_REACAO_ADVERSA r
    WHERE ID_REACAO_ADVERSA = pIdReacaoAdversa;
  
    IF(RegistroExiste > 0) THEN  
      SELECT ID_DOCUMENTO, VERSAO_DOCUMENTO INTO IdDocumento, Versao
      FROM RES_REACAO_ADVERSA
      WHERE ID_REACAO_ADVERSA = pIdReacaoAdversa;
      
      UPDATE RES_REACAO_ADVERSA SET
        ID_STATUS = pStatusDestino,
        JUSTIFICATIVA_INATIVACAO = pJustificativa,
        ID_EVENTO_INATIVACAO = pIdEventoInativacao,
        USUARIO_ALTERACAO = pUsuario,
        DATA_ALTERACAO = SYSDATE
      WHERE
        ID_REACAO_ADVERSA = pIdReacaoAdversa;
      
      --Inativa o documento vinculado a esta reação adversa  
      RES_DOCUMENTO_PKG.Inativar(
        IdDocumento, Versao, pIdEventoInativacao, pUsuario, pStatusDestino, 
        DocumentoInativacao
      );
      --Retornar a reação adversa com o novo status
      RES_REACAO_ADVERSA_PKG.Retornar(pIdReacaoAdversa, ResultSet);
    ELSE
      --Não alterou nenhum registro porque o registro não existe. 
      --Não retorna o objeto alterado.
      NULL;
    END IF;
  END InativarReacaoEDocumento;
  
  
  PROCEDURE RetornarTiposReacao
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA.ID_REACAO_ADVERSA%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN ResultSet FOR
      SELECT
        ID_REACAO_ADVERSA AS IdReacaoAdversa,
        CODE_TIPO_REACAO AS Code
      FROM RES_REACAO_ADVERSA_TIPOS
      WHERE 
        ID_REACAO_ADVERSA = pIdReacaoAdversa;
  END RetornarTiposReacao;
  
  PROCEDURE SaveOnTEhrQueue
  (
    pIdReacaoAdversa RES_REACAO_ADVERSA_THINKEHR.ID_REACAO_ADVERSA%TYPE,
    pIdReacaoAdversaOriginal RES_REACAO_ADVERSA_THINKEHR.ID_REACAO_ADVERSA%TYPE,
    pIdStatus RES_REACAO_ADVERSA_THINKEHR.ID_STATUS%TYPE,
    pTEHRData RES_REACAO_ADVERSA_THINKEHR.THINKEHR_DATA%TYPE,
    pIdPacienteTEHR RES_REACAO_ADVERSA_THINKEHR.ID_PACIENTE_TEHR%TYPE,
    pUsuario  RES_REACAO_ADVERSA_THINKEHR.USUARIO_CRIACAO%TYPE,
    ResultSet OUT SYS_REFCURSOR
  ) IS
    IdTEHR VARCHAR2(60);
  BEGIN
    BEGIN
      SELECT ID_TEHR 
      INTO IdTEHR
      FROM RES_REACAO_ADVERSA_THINKEHR 
      WHERE 
        ID_REACAO_ORIGINAL = pIdReacaoAdversaOriginal
        AND ROWNUM = 1
      ORDER BY DATA_CRIACAO DESC;      
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        IdTEHR := NULL;
    END;
    
    IF(IdTEHR IS NOT NULL) THEN
      INSERT INTO RES_REACAO_ADVERSA_THINKEHR(
        ID_REACAO_ADVERSA,
        ID_REACAO_ORIGINAL,
        ID_STATUS, 
        THINKEHR_DATA,
        USUARIO_CRIACAO,
        USUARIO_ALTERACAO,
        ID_PACIENTE_TEHR,
        ID_TEHR
      ) VALUES (
        pIdReacaoAdversa,
        pIdReacaoAdversaOriginal,
        pIdStatus,
        pTEHRData,
        pUsuario,
        pUsuario,
        pIdPacienteTEHR,
        IdTEHR
      );
    ELSE
      INSERT INTO RES_REACAO_ADVERSA_THINKEHR(
        ID_REACAO_ADVERSA,
        ID_REACAO_ORIGINAL,
        ID_STATUS, 
        THINKEHR_DATA,
        USUARIO_CRIACAO,
        USUARIO_ALTERACAO,
        ID_PACIENTE_TEHR
      ) VALUES (
        pIdReacaoAdversa,
        pIdReacaoAdversaOriginal,
        pIdStatus,
        pTEHRData,
        pUsuario,
        pUsuario,
        pIdPacienteTEHR
      );
    END IF;
    OPEN ResultSet FOR
      SELECT ID_REACAO_ADVERSA FROM RES_REACAO_ADVERSA_THINKEHR;
      
  END SaveOnTEhrQueue;

END RES_REACAO_ADVERSA_PKG;

INSERT INTO RES_MIGRACOES(ID, VERSAO) VALUES (27, '0.0.27');