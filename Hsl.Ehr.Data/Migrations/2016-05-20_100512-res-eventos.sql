﻿--UP
CREATE TABLE RES_TIPO_EVENTO
(
  ID_TIPO_EVENTO NUMBER(10,0) NOT NULL PRIMARY KEY,
  NOME VARCHAR(20)
);

INSERT INTO RES_TIPO_EVENTO(ID_TIPO_EVENTO, NOME) 
  VALUES (1, 'Registro de Alergias');

ALTER TABLE RES_EVENTO
  ADD ID_TIPO_EVENTO NUMBER(10,0);

ALTER TABLE RES_EVENTO
  ADD DATA_CRIACAO DATE DEFAULT SYSDATE;

ALTER TABLE RES_EVENTO
  ADD NR_ATENDIMENTO_SIH NUMBER(10,0);

UPDATE RES_EVENTO SET ID_TIPO_EVENTO = 1;
UPDATE RES_EVENTO SET DATA_CRIACAO = SYSDATE;

ALTER TABLE RES_EVENTO MODIFY (ID_TIPO_EVENTO NOT NULL);
ALTER TABLE RES_EVENTO MODIFY (DATA_CRIACAO NOT NULL);

ALTER TABLE RES_REACAO_ADVERSA
	ADD ID_REACAO_ORIGINAL NUMBER(10,0) NOT NULL;

ALTER TABLE RES_REACAO_ADVERSA
  ADD CONSTRAINT RES_REACAO_REACAO_ORI_FK 
    FOREIGN KEY(ID_REACAO_ADVERSA)
      REFERENCES RES_REACAO_ADVERSA(ID_REACAO_ADVERSA);

ALTER TABLE RES_EVENTO
  ADD CONSTRAINT RES_EVENTO_TIPO_FK 
    FOREIGN KEY(ID_TIPO_EVENTO)
      REFERENCES RES_TIPO_EVENTO(ID_TIPO_EVENTO);

CREATE SEQUENCE RES_EVENTO_SEQ INCREMENT BY 1 START WITH 1;

ALTER TABLE RES_REACAO_ADVERSA
  ADD ID_EVENTO_INATIVACAO NUMBER(10,0);
  
ALTER TABLE RES_REACAO_ADVERSA
  ADD CONSTRAINT RES_EVENTO_INAT_REACAO_FK
    FOREIGN KEY(ID_EVENTO_INATIVACAO)
      REFERENCES RES_EVENTO(ID_EVENTO);

ALTER TABLE RES_DOCUMENTO
  ADD ID_EVENTO_INATIVACAO NUMBER(10,0);
  
ALTER TABLE RES_DOCUMENTO
  ADD CONSTRAINT RES_EVENTO_INAT_DOC_FK
    FOREIGN KEY(ID_EVENTO_INATIVACAO)
      REFERENCES RES_EVENTO(ID_EVENTO);

INSERT INTO RES_MIGRACOES(ID, VERSAO) VALUES (15, '0.0.15');

-- DOWN
--ALTER TABLE RES_EVENTO
--  DROP COLUMN NR_ATENDIMENTO_SIH;
--ALTER TABLE RES_EVENTO
--  DROP COLUMN DATA_CRIACAO;
--ALTER TABLE RES_EVENTO
--  DROP CONSTRAINT RES_EVENTO_TIPO_FK;
--ALTER TABLE RES_EVENTO
--  DROP COLUMN ID_TIPO_EVENTO;
--DROP TABLE RES_TIPO_EVENTO;
--DROP SEQUENCE RES_EVENTO_SEQ;
--DELETE FROM RES_MIGRACOES WHERE VERSAO = '0.0.15';