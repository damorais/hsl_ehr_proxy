﻿using System;
using System.Collections.Generic;
using System.Data;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Data.Repositories.Terminologies;
using Hsl.Ehr.Data.Services;
using Oracle.ManagedDataAccess.Client;
using Hsl.Ehr.Domain.Entities.ValueObjects.Terminology;
using Hsl.Ehr.Data.Repositories.ReacoesAdversas;
using Hsl.Ehr.Data.Repositories;
using Hsl.Ehr.Util.AdvancedTypes;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Data.Marand;
using System.Threading.Tasks;
using Dapper;
using System.Linq;

namespace Hsl.Ehr.Data
{
    class Program
    {
        static string conn = System.Configuration.ConfigurationManager.ConnectionStrings["Padrao"].ConnectionString;

        //static async Task Teste(string Nome)
        //{
        //    //Console.WriteLine("Iniciando");
        //    //System.IO.File.AppendAllText("teste.txt", $"{Nome} - teste1;");
        //    //System.Threading.Thread.Sleep(3000);
        //    //System.IO.File.AppendAllText("teste.txt", $"{Nome} - teste2;");
        //    //System.IO.File.AppendAllText("teste.txt", $"{Nome} - teste3;");
        //    //System.IO.File.AppendAllText("teste.txt", $"{Nome} - teste4;");
        //    //Console.WriteLine("encerrando");
        //}

        static void Main(string[] args)
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var result = db.Query<int>("SELECT ID_PACIENTE FROM PEP_PACIENTE WHERE EHR_ID IS NULL");

                var origin = result.Count();

                var i = 1;

                foreach (var idPaciente in result)
                {
                    //var paciente = new PacientesRepository("http://sihassistencialservicesimulacao.hsl.org.br:1000").BuscarPaciente(idPaciente);
                    Console.WriteLine($"{i} - Remaining {origin - i}");
                    i++;
                }



                //var repo = new ReacoesAdversasDraftsRepository(db);



                //var r = repo.RetornarPorPaciente(1, "dmgmorais");
                ////foreach(var r in )
                //Console.WriteLine(r);
            }


            //var paciente = new PacientesRepository(this.UrlProvedorDemografia).BuscarPaciente(IdPaciente);

            //Teste();
            //Console.WriteLine("App");
            ////Task.Run(Teste);
            //Task.Factory.StartNew(() => Teste("Daniel"));
            //Console.WriteLine("App2");
            //Console.ReadKey();


            //using (IDbConnection db = new OracleConnection(conn))
            //{
            //    var repo = new ReacoesAdversasDraftsRepository(db);

            //    var r = repo.RetornarPorPaciente(1, "dmgmorais");
            //    //foreach(var r in )
            //    Console.WriteLine(r);
            //}

            //TestarCriacaoDocumentoMarand();

            //ReacaoAdversa reacao;

            //using (IDbConnection db = new OracleConnection(conn))
            //{
            //    var repo = new ReacoesAdversasRepository(db);
            //    reacao = Option.Some(repo.Retornar(96));
            //}

            //TesteRemocao();
            //TestarRetornarReacao();
            //TestarRetornarDocumento();
            //RascunhoParaProd();
            //InativarReacao();
            //ObterReacaoAdversa();
            //AdicionarReacaoAdversa();
            //TestarDocumento();
            //TesteMedicamentos();
            //TesteRepo();
            //TestarPaciente();
        }

        //private static void TestarPaciente()
        //{
        //    var pacRepo = new PacientesRepository();
        //    Console.WriteLine(pacRepo.BuscarPaciente(565456));
        //}

        private static void TestarCriacaoDocumentoMarand()
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new ReacoesAdversasRepository(db);

                var reacao = Option.Some(repo.Retornar(91));//RetornarReacoesAtivasPorPaciente(99).First();

                var composition = reacao.AsMarandAdverseReactionComposition();

                Console.WriteLine(composition);

                // repo.SaveOnTEHRQueue(reacao.IdReacaoAdversa, reacao.IdReacaoOriginal, 1, composition, new Guid(), "dmgmorais");


                //Console.WriteLine(repo.RetornarReacoesAtivasPorPaciente(99).First());




                //Console.WriteLine(repo.RetornarPorVersao(25, 1));


            }
        }

        private static void TestarRetornarDocumento()
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new DocumentosRepository(db);
                Console.WriteLine(repo.RetornarPorVersao(25, 1));


            }
                //throw new NotImplementedException();
        }

        private static void TestarRetornarReacao()
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new ReacoesAdversasRepository(db);
                Console.WriteLine("Reacao 1");
                Console.WriteLine(repo.Retornar(1));

                Console.WriteLine();
                Console.WriteLine("Reacao 2");
                Console.WriteLine(repo.Retornar(2));
            }
            //throw new NotImplementedException();
        }

        static ReacaoAdversa ReacaoSampleMedicamento
        {
            get
            {
                return new ReacaoAdversa()
                {
                    Categoria = new CategoriaDaReacao((int)CategoriasDaReacaoEnum.Medicamento, "Medicamento"),
                    Descricao = "Uma nova reação de teste",
                    Item = "Aas Protect 100mg",
                    Substancias = new List<Substancia>()
                    {
                        new Substancia() {
                            Code = "00090",
                            Value = "Acetilsalicilato de alumínio",
                            Terminology = "terminologies/substancias/00090"
                        },
                        new Substancia() {
                            Code = "00091",
                            Value = "Acetilsalicilato de cálcio",
                            Terminology = "terminologies/substancias/00091"
                        },
                        new Substancia() {
                            Code = "09600",
                            Value = "Acetilsalicilato de racelisina e glicina",
                            Terminology = "/terminologies/substancias/09600"
                        }
                    },
                        NotificarEventoAdverso = false,
                        TiposDeReacao = new List<TipoDeReacao>()
                    {
                        new TipoDeReacao((int)TipoDeReacaoEnum.Angioedema, "Angioedema"),
                        new TipoDeReacao((int)TipoDeReacaoEnum.Broncoespasmo, "Broncoespasmo")
                    }
                };
            }
        }

        static ReacaoAdversa ReacaoSampleMedicamento2
        {
            get
            {
                return new ReacaoAdversa()
                {
                    Categoria = new CategoriaDaReacao((int)CategoriasDaReacaoEnum.Medicamento, "Medicamento"),
                    Descricao = "Uma outra nova reação de teste",
                    Item = "Buscofem 400mg",
                    Substancias = new List<Substancia>()
                    {
                        new Substancia() {
                            Code = "04767",
                            Value = "Ibuprofeno alumínio",
                            Terminology = "/terminologies/substancias/04767"
                        },
                        new Substancia() {
                            Code = "04769",
                            Value = "Ibuprofeno arginina",
                            Terminology = "/terminologies/substancias/04769"
                        }
                    },
                    NotificarEventoAdverso = false,
                    TiposDeReacao = new List<TipoDeReacao>()
                    {
                        new TipoDeReacao((int)TipoDeReacaoEnum.ChoqueAnafilatico, "Choque Anafilático")
                    }
                };
            }
        }

        //public static void TesteRemocao()
        //{
        //    using (IDbConnection db = new OracleConnection(conn))
        //    {
        //        var repo = new AlergiasRepository(db);

        //        repo.Inserir(1, 50, "dmgmorais");
        //        repo.Inserir(1, 51, "dmgmorais");
        //        repo.Inserir(1, 52, "dmgmorais");
        //    }
        //}

        //public static void RascunhoParaProd()
        //{
        //    ReacaoAdversa raAlter1;
        //    //ReacaoAdversa raAlter2;

        //    //using (IDbConnection db = new OracleConnection(conn))
        //    //{
        //    //    var repo = new ReacoesAdversasRepository(db);

        //    //    raAlter1 = Option.Some(repo.Retornar(1));
        //    //    //raAlter2 = Option.Some(repo.Retornar(102));
        //    //}

        //    //raAlter1.Descricao = "Descrição Alterada 2!!!";
        //    ////raAlter2.Descricao = "Descrição Alterada 2!!!";

        //    var rascunho = new ReacoesAdversasDraft()
        //    {
        //        Conteudo = new ConteudoReacoesAdversasDraft()
        //        {
        //            NovasReacoes = new List<NovaReacao>()
        //            {
        //                new NovaReacao()
        //                {
        //                    CodigoTemporario = "teste",
        //                    ReacaoAdversa = ReacaoSampleMedicamento
        //                },
        //                new NovaReacao()
        //                {
        //                    CodigoTemporario = "teste2",
        //                    ReacaoAdversa = ReacaoSampleMedicamento2
        //                }
        //            }
        //            //,
        //            //ReacoesAtualizadas = new List<ReacaoAdversa>()
        //            //{
        //            //    raAlter1//, raAlter2
        //            //}//,
        //            //ReacoesInativadas = new List<ReacaoAInativar>()
        //            //{
        //            //    //new ReacaoAInativar() {
        //            //    //    IdReacao = 109,
        //            //    //    Justificativa = "Reação cadastrada incorretamente"
        //            //    //},
        //            //    new ReacaoAInativar() {
        //            //        IdReacao = 112,
        //            //        Justificativa = "Reação cadastrada incorretamente"
        //            //    }
        //            //}
        //            //,
        //            //, ReacoesInativadas = new List<ReacaoAInativar>()
        //            //{
        //            //    new ReacaoAInativar() {
        //            //        IdReacao = 16,
        //            //        Justificativa = "Reação cadastrada incorretamente"
        //            //    }
        //            //    //,
        //            //    //new ReacaoAInativar() {
        //            //    //    IdReacao = 10,
        //            //    //    Justificativa = "Reação cadastrada incorretamente"
        //            //    //}
        //            //}
        //            //, ReacoesValidadas = new List<ReacaoValidada>() {
        //            //    new ReacaoValidada() { NrSequenciaSIH= 10 , Categoria =  "teste", Descricao = "teste" },
        //            //    new ReacaoValidada() { NrSequenciaSIH= 20 , Categoria =  "teste", Descricao = "teste" },
        //            //    new ReacaoValidada() { NrSequenciaSIH= 30 , Categoria =  "teste", Descricao = "teste" },
        //            //    new ReacaoValidada() { NrSequenciaSIH= 40 , Categoria =  "teste", Descricao = "teste" }
        //            //}
        //        },
        //        IdPaciente = 1,
        //        Usuario = "dmgmorais",
        //        DataCriacao = DateTime.Now,
        //        DataAlteracao = DateTime.Now
        //    };

        //    var servico = new ReacoesAdversasService(conn);
        //    var idDraft = servico.AdicionarDraft(rascunho, "dmgmorais", 1476);
        //    //servico.PromoverDraft(idDraft, "dmgmorais", null);

        //    Console.WriteLine("Promovido!!!");
        //    Console.ReadKey();
        //    Console.WriteLine(idDraft);
        //    //servico.PromoverDraft(idDraft, 1);
        //}

        public static void TestarDocumento()
        {
            var tipoDocumento = TipoDocumento.RegistroAlergias;
            var idPaciente = 1;
            var idEvento = 1;
            var usuario = "dmgmorais";

            using (IDbConnection db = new OracleConnection(conn))
            {
                //Como fazer usando uma transação
                db.Open();

                var reacao = new ReacaoAdversa()
                {
                    Categoria = new CategoriaDaReacao((int)CategoriasDaReacaoEnum.Alimento, "Älimento"),
                    Item = "Amendoim",
                    Descricao = "Uma reação de teste"
                };

                var trans = db.BeginTransaction();
                var repoDoc = new DocumentosRepository(db, trans);
                var repoReacao = new ReacoesAdversasRepository(db, trans);

                //var doc = new Documento()
                //{
                //    IdDocumento = 31,
                //    Versao = 2
                //};

                ////var res = repoReacao.Adicionar(doc, reacao);
                //var res = from doc in repoDoc.CriarNovo(tipoDocumento, idPaciente, idEvento, usuario)
                //          from r in repoReacao.Adicionar(doc, reacao)
                //              //from s in repoDoc.Inativar(doc.IdDocumento, doc.Versao, usuario)
                //              //from t in repoDoc.CriarNovaVersao(doc, 1, "helosreis")
                //          select r;

                //if (res.GetType() == typeof(None<ReacaoAdversa>))
                //{
                //    trans.Rollback();
                //}
                //else
                //{
                //    trans.Commit();
                //}

                //Console.WriteLine(res);
            }
        }

        public static void ObterReacaoAdversa()
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new ReacoesAdversasRepository(db);

                var r = repo.Retornar(41);

                Console.WriteLine(r);

                var r2 = repo.Retornar(42);

                Console.WriteLine(r2);

                var r3 = repo.Retornar(43);

                Console.WriteLine(r3);
            }
        }
        
        public static void AdicionarReacaoAdversa()
        {
            var reacaoAdversa = new ReacaoAdversa()
            {
                Categoria = new CategoriaDaReacao((int)CategoriasDaReacaoEnum.Medicamento, "Medicamento"),
                Descricao = "Uma nova reação de teste",
                Item = "Aas Protect 100mg",
                Substancias = new List<Substancia>()
                {
                    new Substancia() {
                        Code = "00090",
                        Value = "Acetilsalicilato de alumínio",
                        Terminology = "http://localhost:64361/terminologies/substancias/00090"
                    },
                    new Substancia() {
                        Code = "00091",
                        Value = "Acetilsalicilato de cálcio",
                        Terminology = "http://localhost:64361/terminologies/substancias/00091"
                    },
                    new Substancia() {
                        Code = "09600",
                        Value = "Acetilsalicilato de racelisina e glicina",
                        Terminology = "http://localhost:64361/terminologies/substancias/09600"
                    }
                },
                NotificarEventoAdverso = false,
                TiposDeReacao = new List<TipoDeReacao>()
                {
                    new TipoDeReacao((int)TipoDeReacaoEnum.Angioedema, "Angioedema"),
                    new TipoDeReacao((int)TipoDeReacaoEnum.Broncoespasmo, "Broncoespasmo")
                }
            };

            using (IDbConnection dbConnection = new OracleConnection(conn))
            {
                int idPaciente = 1;
                int idEvento = 1;
                string usuario = "dmgmorais";
                //dbConnection.Open();
                //var transaction = dbConnection.BeginTransaction();

                //var documentosRepository = new DocumentosRepository(dbConnection, transaction);
                //var reacoesRepository = new ReacoesAdversasRepository(dbConnection, transaction);

                //var result = from documentoAdicionado in documentosRepository.CriarNovo(TipoDocumento.RegistroAlergias, idPaciente, idEvento, usuario)
                //             from reacaoAdicionada in reacoesRepository.Adicionar(documentoAdicionado, reacaoAdversa)
                //             from substanciasAdicionadas in
                //                 reacaoAdversa.Substancias.Select(
                //                     substancia => reacoesRepository.AdicionarSubstancia(
                //                         reacaoAdicionada.IdReacaoAdversa, substancia)).ToList().AsOption()
                //             from tiposDeReacaoAdicionados in
                //                 reacaoAdversa.TiposDeReacao.Select(
                //                     tipoReacao => reacoesRepository.AdicionarTipoReacao(
                //                         reacaoAdicionada.IdReacaoAdversa, tipoReacao.Code)).ToList().AsOption()
                //             select new { documentoAdicionado, reacaoAdicionada, substanciasAdicionadas, tiposDeReacaoAdicionados };

                //EndOperation(result, transaction);

                //Console.WriteLine(result);
            }
        }

        public static void EndOperation<T>(Option<T> result, IDbTransaction transaction)
        {
            if (result.GetType() == typeof(None<T>))
                transaction.Rollback();
            else
                transaction.Commit();
        }
             
        public static void TesteRascunho()
        {
            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new ReacoesAdversasDraftsRepository(db);

                var resultado = repo.RetornarPorPaciente(1, "dmgmorais");

                Console.WriteLine(resultado);


                //var draft = new ReacoesAdversasDraft()
                //{
                //    Conteudo = new ConteudoReacoesAdversasDraft(
                //        NovasReacoes: new List<ReacaoAdversa>()
                //        {
                //            new ReacaoAdversa()
                //            {
                //                Categoria = new CategoriaDaReacao(414285001, "Teste"),
                //                Item = "Teste Reação 1",
                //                DescricaoDaReacao = "Teste",
                //                NotificarEventoAdverso = false
                //            }
                //        },
                //        ReacoesAtualizadas: new List<ReacaoAdversa>()
                //        {
                //            new ReacaoAdversa()
                //            {
                //                ID = 123,
                //                Categoria = new CategoriaDaReacao(414285001, "Teste"),
                //                Item = "Teste Reação 1",
                //                DescricaoDaReacao = "Teste",
                //                NotificarEventoAdverso = false
                //            }
                //        },
                //        IdsReacoesExcluidas: new List<int>() { 1, 2, 3, 4 },
                //        IdsReacoesPassadasALimpo: new List<int> { 5, 6, 7, 8 }
                //    )
                //};

                ////resultado = repo.Adicionar(draft, "dmgmorais", 1, 1);

                //var draftUpdate = new ReacoesAdversasDraft()
                //{
                //    Conteudo = new ConteudoReacoesAdversasDraft(
                //        NovasReacoes: new List<ReacaoAdversa>()
                //        {
                //            new ReacaoAdversa()
                //            {
                //                Categoria = new CategoriaDaReacao(414285001, "Teste"),
                //                Item = "Uhu",
                //                DescricaoDaReacao = "Teste",
                //                NotificarEventoAdverso = false
                //            }
                //        },
                //        ReacoesAtualizadas: new List<ReacaoAdversa>()
                //        {
                //            new ReacaoAdversa()
                //            {
                //                ID = 123,
                //                Categoria = new CategoriaDaReacao(414285001, "Teste"),
                //                Item = "Testando",
                //                DescricaoDaReacao = "Teste",
                //                NotificarEventoAdverso = false
                //            }
                //        },
                //        IdsReacoesExcluidas: new List<int>() { 3, 6 },
                //        IdsReacoesPassadasALimpo: new List<int> { 5, 6, 8 }
                //    )
                //};



                //repo.Atualizar(25, draftUpdate);
                //repo.Excluir(24);



                //var repo = new MedicamentosRepository(db);

                //var r = repo.GetSubstanciasByCode(1223);

                //Console.WriteLine(r);

                //foreach (var s in r)
                //    Console.WriteLine(s);


            }

            //Console.WriteLine(resultado);
        }

        public static void TesteMedicamentos()
        {
            //var conn = System.Configuration.ConfigurationManager.ConnectionStrings["Padrao"].ConnectionString;
            IEnumerable<MedicamentoResumido> resultados;

            //using (IDbConnection db = new OracleConnection(conn))
            //{
            //    var repo = new MedicamentosRepository(db);

            //    resultados = repo.GetByNome("ac");
            //}

            using (IDbConnection db = new OracleConnection(conn))
            {
                //var repo = new MedicamentosRepository(db);

                //var r = repo.GetSubstanciasByCode(1223);

                //Console.WriteLine(r);

                //foreach (var s in r)
                //    Console.WriteLine(s);

                
            }

            //foreach (var s in resultados)
            //{
            //    Console.WriteLine(s);
            //}
        }

        public static void TesteService()
        {
            //var conn = System.Configuration.ConfigurationManager.ConnectionStrings["Padrao"].ConnectionString;

            var tiporeacaoservico = new TiposDeReacaoTerminologyService(conn, true);

            IEnumerable<TipoDeReacao> resultados = tiporeacaoservico.GetAll();
            TipoDeReacao reacao = tiporeacaoservico.GetByCode(4386001);

            foreach (var s in resultados)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("GetBYCODE");
            Console.WriteLine(reacao);

        }

        public static void TesteRepo()
        {
            //var conn = System.Configuration.ConfigurationManager.ConnectionStrings["Padrao"].ConnectionString;
            IEnumerable<TipoDeReacao> resultados;
            TipoDeReacao reacao;

            using (IDbConnection db = new OracleConnection(conn))
            {
                var repo = new TiposDeReacaoRepository(db);

                resultados = repo.GetAll();
                reacao = repo.GetByCode(4386001);
            }

            foreach (var s in resultados)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("GetBYCODE");
            Console.WriteLine(reacao);
        }
    }
}
