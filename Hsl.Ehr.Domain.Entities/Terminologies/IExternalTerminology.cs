﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies
{
    public interface ISubstancia : ICodedText
    {
        //string DCB { get; }
        string Code { get; }
        string Terminology { get; }
    }
}
