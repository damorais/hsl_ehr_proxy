﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies
{
    public interface IInternalCode : ICodedText
    {
        string Code { get; }
    }
}
