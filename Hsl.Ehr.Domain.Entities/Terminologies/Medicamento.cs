﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies
{
    public class Medicamento
    {
        public int Code { get; set; }
        public string Value { get; set; }
        public IEnumerable<Substancia> Substancias { get; private set; }

        public Medicamento()
        {

        }

        public Medicamento(int Code, string Value, IEnumerable<Substancia> Substancias)
        {
            this.Code = Code;
            this.Value = Value;
            this.Substancias = Substancias;
        }

        public override string ToString() => $"Code: {this.Code} - Value: {this.Value} - Substancias: {this.Substancias.Count()}";
    }
}
