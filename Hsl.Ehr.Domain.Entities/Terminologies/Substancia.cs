﻿namespace Hsl.Ehr.Domain.Entities.Terminologies
{
    public class Substancia
    {
        public string Code { get; set; }
        public string DCB { get; set; }
        public string Value { get; set; }

        public Substancia()
        {

        }

        public Substancia(string Code, string DCB, string Value)
        {
            this.Code = Code;
            this.DCB = DCB;
            this.Value = Value;
        }

        public override string ToString() => $"DCB: {this.DCB} - Value: {this.Value}";
    }
}
