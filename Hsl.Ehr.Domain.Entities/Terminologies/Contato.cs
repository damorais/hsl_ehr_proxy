﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies
{
    public class Contato : ISnomedCodedText
    {
        public int Code { get; private set; }
        public string Value { get; private set; }

        public Contato()
        {

        }

        public Contato(int Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }

        public override string ToString() => $"Code: {this.Code} - Value: {this.Value}";
    }
}
