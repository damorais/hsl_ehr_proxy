﻿namespace Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente
{
    public class ClassificacaoDeDanoDaReacao : IInternalCode
    {
        public string Code { get; private set; }
        public string Value { get; private set; }

        public ClassificacaoDeDanoDaReacao(string Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }

        public override string ToString() => $"Code: {this.Code} - Value: {this.Value}";
    }
}

