﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas
{
    public enum CategoriasDaReacaoEnum
    {
        Alimento = 414285001,
        Medicamento = 416098002,
        Inalante = 116177003,
        Latex = 300916003,
        NegaAlergias = 1
    }

    public class CategoriaDaReacao : ISnomedCodedText
    {
        public int Code { get; set; }
        public string Value { get; set; }

        public CategoriaDaReacao()
        {

        }

        public CategoriaDaReacao(int Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }

        public override string ToString() => $"Code: {this.Code} - Value: {this.Value}";
    }
}
