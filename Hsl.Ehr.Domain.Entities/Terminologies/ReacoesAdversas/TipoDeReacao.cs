﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas
{
    public enum TipoDeReacaoEnum
    {
        AgitaçãoPsicomotora = 47295007,
        Angioedema = 402387002,
        Broncoespasmo = 4386001,
        ChoqueAnafilatico = 39579001,
        DermatiteAlergica = 24079001,
        DermatiteDeContatoAlergica = 238575004,
        EdemaDeGlote = 111278002,
        ErupcaoCutanea = 271807003,
        ParadaCardiaca = 410429000,
        RebaixamentoDoNivelDeConsciencia = 443371007,
        Prurido = 418363000,
        TosseAlergica = 300959008,
        Urticaria = 126485001
    }

    public class TipoDeReacao : ISnomedCodedText
    {
        public int Code { get; set; }
        public string Value { get; set; }

        public TipoDeReacao()
        {
        }

        public TipoDeReacao(int Code, string Value)
        {
            this.Code = Code;
            this.Value = Value;
        }

        public override string ToString() => $"Code: {this.Code} - Value: {this.Value}";
    }
}
