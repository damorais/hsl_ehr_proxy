﻿using System.Collections.Generic;
using System.Linq;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;

namespace Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts
{
    public class NovaReacao
    {
        public string CodigoTemporario { get; set; }
        public ReacaoAdversa ReacaoAdversa { get; set; }
    }

    public class ReacaoAInativar
    {
        public int IdReacao { get; set; }
        public string Categoria { get; set; }
        public string Descricao { get; set; }
        public string Justificativa { get; set; }
    }

    public class ReacaoValidada
    {
        public int NrSequenciaSIH { get; set; }
        public string Categoria { get; set; }
        public string Descricao { get; set; }
    }

    public class ConteudoReacoesAdversasDraft
    {
        public IEnumerable<NovaReacao> NovasReacoes { get; set; }
        public IEnumerable<ReacaoAdversa> ReacoesAtualizadas { get; set; }
        public IEnumerable<ReacaoAInativar> ReacoesInativadas { get; set; }
        public IEnumerable<ReacaoValidada> ReacoesValidadas { get; set; }

        public ConteudoReacoesAdversasDraft()
        {
            NovasReacoes = new List<NovaReacao>();
            ReacoesAtualizadas = new List<ReacaoAdversa>();
            ReacoesInativadas = new List<ReacaoAInativar>();
            ReacoesValidadas = new List<ReacaoValidada>();
        }

        public ConteudoReacoesAdversasDraft(
            IEnumerable<NovaReacao> NovasReacoes, IEnumerable<ReacaoAdversa> ReacoesAtualizadas,
            IEnumerable<ReacaoAInativar> ReacoesInativadas, IEnumerable<ReacaoValidada> ReacoesValidadas)
        {
            this.NovasReacoes = (NovasReacoes == null) ? 
                new List<NovaReacao>() : NovasReacoes;
            this.ReacoesAtualizadas = (ReacoesAtualizadas == null) ? 
                new List<ReacaoAdversa>() : ReacoesAtualizadas;
            this.ReacoesInativadas = (ReacoesInativadas == null) ? 
                new List<ReacaoAInativar>() : ReacoesInativadas;
            this.ReacoesValidadas = (ReacoesValidadas == null) ? 
                new List<ReacaoValidada>() : ReacoesValidadas;
        }

        public override string ToString() => 
            $"N: {NovasReacoes.Count()} - A: {ReacoesAtualizadas.Count()} - E: {ReacoesInativadas.Count()} - PL: {ReacoesValidadas.Count()}";
    }
}