﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts
{
    public class ReacoesAdversasDraft
    {
        public int IdDraft { get; set; }
        public ConteudoReacoesAdversasDraft Conteudo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
        public string Usuario { get; set; }
        public int IdPaciente { get; set; }

        public ReacoesAdversasDraft()
        {

        }

        public ReacoesAdversasDraft(int IdDraft, ConteudoReacoesAdversasDraft Conteudo, DateTime DataCriacao, DateTime DataAlteracao, string Usuario, int IdPaciente)
        {
            this.IdDraft = IdDraft;
            this.Conteudo = Conteudo;
            this.DataCriacao = DataCriacao;
            this.DataAlteracao = DataAlteracao;
            this.Usuario = Usuario;
            this.IdPaciente = IdPaciente;
        }

        public ReacoesAdversasDraft(ConteudoReacoesAdversasDraft Conteudo)
        {
            this.Conteudo = Conteudo;
            this.DataCriacao = DateTime.Now;
            this.DataAlteracao = DateTime.Now;
        }
    }
}
