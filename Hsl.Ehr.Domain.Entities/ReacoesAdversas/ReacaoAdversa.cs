﻿using System;
using System.Collections.Generic;
using Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Domain.Entidades.ReacoesAdversas
{
    //TODO: Ajustar os construtores
    public class ReacaoAdversa
    {

        public Documento Documento { get; set; }
        //public int IdDocumento { get; set; }
        //public int VersaoDocumento { get; set; }

        public int IdReacaoAdversa { get; set; }
        public CategoriaDaReacao Categoria { get; set; }
        public string Item { get; set; }
        public IEnumerable<Substancia> Substancias { get; set; }
        public IEnumerable<TipoDeReacao> TiposDeReacao { get; set; }
        public string Descricao { get; set; }
        public bool NotificarEventoAdverso { get; set; }
        public DateTime DataCriacao { get; set; }
        public string UsuarioCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
        public string UsuarioAlteracao { get; set; }
        public StatusDocumento Status { get; set; }
        public string JustificativaInativacao { get; set; }
        public int IdReacaoOriginal { get; set; }

        public string UsuarioBloqueio
        {
            get { return Documento == null? null : Documento.UsuarioBloqueio; }
        }

        public DateTime? DataBloqueio
        {
            get { return Documento == null ? null : Documento.DataBloqueio; }
        }

        public bool IsBloqueado
        {
            get
            {
                return (Documento != null &&!string.IsNullOrEmpty(Documento.UsuarioBloqueio) && Documento.DataBloqueio.HasValue);
            }
        }

        public ReacaoAdversa()
        {
            Categoria = new CategoriaDaReacao();
            Substancias = new List<Substancia>();
            TiposDeReacao = new List<TipoDeReacao>();
            DataCriacao = DateTime.Now;
            DataAlteracao = DateTime.Now;
        }

        public ReacaoAdversa(int IdReacaoAdversa, 
            Documento Documento, CategoriaDaReacao Categoria, 
            string Item,
            IEnumerable<Substancia> Substancias, IEnumerable<TipoDeReacao> TiposDeReacao,
            string Descricao, bool NotificarEventoAdverso, StatusDocumento Status, 
            string JustificativaInativacao,
            DateTime DataCriacao, string UsuarioCriacao,
            DateTime DataAlteracao, string UsuarioAlteracao,
            int IdReacaoOriginal)
        {
            this.IdReacaoAdversa = IdReacaoAdversa;
            this.Documento = Documento;
            this.Categoria = Categoria;
            this.Item = Item;
            this.Substancias = Substancias == null ? new List<Substancia>() : Substancias;
            this.TiposDeReacao = TiposDeReacao == null ? new List<TipoDeReacao>() : TiposDeReacao;
            this.Descricao = Descricao;
            this.NotificarEventoAdverso = NotificarEventoAdverso;
            this.Status = Status;
            this.JustificativaInativacao = JustificativaInativacao;
            this.DataCriacao = DataCriacao;
            this.UsuarioCriacao = UsuarioCriacao;
            this.DataAlteracao = DataAlteracao;
            this.UsuarioAlteracao = UsuarioAlteracao;
            this.IdReacaoOriginal = IdReacaoOriginal;
        }
        //public NotificacaoEventoAdverso NotificacaoEventoAdverso { get; set; }

        public override string ToString() => $"ID: {IdReacaoAdversa}, Item: {Item}";
    }

    //TODO: Preciso reintegrar este objeto na estrutura da reação adversa
    //public class NotificacaoEventoAdverso
    //{
    //    public bool HouveDanoAoPaciente { get; set; }
    //    public DateTime DataDeInicioDaReacao { get; set; }
    //    public bool ViaDeAdministracaoIntravenosa { get; set; }
    //    public string TempoDeInfusao { get; set; }
    //    public string ComentariosSobreOEvento { get; set; }
    //    public string IntervencaoClinicaImediata { get; set; }
    //    public ClassificacaoDeDanoDaReacao ClassificacaoDoDano { get; set; }
    //}

    public class Substancia : ISubstancia
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public string Terminology { get; set; }
        public string DCB { get { return Code; } }

        public Substancia()
        {

        }

        public Substancia(string Code, string Value, string Terminology)
        {
            this.Code = Code;
            this.Value = Value;
            this.Terminology = Terminology;
        }

        public override string ToString() =>
            $"Substancia is: Code - {Code} - Value - {Value} - Terminology - {Terminology}";
    }
}
