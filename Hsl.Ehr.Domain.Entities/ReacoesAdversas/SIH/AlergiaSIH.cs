﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities.ReacoesAdversas.SIH
{
    public class AlergiaSIH
    {
        public int NrSequenciaSIH { get; set; }
        public string Tipo { get; set; }
        public string Descricao { get; set; }
        public string Reacao { get; set; }
    }
}
