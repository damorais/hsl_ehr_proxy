﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities
{
    public enum TipoDocumento
    {
        RegistroAlergias = 1
    }

    public enum StatusDocumento
    {
        Ativo = 1,
        Inativo = 2,
        Modificado = 3
    }

    public class Documento
    {
        public int IdDocumento { get; set; }
        public TipoDocumento TipoDocumento { get; private set; }
        public DateTime DataCriacao { get; private set; }
        public string UsuarioCriacao { get; private set; }
        public DateTime DataAlteracao { get; private set; }
        public string UsuarioAlteracao { get; private set; }
        public StatusDocumento Status { get; private set; }

        public int IdEvento { get; set; }
        public int IdPaciente { get; set; }
        public int Versao { get; set; }
        public string UsuarioBloqueio { get; set; }
        public DateTime? DataBloqueio { get; set; }

        public Documento()
        {
            this.DataCriacao = DateTime.Now;
            this.DataAlteracao = DateTime.Now;
            this.DataBloqueio = null;
        }

        public Documento(int IdDocumento, int Versao, TipoDocumento TipoDocumento, 
            DateTime DataCriacao, string UsuarioCriacao, 
            DateTime DataAlteracao, string UsuarioAlteracao,
            DateTime? DataBloqueio, string UsuarioBloqueio,

            StatusDocumento Status, int IdEvento, int IdPaciente)
        {
            this.IdDocumento = IdDocumento;
            this.Versao = Versao;
            this.TipoDocumento = TipoDocumento;
            this.DataCriacao = DataCriacao;
            this.UsuarioCriacao = UsuarioCriacao;
            this.DataAlteracao = DataAlteracao;
            this.UsuarioAlteracao = UsuarioAlteracao;
            this.DataBloqueio = DataBloqueio;
            this.UsuarioBloqueio = UsuarioBloqueio;
            this.Status = Status;
            this.IdEvento = IdEvento;
            this.IdPaciente = IdPaciente;
        }

        public override string ToString() =>
            $"STATUS: {Status}, Documento: {IdDocumento}, Tipo: {TipoDocumento}, DataCriacao: {DataCriacao.ToString()}, Usuario: {UsuarioCriacao}, Evento: {IdEvento}, Paciente: {IdPaciente}";
    }
}
