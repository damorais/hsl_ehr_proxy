﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Entities
{
    public enum TipoEvento
    {
        RegistroReacaoAdversa = 1
    }

    public class Evento
    {
        public int IdEvento { get; set; }
        public string Descricao { get; set; }
        public TipoEvento TipoEvento { get; set; }
        public DateTime DataCriacao { get; set; }
        public int? NumeroAtendimentoSIH { get; set; }

        public Evento()
        {

        }

        public Evento(int IdEvento, string Descricao, 
            TipoEvento TipoEvento, DateTime DataCriacao,
            int? NumeroAtendimentoSIH)
        {
            this.IdEvento = IdEvento;
            this.Descricao = Descricao;
            this.TipoEvento = TipoEvento;
            this.DataCriacao = DataCriacao;
            this.NumeroAtendimentoSIH = NumeroAtendimentoSIH;
        }
    }
}
