﻿using System;
using HslEhrProxy.Services.Archetypes.AdverseReaction;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HslEhrProxy.Services.Tests.Archetypes.AdverseReaction
{
    [TestClass]
    public class ClassificacaoDanoInternalCodeTest
    {
        [TestMethod]
        public void GetClassificacaoDanoInternalCodeTest()
        {
            var classificacaoDanoNenhuma = 
                ClassificacaoDanoInternalCode.GetClassificacaoDano(
                    TipoClassificacaoDano.Nenhum);
            var classificacaoDanoLeve = 
                ClassificacaoDanoInternalCode.GetClassificacaoDano(
                    TipoClassificacaoDano.Leve);
            var classificacaoDanoModerado = 
                ClassificacaoDanoInternalCode.GetClassificacaoDano(
                    TipoClassificacaoDano.Moderado);
            var classificacaoDanoGrave = 
                ClassificacaoDanoInternalCode.GetClassificacaoDano(
                    TipoClassificacaoDano.Grave);

            //Os enums devem mapear exatamente os valores possíveis para
            //o tipo de classificação de dano
            Assert.AreEqual(
                Enum.GetNames(typeof(TipoClassificacaoDano)).Length,
                ClassificacaoDanoInternalCode.AllowedValues.Count);

            //Asserção dos tipos de classificação disponíveis
            Assert.IsNull(classificacaoDanoNenhuma);
            Assert.AreEqual("at0010", classificacaoDanoLeve.Code);
            Assert.AreEqual("at0005", classificacaoDanoModerado.Code);
            Assert.AreEqual("at0007", classificacaoDanoGrave.Code);
        }
    }
}
