﻿using System;
using HslEhrProxy.Services.Archetypes.AdverseReaction;
using HslEhrProxy.Services.Basic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HslEhrProxy.Services.Tests.Archetypes.AdverseReaction
{
    [TestClass]
    public class AdverseReactionDamageTest
    {
        [TestMethod]
        public void VerifyTipoDanoReacaoPacienteTest()
        {
            var danoReacaoPacienteFalse = new AdverseReactionDamage(false);
            var danoReacaoPacienteTrue = new AdverseReactionDamage(false);

            //A instância, para ambos os casos, deve ser subtipo de ICluster
            Assert.IsInstanceOfType(danoReacaoPacienteFalse, typeof(ICluster));
            Assert.IsInstanceOfType(danoReacaoPacienteTrue, typeof(ICluster));
        }

        [TestMethod]
        public void InitDanoReacaoPacienteFalseTest()
        {
            var danoReacaoPaciente = new AdverseReactionDamage(false);

            //Se não houve dano para o paciente, não deve ter classificação
            //do dano
            Assert.IsFalse(danoReacaoPaciente.DanoParaOPaciente);
            Assert.IsNull(danoReacaoPaciente.ClassificacaoDano);
        }

        [TestMethod]
        public void InitDanoReacaoPacienteTrueTest()
        {
            var classificacaoDanoEsperada = 
                ClassificacaoDanoInternalCode.GetClassificacaoDano(
                    TipoClassificacaoDano.Moderado);

            var danoReacaoPaciente = 
                new AdverseReactionDamage(true, TipoClassificacaoDano.Moderado);

            //Se houve dano para o paciente, deve haver a classificação 
            //do dano
            Assert.IsTrue(danoReacaoPaciente.DanoParaOPaciente);
            Assert.IsNotNull(danoReacaoPaciente.ClassificacaoDano);
            Assert.AreEqual(
                classificacaoDanoEsperada, 
                danoReacaoPaciente.ClassificacaoDano
            );
        }
    }
}
