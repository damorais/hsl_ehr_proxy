﻿using System;
using System.Collections.Generic;
using System.Linq;
using HslEhrProxy.Services.Archetypes.AdverseReaction;
using HslEhrProxy.Services.Basic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HslEhrProxy.Services.Tests.Archetypes.AdverseReaction
{
    [TestClass]
    public class EventoAdversoClusterTest
    {
        public readonly IEnumerable<SnomedCodedText> tiposReacao = new List<SnomedCodedText>()
        {
            new SnomedCodedText("R1", "Reação Exemplo 1"),
            new SnomedCodedText("R2", "Reação Exemplo 2")
        };

        [TestMethod]
        public void InitDevePossuirTiposDeReacaoDescricaoEDataInicioTest()
        {
            var eventoAdversoCluster = new EventoAdverso(tiposReacao, "Uma reacao", DateTime.Now);

            Assert.IsNotNull(eventoAdversoCluster.TiposDeReacao);
            Assert.AreNotEqual(0,                   eventoAdversoCluster.TiposDeReacao.Count());

            Assert.IsNotNull(eventoAdversoCluster.DescricaoReacao);
            Assert.AreNotEqual(string.Empty,        eventoAdversoCluster.DescricaoReacao);

            Assert.AreNotEqual(DateTime.MinValue,   eventoAdversoCluster.DataInicioReacao);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InitNaoDevePermitirSemTipoReacaoTest()
        {
            SnomedCodedText tipoReacao = null;

            new EventoAdverso(tipoReacao, null, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InitNaoDevePermitirListaTipoReacaoVaziaTest()
        {
            new EventoAdverso(new List<SnomedCodedText>(), null, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InitNaoDevePermitirSemDescricaoTest()
        {
            new EventoAdverso(tiposReacao, null, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InitNaoDevePermitirComDescricaoEmBrancoTest()
        {
            new EventoAdverso(tiposReacao, string.Empty, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InitNaoDevePermitirDateMinValueEmDataInicioTest()
        {
            new EventoAdverso(tiposReacao, "Uma reação qualquer", DateTime.MinValue);
        }
    }

    [TestClass]
    public class AdverseReactionRiskTest
    {
        //[TestMethod]
        //[ExpectedException(typeof(ArgumentException))]
        //public void InitDevePossuirCategoriaItemTest()
        //{
        //    var DetalheEvento = new DetalhesEvento();
             
        //    //throw new NotImplementedException();
        //}
    }
}
