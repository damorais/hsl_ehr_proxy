﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Hsl.Ehr.Api.Utils
{
    public class RequisitionHeader
    {
        public string Usuario { get; private set; }
        public int? NumeroAtendimentoSIH { get; private set; }

        public RequisitionHeader(string Usuario, int? NumeroAtendimentoSIH)
        {
            this.Usuario = Usuario;
            this.NumeroAtendimentoSIH = NumeroAtendimentoSIH;
        }
    }

    public class RequisitionHeaderManager
    {
        public static RequisitionHeader GetRequisitionHeader(HttpRequestHeaders RequestHeaders)
        {
            string usuario = RequestHeaders.Authorization.ToString();
            int? numeroAtendimentoSIH = null;

            IEnumerable<string> tempNrAtendimento = null;
            if (RequestHeaders.TryGetValues("Nr-Atendimento-SIH", out tempNrAtendimento))
            {
                if (tempNrAtendimento.Count() == 1)
                    numeroAtendimentoSIH = int.Parse(tempNrAtendimento.First());
                else if (tempNrAtendimento.Count() > 1)
                    throw new HttpResponseException(System.Net.HttpStatusCode.BadRequest);
            }

            return new RequisitionHeader(usuario, numeroAtendimentoSIH);
        }
    }
}