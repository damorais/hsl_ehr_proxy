﻿using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.SIH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Hsl.Ehr.Api.Controllers.Sih
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ehr/paciente/{IdPaciente}/sih")]
    public class AlergiasController : ApiController
    {
        [Route("alergias-a-transcrever.json")]
        public IEnumerable<AlergiaSIH> Get(int IdPaciente)
        {
            return ServicesProvider.GetAlergiasSihService().RecuperarAlergiasNaoTranscritas(IdPaciente);
        }
    }
}
