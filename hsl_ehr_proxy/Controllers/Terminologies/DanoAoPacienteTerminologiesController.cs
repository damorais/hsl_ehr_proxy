﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente;
using Hsl.Ehr.Domain.Factories;
using Hsl.Ehr.Domain.Services;
using Hsl.Ehr.Domain.Services.Terminologies;
using Hsl.Ehr.Domain.Services.Terminologies.DanoAoPaciente;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/dano-ao-paciente")]
    public class DanoAoPacienteTerminologiesController : ApiController
    {
        private IDanoAoPacienteTerminologyService TerminologyService;

        public DanoAoPacienteTerminologiesController()
        {
            TerminologyService = DanoAoPacienteTerminologyFactory.GetTerminologyService(TerminologyTypeEnum.STATIC);
        }

        [Route("classificacoes")]
        public IEnumerable<ClassificacaoDeDanoDaReacao> GetClassificacoesDeDano()
        {
            return TerminologyService.GetClassificacoesDeDano();
        }
    }
}