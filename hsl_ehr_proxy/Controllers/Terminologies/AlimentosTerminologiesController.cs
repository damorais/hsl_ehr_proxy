﻿using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/alimentos")]
    public class AlimentosTerminologiesController : ApiController
    {
        [Route("")]
        public IEnumerable<Alimento> Get([FromUri] string Nome = "")
        {
            var nomeNormalizado = HttpUtility.UrlDecode(Nome.Replace("[@@@]", "%"));

            return nomeNormalizado == string.Empty ? 
                ServicesProvider.GetAlimentosService().GetAll() :
                ServicesProvider.GetAlimentosService().GetByValue(nomeNormalizado);
        }

        [Route("{Code}")]
        public Alimento Get(int Code)
        {
            return ServicesProvider.GetAlimentosService().GetByCode(Code);
        }
    }
}
