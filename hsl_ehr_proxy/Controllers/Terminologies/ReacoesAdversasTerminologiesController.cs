﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Domain.Factories;
using Hsl.Ehr.Domain.Services;
using Hsl.Ehr.Domain.Services.Terminologies;
using Hsl.Ehr.Domain.Services.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/reacoes-adversas")]
    public class ReacoesAdversasTerminologiesController : ApiController
    {
        private IReacoesAdversasTerminologyService TerminologyService;

        public ReacoesAdversasTerminologiesController()
        {
            TerminologyService = ReacoesAdversasTerminologyFactory.GetTerminologyService(TerminologyTypeEnum.STATIC);
        }

        [Route("categorias.json")]
        public IEnumerable<CategoriaDaReacao> GetCategorias()
        {
            return ServicesProvider.GetCategoriasDeReacaoService().GetAll();
        }

        [Route("categorias/{Code}.json")]
        public CategoriaDaReacao GetCategoriaByCode(int Code)
        {
            return ServicesProvider.GetCategoriasDeReacaoService().GetByCode(Code);
        }
        
        [Route("tipos.json")]
        public IEnumerable<TipoDeReacao> GetTiposDeReacao()
        {
            return ServicesProvider.GetTiposDeReacaoService().GetAll();
        }

        [Route("tipos/{Code}.json")]
        public TipoDeReacao GetTipoDeReacaoByCode(int Code)
        {
            return ServicesProvider.GetTiposDeReacaoService().GetByCode(Code);
        }
    }
}
