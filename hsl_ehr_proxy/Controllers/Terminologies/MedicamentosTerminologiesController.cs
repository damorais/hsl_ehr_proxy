﻿using System.Collections.Generic;
using System.Web.Http;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.ValueObjects.Terminology;
using System.Web.Http.Cors;
using System.Web;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/medicamentos")]
    public class MedicamentosTerminologiesController: ApiController
    {
        [Route("")]
        public IEnumerable<MedicamentoResumido> Get([FromUri] string Nome = "")
        {
            var nomeNormalizado = HttpUtility.UrlDecode(Nome.Replace("[@@@]", "%"));

            return ServicesProvider.GetMedicamentosService().GetByValue(nomeNormalizado);
        }

        [Route("{Code}")]
        public Medicamento Get(int Code)
        {
            return ServicesProvider.GetMedicamentosService().GetByCode(Code);
        }
    }

    [RoutePrefix("terminologies/substancias")]
    public class SubstanciasTerminologiesController : ApiController
    {
        [Route("{DCB}")]
        public Substancia Get(string DCB)
        {
            return ServicesProvider.GetMedicamentosService().GetSubstanciaByDCB(DCB);
        }
    }
}