﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/inalantes")]
    public class InalantesTerminologiesController : ApiController
    {
        [Route("")]
        public IEnumerable<Inalante> Get([FromUri] string Nome = "")
        {
            var nomeNormalizado = HttpUtility.UrlDecode(Nome.Replace("[@@@]", "%"));

            return nomeNormalizado == string.Empty ?
                ServicesProvider.GetInalantesService().GetAll() :
                ServicesProvider.GetInalantesService().GetByValue(nomeNormalizado);
        }

        [Route("{Code}")]
        public Inalante Get(int Code)
        {
            return ServicesProvider.GetInalantesService().GetByCode(Code);
        }
    }
}