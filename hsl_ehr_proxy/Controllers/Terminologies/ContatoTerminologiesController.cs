﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Api.Controllers.Terminologies
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("terminologies/contato")]
    public class ContatoTerminologiesController : ApiController
    {
        [Route("")]
        public IEnumerable<Contato> Get([FromUri] string Nome = "")
        {
            var nomeNormalizado = HttpUtility.UrlDecode(Nome.Replace("[@@@]", "%"));

            return nomeNormalizado == string.Empty ?
                ServicesProvider.GetContatoService().GetAll() :
                ServicesProvider.GetContatoService().GetByValue(nomeNormalizado);
        }

        [Route("{Code}")]
        public Contato Get(int Code)
        {
            return ServicesProvider.GetContatoService().GetByCode(Code);
        }
    }
}
