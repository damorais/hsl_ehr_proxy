﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Domain.Abstract.Repositories;
using Hsl.Ehr.Domain.Entidades;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using System;
using Hsl.Ehr.Util.AdvancedTypes;
using Hsl.Ehr.Domain.Marand;

namespace Hsl.Ehr.Api.Controllers.ReacoesAdversas
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ehr/paciente/{IdPaciente}")]
    public class ReacoesAdversasController : ApiController
    {
        public ReacoesAdversasController()
        {
        }

        [Route("reacoes-adversas")]
        public IEnumerable<ReacaoAdversa> Get(int IdPaciente)
        {
            return ServicesProvider.GetReacoesAdversasService().RetornarReacoesAtivasPorPaciente(IdPaciente);
        }

        [Route("reacoes-adversas-inativas")]
        public IEnumerable<ReacaoAdversa> GetInativas(int IdPaciente)
        {
            return ServicesProvider.GetReacoesAdversasService().RetornarReacoesInativasPorPaciente(IdPaciente);
        }

        [Route("reacoes-adversas/{IdReacaoAdversa}")]
        public ReacaoAdversa Get(int IdPaciente, int IdReacaoAdversa)
        {
            var reacaoAdversa = ServicesProvider.GetReacoesAdversasService().RetornarReacaoAdversa(IdPaciente, IdReacaoAdversa);

            if (Option.IsNone(reacaoAdversa))
                throw new HttpResponseException(System.Net.HttpStatusCode.NoContent);

            return Option.Some(reacaoAdversa);
        }

        [Route("reacoes-adversas/{IdReacaoAdversa}/historico")]
        public IEnumerable<ReacaoAdversa> GetHistorico(int IdPaciente, int IdReacaoAdversa)
        {
            return ServicesProvider.GetReacoesAdversasService().RetornarHistoricoReacao(IdReacaoAdversa);
        }

        //[Route("reacoes-adversas/{IdReacaoAdversa}/marand")]
        //public Dictionary<string, object> GetMarand(int IdPaciente, int IdReacaoAdversa)
        //{
        //    var result = Option.Some(ServicesProvider.GetReacoesAdversasService().RetornarReacaoAdversa(IdPaciente, IdReacaoAdversa)).AsMarandComposition();

        //    return result;//Transform(Option.Some(ServicesProvider.GetReacoesAdversasService().RetornarReacaoAdversa(IdPaciente, IdReacaoAdversa)));
        //}

        //[Route("reacoes-adversas/{IdReacaoAdversa}/historico")]
        //public string GetHistorico(int IdPaciente, int IdPaciente)
        //{
        //    throw new NotImplementedException();
        //}

        //[Route("reacoes-adversas")]
        //public IEnumerable<ReacaoAdversa> Post(int IdPaciente, [FromBody] IEnumerable<ReacaoAdversa> ReacoesAdversas)
        //{
        //    throw new NotImplementedException();
        //}

        //[Route("{IdPaciente}/reacoes-adversas/{IdReacaoAdversa}/trans")]
        //public Dictionary<string, object> GetTransf(int IdPaciente, int IdReacaoAdversa)
        //{
        //    var r = RARepository.Get(1, 1);

        //    var result = new Dictionary<string, object>();

        //    if(r!= null)
        //    {
        //        result = this.Transform(r);
        //    }

        //    return result;
        //}

        //public Dictionary<string, object> Transform(ReacaoAdversa ReacaoAdversa)
        //{
        //    var d = new Dictionary<string, object>()
        //    {
        //        { "ctx/language", "pt" },
        //        { "ctx/territory", "BR" },
        //        { "ctx/composer_name", $"{ReacaoAdversa.UsuarioAlteracao}|enfermagem"},//"sblake|Silvia Blake|enfermeira" },
        //        { "ctx/time", DateTime.Now },
        //        {
        //            "lista_de_reações_adversas", new List<Dictionary<string, object>>()
        //            {
        //                new Dictionary<string, object>()
        //                {
        //                    {
        //                        "risco_de_reação_adversa_alergias", new List<Dictionary<string, object>>()
        //                        {
        //                            new Dictionary<string, object>()
        //                            {
        //                                { "categoria",  ReacaoAdversa.Categoria.AsMarandPropertyItem() },
        //                                { "item",       ReacaoAdversa.Item },

        //                                #region Detalhes do Evento
        //                                {
        //                                    "detalhes_do_evento", new Dictionary<string, object>()
        //                                    {
        //                                        {
        //                                            "substancia", ReacaoAdversa.Substancias.AsMarandPropertyItem()
        //                                        },
        //                                        {
        //                                            "evento_adverso", new Dictionary<string, object>()
        //                                            {
        //                                                { "tipo_de_reação", ReacaoAdversa.TiposDeReacao.AsMarandPropertyItem() },
        //                                                { "descrição_da_reação", ReacaoAdversa.Descricao },
        //                                                { "data_início_da_reação", ReacaoAdversa.DataCriacao.AsMarandPropertyItemOnlyDate() },
        //                                            }
        //                                        }
        //                                    }

        //                                },
        //                                #endregion
        //                                { "notificar_evento_adverso", ReacaoAdversa.NotificarEventoAdverso }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    };

        //    return d;
        //}



    }
}
