﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Hsl.Ehr.Api.Configuration;
using Hsl.Ehr.Api.Utils;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;
using Hsl.Ehr.Util.AdvancedTypes;

namespace Hsl.Ehr.Api.Controllers.ReacoesAdversas
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("ehr/paciente/{IdPaciente}/rascunhos/reacoes-adversas")]
    public class ReacoesAdversasDraftsController : ApiController
    {
        [Route("")]
        public ReacoesAdversasDraft Get(int IdPaciente)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            return ServicesProvider.GetReacoesAdversasService().RetornarDraftPorPaciente(IdPaciente, requestHeader.Usuario);
        }

        [Route("novas-nao-liberadas")]
        public IEnumerable<ReacaoAdversa> GetReacoesNovasNaoLiberadas(int IdPaciente)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            var resultado = ServicesProvider.GetReacoesAdversasService().RetornarNovasReacoesPorOutrosEmRascunho(IdPaciente, requestHeader.Usuario).ToList();

            return resultado;
        }

        [Route("")]
        public int Post(int IdPaciente, [FromBody] ReacoesAdversasDraft Draft)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            return ServicesProvider.GetReacoesAdversasService().AdicionarDraft(Draft, requestHeader.Usuario, IdPaciente);
        }       

        [Route("{IdReacaoAdversaDraft}")]
        public bool Delete(int IdReacaoAdversaDraft)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            var resultado = ServicesProvider.GetReacoesAdversasService().ExcluirDraft(IdReacaoAdversaDraft, requestHeader.Usuario);

            if(resultado.GetType() == typeof(Failure<string>))
            {
                var erro = (Failure<string>)resultado;

                if(erro.Error.GetType() == typeof(System.UnauthorizedAccessException))
                    throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            }


            return true;
        }

        [Route("{IdReacaoAdversaDraft}")]
        public bool Put(int IdPaciente, int IdReacaoAdversaDraft, [FromBody] ReacoesAdversasDraft Draft)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            ServicesProvider.GetReacoesAdversasService().AtualizarDraft(IdReacaoAdversaDraft, Draft, requestHeader.Usuario);

            return true;
        }

        [Route("{IdReacaoAdversaDraft}")]
        public bool Post(int IdPaciente, int IdReacaoAdversaDraft)
        {
            var requestHeader = RequisitionHeaderManager.GetRequisitionHeader(Request.Headers);

            return ServicesProvider.GetReacoesAdversasService().PromoverDraft(IdReacaoAdversaDraft, requestHeader.Usuario, requestHeader.NumeroAtendimentoSIH);
        }
    }
}