﻿using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Hsl.Ehr.Api.Configuration;

namespace Hsl.Ehr.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            //Configurações da aplicação
            AppConfig.RegisterFactories();
            AppConfig.RegisterServices();
        }
    }
}
