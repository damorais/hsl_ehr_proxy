﻿using System;

namespace Hsl.Ehr.Util.AdvancedTypes
{
    public interface Option<T> { }

    public static class Option
    {
        public static Option<T> From<T>(T Value) => Value.AsOption();
        public static Option<T> None<T>() => new None<T>();

        public static bool IsNone<T>(Option<T> Value) => Value.GetType() == typeof(None<T>);
        public static T Some<T>(Option<T> Value) {
            if (Value.GetType() == typeof(Some<T>))
                return ((Some<T>)Value).Value;
            else
                throw new InvalidCastException();
        }
    }

    public class Some<T> : Option<T>
    {
        public T Value { get; private set; }

        public Some(T Value)
        {
            this.Value = Value;
        }

        public override string ToString() => $"Some({Value.ToString()})";
    }

    public class None<T> : Option<T>
    {
        public override string ToString() => $"None";
    }

    public static class OptionsExtensions
    {
        public static Option<T> AsOption<T>(this T Value)
        {
            if (Value == null)
                return new None<T>();
            else
                return new Some<T>(Value);
        }

        public static Option<B> Select<A, B>(this Option<A> a, Func<A, Option<B>> func)
        {
            var someA = a as Some<A>;

            return someA == null ? new None<B>() : func(someA.Value);
        }

        //public static Option<B> SelectMany<A, B>(this Option<A> a, Func<A, Option<B>> select)
        //{
        //    return a.Select(aValue => aValue.AsOption());
        //          //func(aValue).Select(bValue =>
        //          //  select(aValue, bValue).AsOption()));
        //}

        public static Option<C> SelectMany<A, B, C>(this Option<A> a, Func<A, Option<B>> func, Func<A, B, C> select)
        {
            return a.Select(aValue =>
                  func(aValue).Select(bValue =>
                    select(aValue, bValue).AsOption()));
        }
    }
}
