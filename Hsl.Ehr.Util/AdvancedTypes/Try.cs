﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Util.AdvancedTypes
{
    public abstract class Try<T> { }

    public class Success<T> : Try<T>
    {
        public T Value { get; }

        public Success(T Value)
        {
            this.Value = Value;
        }

        public override string ToString() => $"Success({Value.ToString()})";
    }

    public class Failure<T> : Try<T>
    {
        public Exception Error;
        public Failure(Exception Error)
        {
            this.Error = Error;
        }

        public override string ToString() => $"Failure({Error.Message})";
    }

    public static class TryExtensions
    {
        public static Try<T> AsTry<T>(this object Value)
        {
            //Type type = Value.GetType();

            //if (type != typeof(Exception) && !type.IsSubclassOf(typeof(Exception)))
            //    return new Success<T>((T)Value);
            //else
            //    return new Failure<T>((Exception)Value);
            throw new NotImplementedException();
        }
    }
}
