﻿module CompositionsRest

open FSharp.Data
open FSharp.Data.JsonExtensions

let compositionBaseUrl = BaseRest.urlBase :: "composition" :: [] |> String.concat "/"

let targetGetCompositionAddress (compositionId:System.Guid) = 
    compositionBaseUrl :: (compositionId.ToString()) :: []

let getComposition (compositionId:System.Guid) = 
    let targetCompositionAddress = 
        targetGetCompositionAddress compositionId
        |> BaseRest.buildUrl
    Http.Request(targetCompositionAddress, silentHttpErrors = true) 
    |> BaseRest.printResponse