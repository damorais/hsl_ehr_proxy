﻿module BaseRest

open FSharp.Data

let urlBase = "http://localhost:8088"

let buildUrl parameterList =
    parameterList |> String.concat "/"


let printResponse response =
    response.StatusCode 
    |> printfn "Response Status Code: %i" 
    
    match response.Headers.TryFind "Content-Type" with
    | Some(value) -> printfn "Content-Type: %s" value
    | None -> printfn "Content-Type: UNDEFINED"         
    
    match response.Body with
    | Text text -> JsonValue.Parse(text) |> string |> printfn "Response body: \n %s"
    | Binary bytes -> printfn "Got %d bytes of bynary content." bytes.Length 