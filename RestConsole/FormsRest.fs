﻿module FormsRest

open FSharp.Data
open FSharp.Data.JsonExtensions

let formBaseUrl = BaseRest.urlBase :: "form" :: [] |> String.concat "/"

let targetGetFormAddress formName formVersion = 
    formBaseUrl :: formName :: formVersion :: []


let targetGetFormAddressResource formName formVersion formResource =
    targetGetFormAddress formName formVersion @ "resource" :: formResource :: []

let getFormByVersion formName formVersion =
    let targetFormAddress = 
        targetGetFormAddress formName formVersion 
        |> BaseRest.buildUrl
    Http.Request(targetFormAddress, silentHttpErrors = true) 
    |> BaseRest.printResponse


let getFormResource formResource formName formVersion =
    let targetAddress = 
        targetGetFormAddressResource formName formVersion formResource 
        |> BaseRest.buildUrl
    Http.Request(targetAddress, silentHttpErrors = true)
    |> BaseRest.printResponse