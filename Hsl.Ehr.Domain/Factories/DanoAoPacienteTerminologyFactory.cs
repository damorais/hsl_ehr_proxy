﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Services.Terminologies;
using Hsl.Ehr.Domain.Services.Terminologies.DanoAoPaciente;
using Hsl.Ehr.Domain.Services.Terminologies.DanoAoPaciente.Static;

namespace Hsl.Ehr.Domain.Factories
{
    public class DanoAoPacienteTerminologyFactory
    {
        private static IDanoAoPacienteTerminologyService TerminologyService { get; set; }

        public static IDanoAoPacienteTerminologyService GetTerminologyService(TerminologyTypeEnum TerminologyType)
        {
            if (TerminologyService == null)
            {
                switch (TerminologyType)
                {
                    case TerminologyTypeEnum.STATIC:
                        if (TerminologyService == null)
                            TerminologyService = new DanoAoPacienteTerminologyService();
                        break;
                    default:
                        break;
                }
            }

            return TerminologyService;
        }
    }
}
