﻿using Hsl.Ehr.Domain.Services.Terminologies;
using Hsl.Ehr.Domain.Services.Terminologies.ReacoesAdversas;
using Hsl.Ehr.Domain.Services.Terminologies.ReacoesAdversas.Static;

namespace Hsl.Ehr.Domain.Factories
{
    public class ReacoesAdversasTerminologyFactory
    {
        private static IReacoesAdversasTerminologyService TerminologyService { get; set; }

        public static IReacoesAdversasTerminologyService GetTerminologyService(TerminologyTypeEnum TerminologyType)
        {
            if (TerminologyService == null)
            {
                switch (TerminologyType)
                {
                    case TerminologyTypeEnum.STATIC:
                        if (TerminologyService == null)
                            TerminologyService = new ReacoesAdversasTerminologyService();
                        break;
                    default:
                        break;
                }
            }

            return TerminologyService;
        }
    }
}
