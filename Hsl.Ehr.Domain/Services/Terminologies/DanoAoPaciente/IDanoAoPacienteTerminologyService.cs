﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente;

namespace Hsl.Ehr.Domain.Services.Terminologies.DanoAoPaciente
{
    public interface IDanoAoPacienteTerminologyService
    {
        IEnumerable<ClassificacaoDeDanoDaReacao> GetClassificacoesDeDano();
    }
}
