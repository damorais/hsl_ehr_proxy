﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies.DanoAoPaciente;

namespace Hsl.Ehr.Domain.Services.Terminologies.DanoAoPaciente.Static
{
    internal class DanoAoPacienteTerminologyService : IDanoAoPacienteTerminologyService
    {
        private static IEnumerable<ClassificacaoDeDanoDaReacao> ClassificacoesDoDano = new List<ClassificacaoDeDanoDaReacao>()
        {
            new ClassificacaoDeDanoDaReacao(Code: "at0010", Value: "Leve - Com ou sem necessidade de intervenção clínica"),
            new ClassificacaoDeDanoDaReacao(Code: "at0005", Value: "Moderado - Com necessidade de intervenção clínica"),
            new ClassificacaoDeDanoDaReacao(Code: "at0007", Value: "Grave - Grave - Hemorragia, Broncoaspiração, Edema de Glote ...")
        };

        public IEnumerable<ClassificacaoDeDanoDaReacao> GetClassificacoesDeDano()
        {
            return ClassificacoesDoDano;
        }
    }
}
