﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Domain.Services.Terminologies.ReacoesAdversas
{
    public interface IReacoesAdversasTerminologyService
    {
        IEnumerable<CategoriaDaReacao> GetCategorias();

        IEnumerable<TipoDeReacao> GetTiposDeReacao();
    }
}
