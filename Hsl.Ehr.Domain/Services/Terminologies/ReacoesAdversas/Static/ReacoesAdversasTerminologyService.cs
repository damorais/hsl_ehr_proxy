﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Domain.Services.Terminologies.ReacoesAdversas.Static
{
    internal class ReacoesAdversasTerminologyService : IReacoesAdversasTerminologyService
    {
        private static IEnumerable<CategoriaDaReacao> Categorias = new List<CategoriaDaReacao>()
        {
            new CategoriaDaReacao(Code: (int)CategoriasDaReacaoEnum.Alimento    , Value: "Alimento"),
            new CategoriaDaReacao(Code: (int)CategoriasDaReacaoEnum.Medicamento , Value: "Medicamento"),
            new CategoriaDaReacao(Code: (int)CategoriasDaReacaoEnum.Inalante    , Value: "Inalante"),
            new CategoriaDaReacao(Code: (int)CategoriasDaReacaoEnum.Latex       , Value: "Látex")
        };

        private static IEnumerable<TipoDeReacao> TiposDeReacao = new List<TipoDeReacao>()
        {
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.AgitaçãoPsicomotora                , Value: "Agitação Psicomotora"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.Angioedema                         , Value: "Angioedema"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.Broncoespasmo                      , Value: "Broncoespasmo"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.ChoqueAnafilatico                  , Value: "Choque Anafilático"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.DermatiteAlergica                  , Value: "Dermatite Alérgica"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.DermatiteDeContatoAlergica         , Value: "Urticária"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.EdemaDeGlote                       , Value: "Edema de Glote"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.ErupcaoCutanea                     , Value: "Erupção Cutânea"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.ParadaCardiaca                     , Value: "Parada cardíaca"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.RebaixamentoDoNivelDeConsciencia   , Value: "Prurido"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.Prurido                            , Value: "Tosse Alérgica"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.TosseAlergica                      , Value: "Dermatite de Contato Alérgica"),
            new TipoDeReacao(Code: (int)TipoDeReacaoEnum.Urticaria                          , Value: "Rebaixamento do Nível de Consciência")
        };

        public IEnumerable<CategoriaDaReacao> GetCategorias()
        {
            return Categorias;
        }

        public IEnumerable<TipoDeReacao> GetTiposDeReacao()
        {
            return TiposDeReacao;
        }
    }
}
