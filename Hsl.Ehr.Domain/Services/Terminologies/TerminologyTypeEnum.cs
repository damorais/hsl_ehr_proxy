﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Services.Terminologies
{
    public enum TerminologyTypeEnum
    {
        STATIC = 1,
        FROM_DB = 2
    }
}
