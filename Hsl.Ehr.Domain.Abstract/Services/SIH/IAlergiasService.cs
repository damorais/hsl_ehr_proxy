﻿using Hsl.Ehr.Domain.Entities.ReacoesAdversas.SIH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsl.Ehr.Domain.Abstract.Services.SIH
{
    public interface IAlergiasService
    {
        IEnumerable<AlergiaSIH> RecuperarAlergiasNaoTranscritas(int IdPaciente);
    }
}
