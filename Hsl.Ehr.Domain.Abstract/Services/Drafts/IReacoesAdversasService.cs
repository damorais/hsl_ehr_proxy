﻿using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;
using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;
using Hsl.Ehr.Util.AdvancedTypes;
using System.Collections.Generic;

namespace Hsl.Ehr.Domain.Abstract.Services.Drafts
{
    public interface IReacoesAdversasService
    {
        //TODO: Na verdade, deverá receber o usuário também
        ReacoesAdversasDraft RetornarDraftPorPaciente(int IdPaciente, string Usuario);
        IEnumerable<ReacaoAdversa> RetornarNovasReacoesPorOutrosEmRascunho(int IdPaciente, string Usuario);
        int AdicionarDraft(ReacoesAdversasDraft Draft, string Usuario, int IdPaciente);
        void AtualizarDraft(int IdReacoesAdversasDraft, ReacoesAdversasDraft Draft, string Usuario);
        Try<string> ExcluirDraft(int IdReacoesAdversasDraft, string Usuario);
        Option<ReacaoAdversa> RetornarReacaoAdversa(int IdPaciente, int IdReacaoAdversa);
        IEnumerable<ReacaoAdversa> RetornarReacoesAtivasPorPaciente(int IdPaciente);
        IEnumerable<ReacaoAdversa> RetornarReacoesInativasPorPaciente(int IdPaciente);
        IEnumerable<ReacaoAdversa> RetornarHistoricoReacao(int IdReacaoAdversa);
        bool PromoverDraft(int IdReacoesAdversasDraft, string Usuario, int? NumeroAtendimento);

        //TODO: Entrarão os outros serviços referentes a ReacoesAdversas
    }
}
