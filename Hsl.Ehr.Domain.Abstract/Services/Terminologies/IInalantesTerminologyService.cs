﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Domain.Abstract.Services.Terminologies
{
    public interface IInalantesTerminologyService
    {
        IEnumerable<Inalante> GetAll();
        Inalante GetByCode(int Code);
        IEnumerable<Inalante> GetByValue(string Value);
    }
}
