﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies.ReacoesAdversas;

namespace Hsl.Ehr.Domain.Abstract.Services.Terminologies
{
    public interface ITiposDeReacaoTerminologyService : ISnomedTerminologyService<TipoDeReacao>
    {

    }
}
