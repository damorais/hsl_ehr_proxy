﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Domain.Abstract.Services.Terminologies
{
    public interface IContatoTerminologyService
    {
        IEnumerable<Contato> GetAll();
        IEnumerable<Contato> GetByValue(string Value);
        Contato GetByCode(int Code);
    }
}
