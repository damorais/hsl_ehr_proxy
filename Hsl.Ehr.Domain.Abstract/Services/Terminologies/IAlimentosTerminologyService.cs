﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Domain.Abstract.Services.Terminologies
{
    public interface IAlimentosTerminologyService
    {
        IEnumerable<Alimento> GetAll();
        IEnumerable<Alimento> GetByValue(string Value);
        Alimento GetByCode(int Code);
    }
}
