﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies;
using Hsl.Ehr.Domain.Entities.ValueObjects.Terminology;

namespace Hsl.Ehr.Domain.Abstract.Services.Terminologies
{
    public interface IMedicamentosTerminologyService
    {
        IEnumerable<MedicamentoResumido> GetAll();

        IEnumerable<MedicamentoResumido> GetByValue(string Value);

        Medicamento GetByCode(int Code);

        Substancia GetSubstanciaByDCB(string DCB);
    }
}
