﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Abstract.Repositories;

namespace Hsl.Ehr.Domain.Abstract.Factories
{
    public interface IReacoesAdversasRepositoryFactory
    {
        IReacoesAdversasRepository Create();
    }
}
