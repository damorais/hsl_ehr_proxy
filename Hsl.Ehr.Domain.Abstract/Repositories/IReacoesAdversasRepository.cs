﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entidades;
using Hsl.Ehr.Domain.Entidades.ReacoesAdversas;

namespace Hsl.Ehr.Domain.Abstract.Repositories
{
    public interface IReacoesAdversasRepository
    {
        IEnumerable<ReacaoAdversa> GetAll(int IdPaciente);

        ReacaoAdversa Get(int IdPaciente, int IdReacaoAdversa);

        //TODO: Provavelmente não vai retornar a lista dos itens modificados.
        IEnumerable<ReacaoAdversa> Add(int IdPaciente, IEnumerable<ReacaoAdversa> ReacoesAdversas);
    }
}
