﻿using Hsl.Ehr.Domain.Entities.ReacoesAdversas.Drafts;

namespace Hsl.Ehr.Domain.Abstract.Repositories.Drafts
{
    public interface IReacoesAdversasDraftsRepository
    {
        ReacoesAdversasDraft RetornarPorPaciente(int IdPaciente, string Usuario);

        int Adicionar(ReacoesAdversasDraft Draft, string Usuario, int IdPaciente);

        ReacoesAdversasDraft Atualizar(int IdReacoesAdversasDraft, ReacoesAdversasDraft Draft);

        int Excluir(int IdReacoesAdversasDraft);
    }
}
