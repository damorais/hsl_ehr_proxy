﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hsl.Ehr.Domain.Entities.Terminologies;

namespace Hsl.Ehr.Domain.Abstract.Repositories.Terminologies
{
    public interface ISnomedTerminologyRepository<T> where T : ISnomedCodedText
    {
        IEnumerable<T> GetAll();
        T GetByCode(int SnomedCode);
    }
}
